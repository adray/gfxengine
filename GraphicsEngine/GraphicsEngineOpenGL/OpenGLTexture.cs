﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GraphicsEngineOpenGL
{
    internal class OpenGLTexture : GraphicsEngineBase.Texture
    {

        public int Descriptor { get; private set; }

        public OpenGLTexture(int descriptor)
        {
            Descriptor = descriptor;
        }

        public void Dispose()
        {
            OpenTK.Graphics.OpenGL.GL.DeleteTexture(Descriptor);
        }

        public void SetItem(object obj)
        {
            Descriptor = ((OpenGLTexture)obj).Descriptor;
        }

        public byte[] Sample()
        {
            throw new NotImplementedException();
        }
    }
}
