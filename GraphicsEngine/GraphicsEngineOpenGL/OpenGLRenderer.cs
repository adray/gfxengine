﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GraphicsEngineBase;
using OpenTK.Graphics.OpenGL;
using System.Drawing;

namespace GraphicsEngineOpenGL
{
    internal class OpenGLRenderer : Renderer
    {

        private OpenGLDevice device;
        private Texture font;
        private Rectangle[] offsets;
        private System.Diagnostics.Stopwatch watch;
        private int frames, drawcalls, triangles, texture_drawcalls;
        private TextObject infotext;

        internal OpenGLRenderer(OpenGLDevice device)
        {
            this.device = device;

            font = device.LoadTexture(System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(
                "font.png"));
            //font = device.LoadTexture("../Resources/diffuse.png");

            offsets = new Rectangle[200];

            System.IO.BinaryReader reader = new System.IO.BinaryReader(System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(
                "s.bin"));

            int i = 0;

            while (reader.BaseStream.Position < reader.BaseStream.Length)
            {

                i = reader.ReadInt32();

                offsets[i] = new Rectangle(
                    reader.ReadInt32(),
                    reader.ReadInt32(),
                    reader.ReadInt32(),
                    reader.ReadInt32());
            }


            reader.Close();

            infotext = new TextObject()
            {
                Colour = new GraphicsEngineMath.Vector4(1,1,0,1),
                Position = new GraphicsEngineMath.Vector2(20,20),
                RenderOptions = TextRenderingOptions.None,
                Scale = new GraphicsEngineMath.Vector2(1,1),
                Text = "",
            };
            watch = new System.Diagnostics.Stopwatch();
            watch.Start();
        }

        public override void RenderScene(Scene scene)
        {
#if DEBUG

            if (!watch.IsRunning || watch.ElapsedMilliseconds > 1000)
            {
                watch.Stop();
                watch.Reset();
                watch.Start();
                infotext.Text = String.Format("FPS {0}\n{1}\n{2} Triangles\n{3} draw calls\n{4} diffuse", frames, device.HardwareDescription, triangles, drawcalls, texture_drawcalls);
                frames = 0;
            }

            frames++;
            triangles = 0;
            drawcalls = 0;
            texture_drawcalls = 0;
#endif

            OpenTK.Graphics.OpenGL.GL.ClearColor(new OpenTK.Graphics.Color4(0, 0.4f, 0.8f, 1));
            device.Clear();

            OpenTK.Graphics.OpenGL.GL.Enable(OpenTK.Graphics.OpenGL.EnableCap.DepthTest);
            OpenTK.Graphics.OpenGL.GL.ShadeModel(OpenTK.Graphics.OpenGL.ShadingModel.Smooth);

            //set proj matrix

            OpenTK.Matrix4 proj =
                OpenTK.Matrix4.CreatePerspectiveFieldOfView((float)Math.PI / 4, 4.0f / 3.0f,// (float)device.Width / device.Height,
                scene.Camera.NearPlane, scene.Camera.FarPlane);

            OpenTK.Graphics.OpenGL.GL.MatrixMode(OpenTK.Graphics.OpenGL.MatrixMode.Projection);
            OpenTK.Graphics.OpenGL.GL.LoadMatrix(ref proj);

            //set view matrix
            OpenTK.Matrix4 view =
                OpenTK.Matrix4.LookAt(scene.Camera.Position.X, scene.Camera.Position.Y, scene.Camera.Position.Z,
                scene.Camera.LookAt.X, scene.Camera.LookAt.Y, scene.Camera.LookAt.Z, 0, 1, 0);

            //OpenTK.Graphics.OpenGL.GL.Enable(EnableCap.Light0);

            //foreach (var light in scene.Lights)
            //{
            //    OpenTK.Graphics.OpenGL.GL.Light(LightName.Light0, LightParameter.Diffuse, light.Color.X);
            //}

            OpenTK.Graphics.OpenGL.GL.Enable(OpenTK.Graphics.OpenGL.EnableCap.CullFace);
            OpenTK.Graphics.OpenGL.GL.Disable(OpenTK.Graphics.OpenGL.EnableCap.Blend);

            foreach (var obj in scene.Objects)
            {
                foreach (var subset in obj.Subsets)
                {

                    if ((obj.Flags & RenderFlags.DiffuseMap) == RenderFlags.DiffuseMap)
                    {
                        device.BindTexture((OpenGLTexture)subset.Material.DiffuseTexture, false);
                    }
                    else
                    {
                        OpenTK.Graphics.OpenGL.GL.Color3(subset.Material.DiffuseConstant.X, subset.Material.DiffuseConstant.Y,
                            subset.Material.DiffuseConstant.Z);
                    }

                    //set world matrix
                    OpenTK.Matrix4 world = new OpenTK.Matrix4(obj.Transform.M11, obj.Transform.M12,
                        obj.Transform.M13, obj.Transform.M14, obj.Transform.M21, obj.Transform.M22,
                        obj.Transform.M23, obj.Transform.M24, obj.Transform.M31, obj.Transform.M32,
                        obj.Transform.M33, obj.Transform.M34, obj.Transform.M41, obj.Transform.M42,
                        obj.Transform.M43, obj.Transform.M44) * view;
                       // OpenTK.Matrix4.Identity;
                    //world.Transpose();

                    OpenTK.Graphics.OpenGL.GL.MatrixMode(OpenTK.Graphics.OpenGL.MatrixMode.Modelview);
                    OpenTK.Graphics.OpenGL.GL.LoadMatrix(ref world);
                    //OpenTK.Graphics.OpenGL.GL.MultMatrix(ref view);
                    //OpenTK.Graphics.OpenGL.GL.Scale(-1, 1, 1);
                    //OpenTK.Graphics.OpenGL.GL.PushMatrix();

                    device.Render((OpenGLMesh) subset.Mesh);
                    //OpenTK.Graphics.OpenGL.GL.PopMatrix();

                    OpenTK.Graphics.OpenGL.GL.Color3(1.0f, 1.0f, 1.0f);

                    triangles += subset.Mesh.PrimitiveCount;

                }


            }

            if (scene.Particles.Count > 0)
            {

                OpenTK.Graphics.OpenGL.GL.Disable(OpenTK.Graphics.OpenGL.EnableCap.CullFace);
                OpenTK.Graphics.OpenGL.GL.Enable(OpenTK.Graphics.OpenGL.EnableCap.Blend);
                OpenTK.Graphics.OpenGL.GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);

                var mesh = device.CreateMesh(VertexType.PositionNormalTextured);

                foreach (var particle in scene.Particles)
                {

                    var direction = scene.Camera.Position - particle.Position;
                    direction.Normalize();

                    var binormal = new GraphicsEngineMath.Vector3();
                    var tangent = new GraphicsEngineMath.Vector3();

                    binormal = GraphicsEngineMath.Vector3.Cross(new GraphicsEngineMath.Vector3(0, 1, 0), direction);
                    tangent = GraphicsEngineMath.Vector3.Cross(binormal, direction);
                    binormal.Normalize();
                    tangent.Normalize();

                    var corners = new GraphicsEngineMath.Vector3[4];
                    corners[0] = particle.Position + binormal * particle.Scale.X + tangent * particle.Scale.Y;
                    corners[1] = particle.Position + binormal * particle.Scale.X - tangent * particle.Scale.Y;
                    corners[2] = particle.Position - binormal * particle.Scale.X + tangent * particle.Scale.Y;
                    corners[3] = particle.Position - binormal * particle.Scale.X - tangent * particle.Scale.Y;

                    mesh.AddTriangle(new Triangle(new Vertex(10).SetData(0,
                        corners[0]).SetData(3, 1).SetData(8, 1, 1),
                        new Vertex(10).SetData(0, corners[1]).SetData(3, 1).SetData(8, 1, 0),
                        new Vertex(10).SetData(0, corners[2]).SetData(3, 1).SetData(8, 0, 1)));
                    mesh.AddTriangle(new Triangle(new Vertex(10).SetData(0,
                        corners[1]).SetData(3, 1).SetData(8, 1, 0),
                        new Vertex(10).SetData(0, corners[2]).SetData(3, 1).SetData(8, 0, 1),
                        new Vertex(10).SetData(0, corners[3]).SetData(3, 1).SetData(8, 0, 0)));

                }

                OpenTK.Graphics.OpenGL.GL.Color3(0f, 0f, 0f);

                device.BindTexture((OpenGLTexture)scene.Particles[0].DiffuseTexture, true);

                OpenTK.Graphics.OpenGL.GL.MatrixMode(OpenTK.Graphics.OpenGL.MatrixMode.Modelview);
                OpenTK.Graphics.OpenGL.GL.LoadMatrix(ref view);
                //OpenTK.Graphics.OpenGL.GL.Scale(-1, 1, 1);

                device.Render((OpenGLMesh)mesh);
                OpenTK.Graphics.OpenGL.GL.PopMatrix();

                triangles += mesh.PrimitiveCount;

            }

            OpenTK.Graphics.OpenGL.GL.Disable(OpenTK.Graphics.OpenGL.EnableCap.DepthTest);
            OpenTK.Graphics.OpenGL.GL.Disable(OpenTK.Graphics.OpenGL.EnableCap.CullFace);
            OpenTK.Graphics.OpenGL.GL.Enable(OpenTK.Graphics.OpenGL.EnableCap.Blend);
            OpenTK.Graphics.OpenGL.GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);

            foreach (var text in scene.Text)
            {


                RenderText(text);

            }

#if DEBUG
            RenderText(infotext);
#endif

            foreach (var quad in scene.Quads)
            {

                DrawQuad(font, new RectangleF(quad.Position.X, quad.Position.Y, quad.Width+quad.Position.X, quad.Position.Y+quad.Height));

            }

            OpenTK.Graphics.OpenGL.GL.Disable(OpenTK.Graphics.OpenGL.EnableCap.Blend);
            
            device.Present();

        }

        private void RenderText(TextObject text)
        {
            device.BindTexture((OpenGLTexture)font, true);

            float x = 0;
            float y = 0;

            var fontmesh = device.CreateMesh(VertexType.PositionNormalTextured);

            for (int i = 0; i < text.Text.Length; i++)
            {
                if (text.Text[i] == '\n')
                {
                    y += 20;
                    x = 0;
                    continue;
                }

                Rectangle rectangle = offsets[text.Text[i]];

                if (rectangle.Width == 0)
                {
                    x += 20;
                    continue;
                }

                fontmesh.AddTriangle(new Triangle(
                    new Vertex(10).SetData(0, text.Position.X + x, device.Height - (text.Position.Y + y + rectangle.Height/2), 0, 1, 0, 0, 0, 1, rectangle.X / 1024.0f, (rectangle.Y + rectangle.Height) / 1024.0f),
                    new Vertex(10).SetData(0, text.Position.X + x + rectangle.Width/2, device.Height - (text.Position.Y + y+ rectangle.Height/2), 0, 1, 0, 0, 0, 1, (rectangle.X + rectangle.Width) / 1024.0f, (rectangle.Y + rectangle.Height) / 1024.0f),
                    new Vertex(10).SetData(0, text.Position.X + x + rectangle.Width/2, device.Height - (text.Position.Y + y ), 0, 1, 0, 0, 0, 1, (rectangle.X + rectangle.Width) / 1024.0f, (rectangle.Y) / 1024.0f)));

                fontmesh.AddTriangle(new Triangle(
                    new Vertex(10).SetData(0, text.Position.X + x, device.Height - (text.Position.Y + y), 0, 1, 0, 0, 0, 1, rectangle.X / 1024.0f, (rectangle.Y) / 1024.0f),
                    new Vertex(10).SetData(0, text.Position.X + x + rectangle.Width/2, device.Height -( text.Position.Y + y), 0, 1, 1, 0, 0, 1, (rectangle.X + rectangle.Width) / 1024.0f, (rectangle.Y) / 1024.0f),
                    new Vertex(10).SetData(0, text.Position.X + x, device.Height - (text.Position.Y + y + rectangle.Height/2), 0, 1, 1, 1, 0, 1, (rectangle.X) / 1024.0f, (rectangle.Y + rectangle.Height) / 1024.0f)));

                x += rectangle.Width/2;
            }

            OpenTK.Graphics.OpenGL.GL.Color3(text.Colour.X, text.Colour.Y, text.Colour.Z);

            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();
            GL.Ortho(0, device.Width, 0, device.Height, -1, 1);
            GL.MatrixMode(MatrixMode.Modelview);
            GL.LoadIdentity();

            device.Render((OpenGLMesh)fontmesh);

            GL.PopMatrix();
        }

        private void DrawQuad(Texture texture, RectangleF rectangle)
        {

            device.BindTexture((OpenGLTexture)font, true);

            var mesh = device.CreateMesh(VertexType.PositionNormalTextured);

            mesh.AddTriangle(new Triangle(
                        new Vertex(10).SetData(0, rectangle.X, rectangle.Y + rectangle.Height, 0, 1, 0, 0, 0, 1, 0, 0),
                        new Vertex(10).SetData(0, rectangle.X + rectangle.Width, rectangle.Y + rectangle.Height, 0, 1, 1, 0, 0, 1, 1.0f, 0),
                        new Vertex(10).SetData(0, rectangle.X, rectangle.Y, 0, 1, 1, 1, 0, 1, 0, 1.0f)));

            mesh.AddTriangle(new Triangle(
                        new Vertex(10).SetData(0, rectangle.X+rectangle.Width, rectangle.Y + rectangle.Height, 0, 1, 0, 0, 0, 1, 1, 0),
                        new Vertex(10).SetData(0, rectangle.X + rectangle.Width, rectangle.Y, 0, 1, 1, 0, 0, 1, 1, 1),
                        new Vertex(10).SetData(0, rectangle.X, rectangle.Y, 0, 1, 1, 1, 0, 1, 0, 1.0f)));

            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();
            GL.Ortho(0, device.Width, device.Height, 0, -1, 1);
            GL.MatrixMode(MatrixMode.Modelview);
            GL.LoadIdentity();

            device.Render((OpenGLMesh)mesh);

            GL.PopMatrix();

        }

        public override void Dispose()
        {
            
        }

        public override void SetPostProcessChain(Chain chain)
        {
            throw new NotImplementedException();
        }
    }
}
