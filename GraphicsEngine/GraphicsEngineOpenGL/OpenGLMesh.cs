﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GraphicsEngineOpenGL
{
    internal class OpenGLMesh : GraphicsEngineBase.Mesh
    {
        public int[] Buffers { get; private set; }
        public List<GraphicsEngineBase.Triangle> Triangles { get; private set; }

        public OpenGLMesh(GraphicsEngineBase.VertexType type)
            : base(type)
        {

            Buffers = new int[2];
            Triangles = base.Triangles;

        }

        public override void UpdateBuffer(GraphicsEngineBase.Device device)
        {
            
            if (!dirty)
                return;

            //OpenTK.Graphics.OpenGL.GL.GenBuffers(Buffers.Length, Buffers);

            //short[] indices = new short[PrimitiveCount*3];
            //float[] vertices = new float[3*PrimitiveCount*this.DataCollection.VertexSize/sizeof(float)];

            //int index=0;

            //foreach (var v in Triangles)
            //{
            //    Array.Copy(v.vertices[0].data, 0, vertices, index, this.DataCollection.VertexSize / sizeof(float));
            //    index += this.DataCollection.VertexSize / sizeof(float);
            //    Array.Copy(v.vertices[0].data, 0, vertices, index, this.DataCollection.VertexSize / sizeof(float));
            //    index += this.DataCollection.VertexSize / sizeof(float);
            //    Array.Copy(v.vertices[0].data, 0, vertices, index, this.DataCollection.VertexSize / sizeof(float));
            //    index += this.DataCollection.VertexSize / sizeof(float);
            //}


            //OpenTK.Graphics.OpenGL.GL.BindBuffer(OpenTK.Graphics.OpenGL.BufferTarget.ArrayBuffer, Buffers[0]);
            //OpenTK.Graphics.OpenGL.GL.BufferData(OpenTK.Graphics.OpenGL.BufferTarget.ArrayBuffer, (IntPtr)(vertices.Length*sizeof(float)),
            //    (vertices), OpenTK.Graphics.OpenGL.BufferUsageHint.StaticDraw);


            //OpenTK.Graphics.OpenGL.GL.BindBuffer(OpenTK.Graphics.OpenGL.BufferTarget.ElementArrayBuffer, Buffers[1]);
            //OpenTK.Graphics.OpenGL.GL.BufferData(OpenTK.Graphics.OpenGL.BufferTarget.ElementArrayBuffer, (IntPtr)(indices.Length * sizeof(short)),
            //    (indices), OpenTK.Graphics.OpenGL.BufferUsageHint.StaticDraw);

            dirty = false;

        }

        public override void Dispose()
        {

            //OpenTK.Graphics.OpenGL.GL.DeleteBuffers(2, Buffers);

        }

        public override void SetItem(object obj)
        {
            throw new NotImplementedException();
        }

        public override int MaxPrimitivesPerSubset
        {
            get { return 10000; }
        }
    }
}
