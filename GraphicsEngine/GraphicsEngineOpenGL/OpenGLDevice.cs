﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GraphicsEngineBase;
using OpenTK.Platform.Windows;
using OpenTK.Graphics.OpenGL;
using GraphicsEngineBase.Loader;

namespace GraphicsEngineOpenGL
{
    internal class DeviceDescription : GraphicsEngineBase.DeviceDescription
    {

        public DeviceDescription() : base("OpenGL")
        {

        }

        public override Device MakeDevice(IntPtr handle)
        {

            return new OpenGLDevice(handle);

        }

        public static DeviceDescription GetDeviceDescription()
        {
            return new DeviceDescription();
        }
    }

    public class OpenGLDevice : Device
    {



        public static GraphicsEngineBase.DeviceDescription DeviceDescription()
        {
            return new DeviceDescription();
        }

        private OpenTK.GLControl control;
        private OpenGLRenderer renderer;
        private System.Windows.Forms.Form window;

        internal OpenGLDevice(IntPtr handle)
        {

            //need to embed opentk control in window
            control = new OpenTK.GLControl(OpenTK.Graphics.GraphicsMode.Default);
            control.Enabled = false;

            window = (System.Windows.Forms.Form)System.Windows.Forms.Form.FromHandle(handle);

            window.Controls.Add(control);
            control.Dock = System.Windows.Forms.DockStyle.Fill;
            window.Show();

            renderer = new OpenGLRenderer(this);

            //setup viewport

            //GL.MatrixMode(MatrixMode.Projection);
            //GL.LoadIdentity();
            //GL.Ortho(0, Width, 0, Height, -1, 1);
            GL.Viewport(0, 0, Width, Height);
            
            //depth buffer
            

        }

        internal void Clear()
        {
            GL.Clear(ClearBufferMask.DepthBufferBit | ClearBufferMask.ColorBufferBit | ClearBufferMask.StencilBufferBit);
        }

        internal void Present()
        {
            control.SwapBuffers();
            GL.Finish();
        }

        public override Renderer Renderer()
        {
            return renderer;
        }

        public override Texture LoadTexture(string filename)
        {

            System.Drawing.Bitmap img = new System.Drawing.Bitmap(filename);


            Texture tex = LoadTexture(img);

            //AddWatchedItem(new Watch(tex, filename, new Reload(
            //    (n) =>
            //    {
            //        try
            //        {
            //            return LoadTexture(new System.Drawing.Bitmap(n));
            //        }
            //        catch (System.IO.FileNotFoundException ex)
            //        {
            //            return tex;
            //        }
            //    })));

            return tex;
        }

        public override Texture LoadTexture(System.IO.Stream stream)
        {

            System.Drawing.Bitmap img = new System.Drawing.Bitmap(stream);


            return LoadTexture(img);
        }

        public override Texture LoadTexture(System.Drawing.Bitmap img)
        {

            int descriptor = GL.GenTexture();
            GL.BindTexture(TextureTarget.Texture2D, descriptor);

            System.Drawing.Imaging.BitmapData data = img.LockBits(new System.Drawing.Rectangle(0, 0, img.Width, img.Height),
                System.Drawing.Imaging.ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            OpenTK.Graphics.OpenGL.GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba,
                img.Width, img.Height, 0, PixelFormat.Rgba, PixelType.UnsignedByte, data.Scan0);

            img.UnlockBits(data);

            GL.GenerateMipmap(GenerateMipmapTarget.Texture2D);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)OpenTK.Graphics.OpenGL.TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)OpenTK.Graphics.OpenGL.TextureMagFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)OpenTK.Graphics.OpenGL.TextureWrapMode.Repeat);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)OpenTK.Graphics.OpenGL.TextureWrapMode.Repeat);


            return new OpenGLTexture(descriptor);
        }

        public override GraphicsEngineBase.ObjectFactory LoadObject(string filename)
        {

	        filename = System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), filename);

            if (System.IO.Path.GetExtension(filename) == ".obj")
                return ObjLoader.Load(this, VertexType.PositionNormalTextured, filename);
            else if (System.IO.Path.GetExtension(filename) == ".ply")
            {
                
                var subsets = new List<ObjectSubset>();
                var instance = new List<ObjectInstanceSubset>();
                var mesh = PlyLoader.Load(this, VertexType.PositionNormalTextured, filename);

                subsets.Add(new ObjectSubset() { Mesh =mesh });
                instance.Add(new ObjectInstanceSubset() { Mesh = mesh });

                return new ObjectFactory(this, subsets, instance);

            }

            throw new Exception("Failed to load mesh, format not recognized.");

        }

        public override Mesh CreateMesh(VertexType vertextype)
        {
            return new OpenGLMesh(vertextype);
        }

        public override int Width
        {
            get { return control.Width; }
        }

        public override int Height
        {
            get { return control.Height; }
        }

        public override string HardwareDescription
        {
            get { return OpenTK.Graphics.OpenGL.GL.GetString(StringName.Renderer); }
        }

        public override Scene LoadScene(string filename)
        {
            throw new NotImplementedException();
        }

        internal void Render(OpenGLMesh mesh)
        {

            mesh.UpdateBuffer(this);

            GL.Begin(BeginMode.Triangles);

            foreach (var tri in mesh.Triangles)
            {
                for (int i = 0; i < 3; i++)
                {

                    GL.TexCoord2(tri.vertices[i].data[8],
                        tri.vertices[i].data[9]);

                    GL.Vertex4(tri.vertices[i].data[0],
                        tri.vertices[i].data[1],
                        tri.vertices[i].data[2],
                        tri.vertices[i].data[3]);

                }
            }
            
            GL.End();

        }

        internal void BindTexture(OpenGLTexture texture, bool blend)
        {

            int mode = blend ? (int)OpenTK.Graphics.OpenGL.TextureEnvMode.Blend
                : (int)OpenTK.Graphics.OpenGL.TextureEnvMode.Replace;

            OpenTK.Graphics.OpenGL.GL.Enable(OpenTK.Graphics.OpenGL.EnableCap.Map1TextureCoord1);
            OpenTK.Graphics.OpenGL.GL.Enable(OpenTK.Graphics.OpenGL.EnableCap.Texture2D);
            OpenTK.Graphics.OpenGL.GL.BindTexture(OpenTK.Graphics.OpenGL.TextureTarget.Texture2D,
                (texture).Descriptor);
            OpenTK.Graphics.OpenGL.GL.PixelStore(OpenTK.Graphics.OpenGL.PixelStoreParameter.UnpackAlignment, 1);
            OpenTK.Graphics.OpenGL.GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)OpenTK.Graphics.OpenGL.TextureMinFilter.Linear);
            OpenTK.Graphics.OpenGL.GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)OpenTK.Graphics.OpenGL.TextureMagFilter.Linear);
            OpenTK.Graphics.OpenGL.GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)OpenTK.Graphics.OpenGL.TextureWrapMode.Repeat);
            OpenTK.Graphics.OpenGL.GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)OpenTK.Graphics.OpenGL.TextureWrapMode.Repeat);
            OpenTK.Graphics.OpenGL.GL.TexEnv(TextureEnvTarget.TextureEnv, TextureEnvParameter.TextureEnvMode,
                //(int)(OpenTK.Graphics.OpenGL.TextureEnvMode.Replace|OpenTK.Graphics.OpenGL.TextureEnvMode.Add));
                //(int)OpenTK.Graphics.OpenGL.TextureEnvMode.Blend);
                mode);
            OpenTK.Graphics.OpenGL.GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.GenerateMipmap, (int)OpenTK.Graphics.OpenGL.GenerateMipmapTarget.Texture2D);

        }

        private static void RenderVBO(OpenGLMesh mesh)
        {
            GL.EnableClientState(ArrayCap.VertexArray);
            //GL.EnableClientState(ArrayCap.NormalArray);
            //GL.EnableClientState(ArrayCap.TextureCoordArray);

            GL.BindBuffer(BufferTarget.ArrayBuffer, mesh.Buffers[0]);
            GL.VertexPointer(2, VertexPointerType.Float, mesh.DataCollection.VertexSize / sizeof(float), 0);

            //GL.BindBuffer(BufferTarget.ArrayBuffer, mesh.Buffers[0]);
            //GL.VertexPointer(4, VertexPointerType.Float, mesh.DataCollection.VertexSize / sizeof(float), 4);

            //GL.BindBuffer(BufferTarget.ArrayBuffer, mesh.Buffers[0]);
            //GL.VertexPointer(2, VertexPointerType.Float, mesh.DataCollection.VertexSize / sizeof(float), 8);

            GL.DrawArrays(BeginMode.Triangles, 0, mesh.PrimitiveCount);


            GL.DisableClientState(ArrayCap.VertexArray);
            //GL.DisableClientState(ArrayCap.NormalArray);
            //GL.DisableClientState(ArrayCap.TextureCoordArray);
        }

        public override TerrainObject LoadTerrain(string image)
        {
            TerrainObject terrain = GraphicsEngineBase.Loader.TerrainLoader.Load(this, image);

            return terrain;
        }

        public override MeshInstance CreateInstance()
        {

            throw new NotImplementedException();

        }

        public override ObjectFactory LoadObject(System.IO.Stream stream)
        {
            return ObjLoader.Load(this, VertexType.PositionNormalTextured, new System.IO.StreamReader(stream));
        }

    }
}
