﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using GraphicsEngineMath;

namespace MathTest
{
    [TestFixture]
    public class PlaneTest
    {

        [Test]
        public void Distance()
        {


            Plane plane = new Plane(new Vector3(0, 1, 0), 100);

            Assert.AreEqual(100, plane.Distance(new Vector3(0, 200, 0)), 0.0001f);


        }

    }
}
