﻿using GraphicsEngineMath;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MathTest
{
    [TestFixture]
    public class QuaternionTest
    {

        [Test]
        public void AngleTest()
        {

            Quaternion q1 = new Quaternion(new Vector3(1,0,0), 90);
            //q.ToAngles();

            Quaternion q2 = Quaternion.FromAngles(new Vector3(90, 0, 0));

            Assert.AreEqual(q1.X, q2.X, 0.00001f);
            Assert.AreEqual(q1.Y, q2.Y, 0.00001f);
            Assert.AreEqual(q1.Z, q2.Z, 0.00001f);
            Assert.AreEqual(q1.W, q2.W, 0.00001f);

        }

        [Test]
        public void LerpTest()
        {

            Quaternion q1 = new Quaternion(new Vector3(1, 0, 0), 90);
            Quaternion q2 = new Quaternion(new Vector3(0, 1, 0), 90);
            
            for (int i = 0; i <= 20; i++)
            {

                float t = i / 20.0f;

                Quaternion lerp = Quaternion.Lerp(q1, q2, t);
                Quaternion slerp = Quaternion.Slerp(q1, q2, t);
                Quaternion nlerp = Quaternion.Nlerp(q1, q2, t);

                Console.WriteLine(String.Format("t={0}, Lerp={1}, Slerp={2}, Nlerp={3}, Angle{4}", t, lerp, slerp, nlerp, lerp.ToAngles()));

            }


        }


    }
}
