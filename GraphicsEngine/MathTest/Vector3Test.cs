﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using GraphicsEngineMath;

namespace MathTest
{
    [TestFixture]
    public class Vector3Test
    {

        [Test]
        public void CrossTest()
        {

            Vector3 v1 = new Vector3(0, 0, 1);
            Vector3 v2 = new Vector3(1, 0, 0);

            Vector3 v3 = Vector3.Cross(v1, v2);


            Assert.AreEqual(0, v3.X, 0.0001f);
            Assert.AreEqual(1, v3.Y, 0.0001f);
            Assert.AreEqual(0, v3.Z, 0.0001f);

            
        }

        [Test]
        public void DotTest()
        {

            Vector3 v1 = new Vector3(1, 2, 3);
            Vector3 v2 = new Vector3(4, 5, 6);

            float dot = Vector3.Dot(v1, v2);


            Assert.AreEqual(32, dot, 0.0001f);

        }

        [Test]
        public void LengthTest()
        {

            Vector3 v = new Vector3(1, 2, 3);

            Assert.AreEqual(14, v.Length()*v.Length(), 0.0001f);

        }

        [Test]
        public void NormalizeTest()
        {

            Vector3 v = new Vector3(10,11,12);
            v.Normalize();

            Assert.AreEqual(1, v.Length(), 0.0001f);

        }

    }
}
