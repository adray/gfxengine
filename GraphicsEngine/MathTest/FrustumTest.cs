﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using GraphicsEngineMath;

namespace MathTest
{
    [NUnit.Framework.TestFixture]
    public class FrustumTest
    {

        [Test]
        public void FrustumContainsTest()
        {

            Frustum frustum = new Frustum((float)Math.PI / 2, 1, 1, 100);

            //foreach (var plane in frustum.Planes)
            //    Console.WriteLine("Normal: " + plane.Normal.ToString() + " D: " + plane.D.ToString());

            Assert.IsTrue(frustum.Test(new Vector3(1, 1, 1), 10));

            Assert.IsFalse(frustum.Test(new Vector3(1, 1, 105), 2));

        }

        [Test]
        public void FrustumContainsTest2()
        {

            Frustum frustum = new Frustum((float)Math.PI / 4, 4.0f/3, 1, 100);

            Assert.IsTrue(frustum.Test(new Vector3(1, 1, 1), 10));

            Assert.IsFalse(frustum.Test(new Vector3(1, 1, 105), 2));

            Assert.IsTrue(frustum.Test(new Vector3(20, 20, 55), 2));

        }

        [Test]
        public void FrustumPlaneTest()
        {

            Frustum frustum = new Frustum((float)Math.PI / 4, 4.0f / 3, 1, 100);

            Assert.True(Vector3.Dot(frustum.Planes[0].Normal, frustum.Planes[1].Normal) < 0);
            Assert.True(Vector3.Dot(frustum.Planes[2].Normal, frustum.Planes[3].Normal) < 0);
            Assert.True(Vector3.Dot(frustum.Planes[4].Normal, frustum.Planes[5].Normal) < 0);

        }

        [Test]
        public void FrustumContainsTest3()
        {

            Frustum frustum = new Frustum((float)Math.PI / 2, 1, 1, 100);

            //foreach (var plane in frustum.Planes)
            //    Console.WriteLine("Normal: " + plane.Normal.ToString() + " D: " + plane.D.ToString());

            Assert.IsTrue(frustum.Test(new Vector3(1, 1, 1), 200));

        }

    }
}
