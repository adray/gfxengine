﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using GraphicsEngineMath;

namespace MathTest
{
    [NUnit.Framework.TestFixture]
    public class MatrixTest
    {

        [Test]
        public void MultiplyTest()
        {


            Random rnd=  new Random();

            for (int j = 0; j < 1000; j++)
            {

                Matrix4x4 m1 = Matrix4x4.Identity();
                Matrix4x4 m2 = new Matrix4x4();

                for (int i = 0; i < 16; i++)
                {
                    //m1.data[i] = (float)rnd.NextDouble() * 200 - 100;
                    m2.data[i] = (float)rnd.NextDouble() * 200 - 100;
                }


                Matrix4x4 res = m1 * m2;

                for (int i = 0; i < 16; i++)
                {

                    Assert.AreEqual(m2.data[i], res.data[i], 0.000001f);

                }

            }
        }

        [Test]
        public void MultiplyTest2()
        {


            Random rnd = new Random();

            for (int j = 0; j < 1000; j++)
            {

                Matrix4x4 m1 = new Matrix4x4();
                Matrix4x4 m2 = new Matrix4x4();

                for (int i = 0; i < 16; i++)
                {
                    m1.data[i] = (float)rnd.NextDouble() * 200 - 100;
                }

                m2 = Matrix4x4.Inverse(m1);

                Matrix4x4 res = m1 * m2;
                Matrix4x4 identity = Matrix4x4.Identity();

                for (int i = 0; i < 16; i++)
                {

                    Assert.AreEqual(identity.data[i], res.data[i], 0.01f);

                }

            }
        }

        [Test]
        public void InvertTest()
        {


            Random rnd = new Random();

            for (int j = 0; j < 1000; j++)
            {

                Matrix4x4 m = new Matrix4x4();

                for (int i = 0; i < 16; i++)
                {
                    m.data[i] = (float)rnd.NextDouble() * 200 - 100;
                }

                Matrix4x4 res = Matrix4x4.Inverse(m);

                Matrix4x4 I = res * m;

                Matrix4x4 I2 = Matrix4x4.Identity();

                for (int i = 0; i < 16; i++)
                {

                    Assert.AreEqual(I2.data[i], I.data[i], 0.1f, "\nPassed: " + j + "\n m * inverse(m)=" + I.ToString()+
                        "\nm="+m.ToString() + "\ninverse(m)=" + res.ToString() );

                }

            }
        }

        [Test]
        public void TransposeTest()
        {

            Random rnd = new Random();

            for (int j = 0; j < 1000; j++)
            {

                Matrix4x4 m = new Matrix4x4();
                Matrix4x4 m2 = new Matrix4x4();

                for (int i = 0; i < 16; i++)
                {
                    m.data[i] = (float)rnd.NextDouble() * 200 - 100;
                    m2.data[i] = m.data[i];
                }

                m.Transpose();

                for (int i = 0; i < 4; i++)
                    for (int k = 0; k < 4; k++)
                    {

                        Assert.AreEqual(m2.data[i+k*4], m.data[k+i*4], 0.1f, "\nPassed: " + j
                            + "\nTranspose:\n" + m.ToString() + "\nOriginal:\n" + m2.ToString());


                    }

                Matrix4x4 m3 = Matrix4x4.Transpose(m2);

                for (int i = 0; i < 4; i++)
                    for (int k = 0; k < 4; k++)
                    {

                        Assert.AreEqual(m2.data[i + k * 4], m3.data[k + i * 4], 0.1f, "\nPassed: " + j
                            + "\nTranspose:\n" + m3.ToString() + "\nOriginal:\n" + m2.ToString());


                    }

            }

        }


        [Test]
        public void RotationYTest()
        {

            Random rnd = new Random();

            for (int j = 0; j < 1000; j++)
            {


                Matrix4x4 m = Matrix4x4.RotationY( (float) (Math.PI * (rnd.NextDouble() * 4 - 2) ));

                Matrix4x4 m2 = Matrix4x4.Inverse(m);

                Matrix4x4 s = m * m2;

                Matrix4x4 i = Matrix4x4.Identity();

                for (int k = 0; k < 16; k++)
                {
                    Assert.AreEqual(i.data[k], s.data[k], 0.001f, "\nPassed: " + j);
                }

            }
        }

        [Test]
        public void Transform()
        {

            Matrix4x4 m = Matrix4x4.Identity();

            Vector4 v = new Vector4(1, 1, 1, 1);

            Vector4 v2 = Matrix4x4.Transform(m, v);

            Assert.AreEqual(v.X, v2.X, 0.0001);
            Assert.AreEqual(v.Y, v2.Y, 0.0001);
            Assert.AreEqual(v.Z, v2.Z, 0.0001);
            Assert.AreEqual(v.W, v2.W, 0.0001);

        }

    }
}
