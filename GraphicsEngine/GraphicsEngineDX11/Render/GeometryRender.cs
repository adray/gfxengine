﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GraphicsEngineBase;
using GraphicsEngineMath;
using ConstantBuffer = GraphicsEngineBase.PooledResource<SlimDX.Direct3D11.Buffer>;

namespace GraphicsEngineDX11
{
    internal partial class DirectX11Renderer
    {    

        private void GBufferRender(Scene scene, Frustum frustum, SlimDX.Matrix view, SlimDX.Matrix projTranspose,
            SlimDX.Matrix viewTranspose,
            Dictionary<PlaneObject, GraphicsEngineDX11.Render.Reflection> reflections)
        {

            #region GBufferRender

            #region Objects

            device.SetVertexShader((DirectX11VertexShader)geometryvertex[(int)RenderVertexFlags.None]);
#if DEBUG
            int culled=0;
#endif

            List<KeyValuePair<GraphicsEngineBase.Object, ObjectSubset>> subsets = new List<KeyValuePair<GraphicsEngineBase.Object, ObjectSubset>>();

            foreach (GraphicsEngineBase.Object obj in scene.Objects)
            {
                if ((obj.Filter & RenderFilter.No_Render) == 0)
                {
                    foreach (var sub in obj.Subsets)
                    {
                        Sphere sphere = sub.Mesh.CalculateBoundingSphere();

                        Vector4 worldpos = Matrix4x4.Transform(obj.Transform * sub.Transform, new Vector4(sphere.Position.X,
                            sphere.Position.Y, sphere.Position.Z, 1));

                        SlimDX.Vector4 VSpos =
                            SlimDX.Vector4.Transform(new SlimDX.Vector4(worldpos.X, worldpos.Y, worldpos.Z, worldpos.W), view);


                        if (!frustum.Test(new Vector3(VSpos.X, VSpos.Y, VSpos.Z), sphere.Radius))
                        {
#if DEBUG
                            culled++;
#endif
                            continue;
                        }

                        subsets.Add(new KeyValuePair<GraphicsEngineBase.Object, ObjectSubset>(obj, sub));
                    }
                }
            }

            List<List<KeyValuePair<GraphicsEngineBase.Object, ObjectSubset>>>
                render = new List<List<KeyValuePair<GraphicsEngineBase.Object, ObjectSubset>>>();
            foreach (KeyValuePair<GraphicsEngineBase.Object, ObjectSubset> sub in subsets)
            {
                bool found = false;
                foreach (var set in render)
                {
                    if (set[0].Value.RenderFlags == sub.Value.RenderFlags &&
                        set[0].Value.Material.DiffuseTexture == sub.Value.Material.DiffuseTexture &&
                        set[0].Value.Material.BumpTexture == sub.Value.Material.BumpTexture)
                    {
                        set.Add(sub);
                        found = true;
                        break;
                    }
                }

                if (!found)
                {
                    List<KeyValuePair<GraphicsEngineBase.Object, ObjectSubset>> list
                        = new List<KeyValuePair<GraphicsEngineBase.Object, ObjectSubset>>();
                    list.Add(sub);
                    render.Add(list);

                }
            
            }

            render.Sort(new Comparison<List<KeyValuePair<GraphicsEngineBase.Object, ObjectSubset>>>((a, b) =>
                {
                    if (a[0].Value.RenderFlags < b[0].Value.RenderFlags)
                        return -1;
                    if (a[0].Value.Material.DiffuseTexture ==
                        b[0].Value.Material.DiffuseTexture)
                        return b[0].Value.Mesh.PrimitiveCount.CompareTo(a[0].Value.Mesh.PrimitiveCount);

                    return -1;
                }));
            

            foreach (KeyValuePair<GraphicsEngineBase.Object, ObjectSubset> sub in subsets)
            {
                ObjectSubset subset = sub.Value;
                GraphicsEngineBase.Object obj = sub.Key;


                RenderFlags combined = obj.Flags & subset.RenderFlags;


                device.SetPixelShader((DirectX11PixelShader)geometrypixel[(int)combined]);

                //set material


                if ((combined & RenderFlags.DiffuseMap) == RenderFlags.DiffuseMap)
                {

                    DirectX11Texture diffuse = (DirectX11Texture)subset.Material.DiffuseTexture;
                    if (diffuse == null)
                        throw new Exception("Diffuse map not supplied.");
                    device.SetPixelTexture(diffuse.texture, 1);
                    device.SetSampler(sampler, 1);
#if DEBUG
                    texture_drawcalls++;
#endif
                }

                if ((combined & RenderFlags.BumpMap) == RenderFlags.BumpMap)
                {

                    DirectX11Texture bump = (DirectX11Texture)subset.Material.BumpTexture;
                    if (bump == null)
                        throw new Exception("Bump map not supplied.");
                    device.SetPixelTexture(bump.texture, 0);
                    device.SetSampler(normalsampler, 0);
                }

                //set transforms and transpose them

                SlimDX.DataStream data = new SlimDX.DataStream(56 * sizeof(float), true, true);

                Matrix4x4 worldTranspose = Matrix4x4.Transpose(obj.RootTransform * obj.Transform * subset.Transform);

                //world
                data.WriteRange<float>(worldTranspose.data);
                //proj
                data.Write<SlimDX.Matrix>(projTranspose);
                //view
                data.Write<SlimDX.Matrix>(viewTranspose);
                //diffuse constant
                data.WriteRange<float>(new float[]{subset.Material.DiffuseConstant.X,subset.Material.DiffuseConstant.Y,
                subset.Material.DiffuseConstant.Z});
                data.Write<float>(scene.Camera.FarPlane);
                data.Write<float>(subset.Material.Specular);
                data.Write<float>(subset.Material.Shininess);

                data.Position = 0;

                PooledResource<SlimDX.Direct3D11.Buffer> buffer;

                buffer = device.CreateConstantBuffer(data);

                buffer.Lock();

                device.SetVertexConstantBuffer(buffer.Value, 0);
                device.SetPixelConstantBuffer(buffer.Value, 0);


                //render
                device.Render((DirectX11Mesh)subset.Mesh);
#if DEBUG
                triangles += subset.Mesh.PrimitiveCount;
                drawcalls++;
#endif

                buffer.Unlock();

            }
 
            

            #endregion

            #region Instance

            device.SetVertexShader((DirectX11VertexShader)geometryvertex[(int)RenderVertexFlags.Instance]);

            foreach (GraphicsEngineBase.ObjectInstance obj in scene.Instances)
            {
                if ((obj.RenderFilter & RenderFilter.No_Render) == 0)
                {
                    foreach (var subset in obj.Subsets)
                    {

                        device.SetPixelShader((DirectX11PixelShader)geometrypixel[(int)obj.RenderFlags]);

                        //set material


                        if ((obj.RenderFlags & RenderFlags.DiffuseMap) == RenderFlags.DiffuseMap)
                        {

                            DirectX11Texture diffuse = (DirectX11Texture)subset.Material.DiffuseTexture;
                            if (diffuse == null)
                                throw new Exception("Diffuse map not supplied.");
                            device.SetPixelTexture(diffuse.texture, 1);
                            device.SetSampler(sampler, 1);

                        }

                        if ((obj.RenderFlags & RenderFlags.BumpMap) == RenderFlags.BumpMap)
                        {

                            DirectX11Texture bump = (DirectX11Texture)subset.Material.BumpTexture;
                            if (bump == null)
                                throw new Exception("Bump map not supplied.");
                            device.SetPixelTexture(bump.texture, 0);
                            device.SetSampler(normalsampler, 0);
                        }

                        //set transforms and transpose them

                        SlimDX.DataStream data = new SlimDX.DataStream(56 * sizeof(float), true, true);

                        Matrix4x4 worldTranspose = Matrix4x4.Identity();

                        //world
                        data.WriteRange<float>(worldTranspose.data);
                        //proj
                        data.Write<SlimDX.Matrix>(projTranspose);
                        //view
                        data.Write<SlimDX.Matrix>(viewTranspose);
                        //diffuse constant
                        data.WriteRange<float>(new float[]{subset.Material.DiffuseConstant.X,subset.Material.DiffuseConstant.Y,
                            subset.Material.DiffuseConstant.Z});
                        data.Write<float>(scene.Camera.FarPlane);
                        data.Write<float>(subset.Material.Specular);
                        data.Write<float>(subset.Material.Shininess);

                        data.Position = 0;

                        ConstantBuffer buffer;

                        buffer = device.CreateConstantBuffer(data);

                        buffer.Lock();

                        device.SetVertexConstantBuffer(buffer.Value, 0);
                        device.SetPixelConstantBuffer(buffer.Value, 0);
                        
                        var subsphere = subset.Mesh.CalculateBoundingSphere();

                        DirectX11Instance instance = (DirectX11Instance) device.CreateInstance();
                        for (int i =0 ; i < obj.Instance.InstanceCount; i++)
                        {
                            var inst = obj.Instance.At(i);
                            Vector4 worldpos = Matrix4x4.Transform(inst, new Vector4(subsphere.Position.X,
                                subsphere.Position.Y, subsphere.Position.Z, 1));

                            SlimDX.Vector4 VSpos =
                                SlimDX.Vector4.Transform(new SlimDX.Vector4(worldpos.X, worldpos.Y, worldpos.Z, worldpos.W), view);

                            if (frustum.Test(new Vector3(VSpos.X, VSpos.Y, VSpos.Z), subsphere.Radius))
                                instance.Add(inst);

                        }

                        if (instance.InstanceCount > 0)
                        {
                            //render
                            device.Render((DirectX11Mesh)subset.Mesh, (DirectX11Instance)instance);
#if DEBUG
                            triangles += subset.Mesh.PrimitiveCount * instance.InstanceCount;
#endif
                            instance.Dispose();
                        }
                        buffer.Unlock();

                    }

                }

            }

            #endregion

            #region Terrain

            //render terrain
            {

                //device.SetVertexShader((DirectX11VertexShader)geometryvertex[(int)RenderVertexFlags.Terrain]);
                device.SetVertexShader(terrainvertex);

                foreach (GraphicsEngineBase.TerrainObject obj in scene.Terrain)
                {

                    //Sphere sphere = obj.Mesh.CalculateBoundingSphere();

                    //Vector4 worldpos = Matrix4x4.Transform(obj.Transform, new Vector4(sphere.Position.X,
                    //    sphere.Position.Y, sphere.Position.Z, 1));

                    //SlimDX.Vector4 VSpos =
                    //    SlimDX.Vector4.Transform(new SlimDX.Vector4(worldpos.X, worldpos.Y, worldpos.Z, worldpos.W), view);


                    //if (!frustum.Test(new Vector3(VSpos.X, VSpos.Y, VSpos.Z), sphere.Radius))
                    //    continue;

                    device.SetPixelShader((DirectX11PixelShader)terrainpixel[(int)obj.Flags]);

                    //set material


                    if ((obj.Flags & RenderFlags.DiffuseMap) == RenderFlags.DiffuseMap)
                    {

                        Texture[] diffuse = obj.Material.BlendTextures;
                        if (diffuse[0] == null || diffuse[1] == null)
                            throw new Exception("Diffuse map not supplied.");
                        device.SetPixelTexture(((DirectX11Texture)diffuse[0]).texture, 2);
                        device.SetSampler(sampler, 2);

                        device.SetPixelTexture(((DirectX11Texture)diffuse[1]).texture, 3);

                        device.SetVertexTexture((DirectX11Texture)obj.BlendWeight, 4);
                    }

                    if ((obj.Flags & RenderFlags.BumpMap) == RenderFlags.BumpMap)
                    {

                        DirectX11Texture bump = (DirectX11Texture)obj.Material.BumpTexture;
                        if (bump == null)
                            throw new Exception("Bump map not supplied.");
                        device.SetPixelTexture(bump.texture, 0);
                        device.SetSampler(normalsampler, 0);
                    }

                    device.SetVertexTexture(obj.Heightmap, 1);
                    device.SetVertexSampler(normalsampler, 1);

                    //set transforms and transpose them

                    SlimDX.DataStream data = new SlimDX.DataStream(56 * sizeof(float), true, true);

                    Matrix4x4 transform = Matrix4x4.Scale(obj.Scale.X, obj.Scale.Y, obj.Scale.Z) *
                        Matrix4x4.Translate(obj.Position.X, obj.Position.Y, obj.Position.Z);

                    Matrix4x4 worldTranspose = Matrix4x4.Transpose(transform);

                    //world
                    data.WriteRange<float>(worldTranspose.data);
                    //proj
                    data.Write<SlimDX.Matrix>(projTranspose);
                    //view
                    data.Write<SlimDX.Matrix>(viewTranspose);
                    //diffuse constant
                    data.WriteRange<float>(new float[]{obj.Material.DiffuseConstant.X,obj.Material.DiffuseConstant.Y,
                    obj.Material.DiffuseConstant.Z});
                    data.Write<float>(scene.Camera.FarPlane);
                    data.Write<float>(obj.Material.Specular);
                    data.Write<float>(obj.Material.Shininess);

                    data.Position = 0;

                    PooledResource<SlimDX.Direct3D11.Buffer> buffer;

                    buffer = device.CreateConstantBuffer(data);

                    buffer.Lock();

                    device.SetVertexConstantBuffer(buffer.Value, 0);
                    device.SetPixelConstantBuffer(buffer.Value, 0);


                    //render
                    device.Render((DirectX11Mesh)obj.Mesh);
#if DEBUG
                    triangles += obj.Mesh.PrimitiveCount;
#endif

                    buffer.Unlock();

                }


            }

            #endregion

            #region Planes


            foreach (var plane in scene.Planes)
            {

                float radius = (float)Math.Sqrt(plane.Width * plane.Width + plane.Height * plane.Height) / 2;
                var centreVS = SlimDX.Vector3.TransformCoordinate(new SlimDX.Vector3(plane.Centre.X, plane.Centre.Y,
                    plane.Centre.Z), view);

                //if (frustum.Test(new GraphicsEngineMath.Vector3(centreVS.X, centreVS.Y, centreVS.Z), radius))
                {

                    //set shader
                    RenderFlags combined = plane.Flags;
                    if (plane.IsReflected)
                        combined |= RenderFlags.Reflection;

                    device.SetPixelShader((DirectX11PixelShader)geometrypixel[(int)combined]);
                    device.SetVertexShader((DirectX11VertexShader)geometryvertex[(int)RenderVertexFlags.Reflection]);




                    if ((plane.Flags & RenderFlags.DiffuseMap) == RenderFlags.DiffuseMap)
                    {

                        //set diffuse map
                        device.SetPixelTexture(((DirectX11Texture)plane.DiffuseMap).texture, 1);
                        device.SetSampler(sampler, 1);

                    }

                    ConstantBuffer buffer = null;

                    //if (plane.IsReflected)
                    {

                        //set reflection map

                        if (plane.IsReflected)
                        {
                            var map = reflections[plane];

                            device.SetPixelTexture(map.Target.Value, 2);

                        }
                        else
                            device.SetPixelTexture(((DirectX11Texture)plane.DiffuseMap).texture, 2);
                        device.SetSampler(reflectionsampler, 2);


                        //set constants

                        SlimDX.Matrix reflection_proj = plane.IsReflected ? reflections[plane].ProjTranspose : SlimDX.Matrix.Identity;

                        SlimDX.Vector3 centre = new SlimDX.Vector3(plane.Centre.X, plane.Centre.Y, plane.Centre.Z);

                        SlimDX.Matrix reflection_view = plane.IsReflected ? reflections[plane].ViewTranspose : SlimDX.Matrix.Identity;

                        SlimDX.Matrix reflection_view_proj = reflection_proj * reflection_view;


                        SlimDX.DataStream stream = new SlimDX.DataStream(sizeof(float) * (16 + 16 + 16 + 16 + 8 + 8), true, true);

                        for (int i = 0; i < 16; i++)
                            stream.Write<float>(0);
                        stream.Write<SlimDX.Matrix>(projTranspose);
                        stream.Write<SlimDX.Matrix>(viewTranspose);

                        stream.Write<SlimDX.Matrix>(reflection_view_proj);

                        stream.WriteRange<float>(new float[] { plane.Centre.X, plane.Centre.Y, plane.Centre.Z });
                        stream.Write<float>(plane.Width);
                        stream.WriteRange<float>(new float[] { plane.Plane.Normal.X, plane.Plane.Normal.Y,
                            plane.Plane.Normal.Z});
                        stream.Write<float>(plane.Height);

                        stream.WriteRange<float>(new float[] { 0, 0, 0 });
                        stream.WriteRange<float>(new float[] { scene.Camera.FarPlane, 10, 1 });

                        stream.Position = 0;

                        buffer = device.CreateConstantBuffer(stream);
                        buffer.Lock();

                        device.SetVertexConstantBuffer(buffer.Value, 0);
                        device.SetPixelConstantBuffer(buffer.Value, 0);

                    }

                    //render
                    device.Render((DirectX11Mesh)particlemesh);

                    if (plane.IsReflected)
                        buffer.Unlock();

                    //if (plane.IsReflected)
                    //    reflections[plane].Target.Unlock();

                }

            }



            #endregion

            
#if nDEBUG

            device.SetRasterState(false, false);

            #region Lights

            device.SetVertexShader((DirectX11VertexShader)geometryvertex[(int)RenderVertexFlags.None]);
            foreach (var light in scene.Lights)
            {
                if (light.LightType == LightType.Point)
                {

                    device.SetPixelShader((DirectX11PixelShader)geometrypixel[(int)RenderFlags.None]);

                    //set material
                    Vector3 dist = light.Position - scene.Camera.Position;

                    Vector3 colour = new Vector3(1, 0, 0);

                    dist = new Vector3(dist.X / light.Radius.X, dist.Y / light.Radius.Y, dist.Z / light.Radius.Z);

                    if (Vector3.Dot(dist, dist) < 1)
                    {
                        colour = new Vector3(0, 1, 0);
                    }

                    //set transforms and transpose them

                    SlimDX.DataStream data = new SlimDX.DataStream(56 * sizeof(float), true, true);

                    SlimDX.Matrix worldTranspose = SlimDX.Matrix.Scaling(light.Radius.X, light.Radius.Y, light.Radius.Z) *
                                SlimDX.Matrix.Translation(light.Position.X, light.Position.Y, light.Position.Z);
                    worldTranspose = SlimDX.Matrix.Transpose(worldTranspose);

                    //world
                    data.Write<SlimDX.Matrix>(worldTranspose);
                    //proj
                    data.Write<SlimDX.Matrix>(projTranspose);
                    //view
                    data.Write<SlimDX.Matrix>(viewTranspose);
                    //diffuse constant
                    data.WriteRange<float>(new float[] { colour.X, colour.Y, colour.Z });
                    data.Write<float>(scene.Camera.FarPlane);
                    data.Write<float>(0);
                    data.Write<float>(0);

                    data.Position = 0;

                    PooledResource<SlimDX.Direct3D11.Buffer> buffer;

                    buffer = device.CreateConstantBuffer(data);

                    buffer.Lock();

                    device.SetVertexConstantBuffer(buffer.Value, 0);
                    device.SetPixelConstantBuffer(buffer.Value, 0);


                    //render
                    device.Render((DirectX11Mesh)unitsphere);

                    buffer.Unlock();


                }


            }

            device.SetRasterState(true, true);


    #endregion
            
#endif

            #endregion
        }


        private class ParticleGroup
        {
            public float ScaleX { get; set; }
            public float ScaleY { get; set; }
            public Texture Texture { get; set; }
            public DirectX11Instance Instance { get; set; }
        }

        private void ParticlesRender(Scene scene, SlimDX.Matrix projTranspose, SlimDX.Matrix viewTranspose)
        {

            #region ParticleRender

            {
                //particles

                device.EnableZParticleBuffer(true);
                //device.EnableZBuffer(false);
                //device.EnableZBuffer(true);
                device.EnableParticleBlending(true);

                device.SetRenderTargets(particlemap);

                device.ClearRenderTargets(new SlimDX.Color4(0, 0, 0, 0));

                //naive implementation

                device.SetVertexShader(billboardvertex);
                device.SetPixelShader(billboardpixel);

                ConstantBuffer buffer0;

                {

                    //set constant buffer 0
                    SlimDX.DataStream data = new SlimDX.DataStream(32 * sizeof(float), true, true);

                    //proj
                    data.Write<SlimDX.Matrix>(projTranspose);

                    //view
                    data.Write<SlimDX.Matrix>(viewTranspose);

                    data.Position = 0;

                    buffer0 = device.CreateConstantBuffer(data);

                    buffer0.Lock();

                    device.SetVertexConstantBuffer(buffer0.Value, 0);



                }

                device.SetSampler(sampler, 0);

                Dictionary<string, ParticleGroup> instance = new Dictionary<string, ParticleGroup>();

                foreach (Particle p in scene.Particles)
                {
                    string hashcode = p.DiffuseTexture.GetHashCode() + p.Scale.X.ToString() + p.Scale.Y.ToString();

                    if (!instance.ContainsKey(hashcode))
                    {
                        instance.Add(hashcode, new ParticleGroup()
                        {
                            Instance = new DirectX11Instance(VertexType.InstanceParticle),
                            ScaleX = p.Scale.X,
                            ScaleY = p.Scale.Y,
                            Texture = p.DiffuseTexture
                        });
                    }
                    instance[hashcode].Instance.AddInstance(new Vertex(3).SetData(0, p.Position));
                }

                foreach (var particle in instance)
                {
                    //set constant buffer 1
                    SlimDX.DataStream data = new SlimDX.DataStream(16 * sizeof(float), true, true);

                    //position
                    //data.WriteRange<float>(new float[] { p.Position.X, p.Position.Y, p.Position.Z, 1 });
                    data.WriteRange<float>(new float[] { 0, 0, 0, 1 });
                    //campos
                    data.WriteRange<float>(new float[] { scene.Camera.Position.X, scene.Camera.Position.Y, scene.Camera.Position.Z, 1 });
                    //scale
                    //data.WriteRange<float>(new float[] { p.Scale.X, p.Scale.Y, 0, p.Opacity });
                    data.WriteRange<float>(new float[] { particle.Value.ScaleX, particle.Value.ScaleY, 0,
                        scene.Particles[0].Opacity });

                    data.Position = 0;

                    ConstantBuffer buffer;

                    buffer = device.CreateConstantBuffer(data);

                    buffer.Lock();

                    device.SetVertexConstantBuffer(buffer.Value, 1);


                    device.SetPixelTexture(((DirectX11Texture)scene.Particles[0].DiffuseTexture).texture, 0);

                    device.Render((DirectX11Mesh)particlemesh, particle.Value.Instance);

#if DEBUG
                    triangles += 2 * particle.Value.Instance.InstanceCount;
#endif

                    buffer.Unlock();
                    particle.Value.Instance.Dispose();
                }

                buffer0.Unlock();

            }

            device.SetRasterState(scene.BackFaceCulling, true);

            device.EnableParticleBlending(false);

            #endregion
        }



    }
}
