﻿/* 
 *
 * The linear depth is based on the article:
 * http://mynameismjp.wordpress.com/2009/03/10/reconstructing-position-from-depth/
 * 
 * The deferred rendering is based on the articles:
 * http://www.catalinzima.com/tutorials/deferred-rendering-in-xna/
 * 
 * 
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConstantBuffer = GraphicsEngineBase.PooledResource<SlimDX.Direct3D11.Buffer>;
using GraphicsEngineBase;
using GraphicsEngineMath;

namespace GraphicsEngineDX11
{
    internal partial class DirectX11Renderer
    {


        private void LightsRender(Scene scene, Frustum frustum, SlimDX.Matrix view, SlimDX.Matrix projTranspose, SlimDX.Matrix viewTranspose, System.Collections.Generic.Dictionary<Light, ShadowMap> shadowcollection)
        {

            #region LightRender

            //light passes
            lightmap = renderTargetPool[device.Width, device.Height,
                scene.HDRLighting ? RenderTargetFlags.HDR : RenderTargetFlags.None];

            lightmap.Lock();

            device.EnableZBuffer(true);
            device.SetRenderTargets(lightmap.Value);
            
            
            

            if (scene.Lights.Count == 0)
            {

                device.Clear(new SlimDX.Color4(0, 1, 1, 1));

            }
            else
            {

                device.Clear(new SlimDX.Color4(0, 0, 0, 0));

                device.EnableLightBlending(true);


                foreach (Light light in scene.Lights)
                {

                    if (light.LightType == LightType.Directional)
                    {

                        ShadowMap shadowmap=null;

                        if (light.IsShadowCaster)
                        {
                            shadowmap=shadowcollection[light];
                            shadowmap.shadowtarget.Unlock();
                        }

                        device.EnableZBuffer(false);

                        LightingFlags flags = LightingFlags.None;
                        if (light.IsShadowCaster)
                            flags |= LightingFlags.Shadow;
                        //if (scene.HDRLighting)
                        //    flags |= LightingFlags.HDR;

                        device.SetVertexShader(directionalLightvertex);
                        device.SetPixelShader((DirectX11PixelShader) directionalLightpixel[(int)flags]);

                        if (light.IsShadowCaster)
                        {
                            device.SetPixelTexture(shadowcollection[light].shadowtarget.Value, 2);
                            device.SetSampler(shadowsampler, 2);
                        }

                        device.SetSampler(sampler, 0);
                        device.SetSampler(sampler, 1);

                        device.SetPixelTexture(normalmap, 0);
                        device.SetPixelTexture(depthmap, 1);

                        //set light view matrix
                        SlimDX.Matrix lightView = light.IsShadowCaster ? shadowmap.lightview : SlimDX.Matrix.Identity;
                        SlimDX.Matrix lightViewTranspose = light.IsShadowCaster ? shadowmap.lightviewtranspose :
                            SlimDX.Matrix.Identity;

                        //set light proj matrix
                        SlimDX.Matrix lightProj = light.IsShadowCaster ? shadowmap.lightproj : SlimDX.Matrix.Identity;
                        SlimDX.Matrix lightProjTranspose = light.IsShadowCaster ? shadowmap.lightprojtranspose
                            : SlimDX.Matrix.Identity;

                        SlimDX.DataStream frustumdata = new SlimDX.DataStream(4 * 4 * sizeof(float), true, true);

                        for (int i = 0; i < 4; i++)
                            frustumdata.WriteRange<float>(new float[] { frustum.Corners[i].X, frustum.Corners[i].Y,
                                frustum.Corners[i].Z, 0});

                        frustumdata.Position = 0;

                        ConstantBuffer fbuffer = device.CreateConstantBuffer(frustumdata);

                        fbuffer.Lock();

                        SlimDX.DataStream data = new SlimDX.DataStream(((16 * 5) + 4) * sizeof(float), true, true);


                        data.WriteRange<float>(new float[] { scene.Camera.Position.X, scene.Camera.Position.Y, scene.Camera.Position.Z, 1 });

                        data.Write<SlimDX.Vector3>(SlimDX.Vector3.TransformCoordinate(new SlimDX.Vector3(
                            light.Position.X, light.Position.Y, light.Position.Z), view));

                        data.Write<float>(1);
                        data.Write<SlimDX.Vector3>(SlimDX.Vector3.TransformNormal(new SlimDX.Vector3(
                            light.Direction.X, light.Direction.Y, light.Direction.Z), view));

                        data.Write<float>(1);

                        data.WriteRange<float>(new float[] { light.Color.X, light.Color.Y, light.Color.Z, 1 });

                        data.Write<SlimDX.Matrix>(SlimDX.Matrix.Identity);
                        data.Write<SlimDX.Matrix>(SlimDX.Matrix.Identity);
                        data.WriteRange<float>(new float[] { 0, 0, 0, 0 });


                        data.Write<SlimDX.Matrix>(SlimDX.Matrix.Transpose(SlimDX.Matrix.Invert(view)));
                        data.Write<SlimDX.Matrix>(lightProjTranspose * lightViewTranspose);


                        data.Position = 0;

                        ConstantBuffer buffer = device.CreateConstantBuffer(data);

                        buffer.Lock();

                        device.SetPixelConstantBuffer(buffer.Value, 0);
                        device.SetPixelConstantBuffer(fbuffer.Value, 1);
                        device.SetVertexConstantBuffer(buffer.Value, 0);
                        device.SetVertexConstantBuffer(fbuffer.Value, 1);

                        device.Render((DirectX11Mesh)frustumquad);

                        fbuffer.Unlock();
                        buffer.Unlock();

                    }
                    else if (light.LightType == LightType.Point)
                    {
                        device.EnableZBuffer(false);

                        Vector3 dist = light.Position - scene.Camera.Position;

                        //if (dist.Length() * dist.Length() < light.Radius.X * light.Radius.X)
                        //{
                        //    device.SetRasterState(false, true);
                        //}

                        dist = new Vector3(dist.X / light.Radius.X, dist.Y / light.Radius.Y, dist.Z / light.Radius.Z);

                        if (Vector3.Dot(dist, dist) < 1)
                        {
                            device.SetRasterState(false, true);
                        }

                        //if (dist.X * dist.X / (light.Radius.X * light.Radius.X)
                        //    + dist.Y * dist.Y / (light.Radius.Y * light.Radius.Y) 
                        //    + dist.Z * dist.Z / (light.Radius.Z * light.Radius.Z) < 1)
                        //{
                        //    device.SetRasterState(false, true);
                        //}

                        device.SetVertexShader(pointLightvertex);
                        device.SetPixelShader(pointLightpixel);

                        device.SetSampler(sampler, 0);
                        device.SetSampler(sampler, 1);

                        device.SetPixelTexture(normalmap, 0);
                        device.SetPixelTexture(depthmap, 1);

                        SlimDX.Matrix worldTranspose =  SlimDX.Matrix.Scaling(light.Radius.X, light.Radius.Y, light.Radius.Z)*
                            SlimDX.Matrix.Translation(light.Position.X, light.Position.Y, light.Position.Z);
                        worldTranspose = SlimDX.Matrix.Transpose(worldTranspose);

                        SlimDX.DataStream data = new SlimDX.DataStream(16 * 4 * sizeof(float), true, true);


                        data.WriteRange<float>(new float[] { scene.Camera.Position.X, scene.Camera.Position.Y, scene.Camera.Position.Z, 1 });

                        //data.Write<SlimDX.Vector3>(SlimDX.Vector3.TransformCoordinate(new SlimDX.Vector3(
                        //    light.Position.X, light.Position.Y, light.Position.Z), view));
                        data.Write<SlimDX.Vector3>(new SlimDX.Vector3(
                            light.Position.X-scene.Camera.Position.X, light.Position.Y-scene.Camera.Position.Y,
                            light.Position.Z-scene.Camera.Position.Z));

                        data.Write<float>(1);
                        //data.Write<SlimDX.Vector3>(SlimDX.Vector3.TransformNormal(new SlimDX.Vector3(
                        //    light.Radius.X, light.Radius.Y, light.Radius.Z), view));
                        data.Write<SlimDX.Vector3>(new SlimDX.Vector3(
                           light.Radius.X, light.Radius.Y, light.Radius.Z));

                        data.Write<float>(1);

                        data.WriteRange<float>(new float[] { light.Color.X, light.Color.Y, light.Color.Z, 1 });
                        //data.Write<SlimDX.Matrix>(inverseViewProj);
                        data.Write<SlimDX.Matrix>(projTranspose * viewTranspose * worldTranspose);
                        data.Write<SlimDX.Matrix>(viewTranspose * worldTranspose);
                        data.Write<float>(scene.Camera.FarPlane);

                        data.Position = 0;

                        ConstantBuffer buffer = device.CreateConstantBuffer(data);
                        buffer.Lock();

                        device.SetVertexConstantBuffer(buffer.Value, 0);
                        device.SetPixelConstantBuffer(buffer.Value, 0);



                        device.Render((DirectX11Mesh)unitsphere);
                        buffer.Unlock();

                        //reset
                        device.SetRasterState(true, true);

                    }
                    else if (light.LightType == LightType.Spot)
                    {

                        device.EnableZBuffer(true);

                        device.SetVertexShader(spotLightvertex);
                        device.SetPixelShader(spotLightpixel);

                        device.SetSampler(sampler, 0);
                        device.SetSampler(sampler, 1);

                        device.SetPixelTexture(normalmap, 0);
                        device.SetPixelTexture(depthmap, 1);

                        SlimDX.Matrix worldTranspose = SlimDX.Matrix.Scaling(light.Radius.X, light.Radius.Y, light.Radius.Z) *
                            SlimDX.Matrix.Translation(light.Position.X, light.Position.Y, light.Position.Z);
                        worldTranspose = SlimDX.Matrix.Transpose(worldTranspose);

                        SlimDX.DataStream data = new SlimDX.DataStream(16 * 4 * sizeof(float), true, true);


                        data.WriteRange<float>(new float[] { scene.Camera.Position.X, scene.Camera.Position.Y, scene.Camera.Position.Z, 1 });

                        data.Write<SlimDX.Vector3>(SlimDX.Vector3.TransformCoordinate(new SlimDX.Vector3(
                            light.Position.X, light.Position.Y, light.Position.Z), view));

                        data.Write<float>(1);
                        data.Write<SlimDX.Vector3>(SlimDX.Vector3.TransformNormal(new SlimDX.Vector3(
                            light.Direction.X, light.Direction.Y, light.Direction.Z), view));

                        data.Write<float>(1);

                        data.WriteRange<float>(new float[] { light.Color.X, light.Color.Y, light.Color.Z, 1 });
                        //data.Write<SlimDX.Matrix>(inverseViewProj);
                        data.Write<SlimDX.Matrix>(projTranspose * viewTranspose * worldTranspose);
                        data.Write<SlimDX.Matrix>(viewTranspose * worldTranspose);
                        data.Write<float>(scene.Camera.FarPlane);

                        data.Position = 0;

                        ConstantBuffer buffer = device.CreateConstantBuffer(data);
                        buffer.Lock();

                        device.SetVertexConstantBuffer(buffer.Value, 0);
                        device.SetPixelConstantBuffer(buffer.Value, 0);


                        device.Render((DirectX11Mesh)unitcone);

                        buffer.Unlock();

                    }

                }


                device.EnableLightBlending(false);
            }

            #endregion


            //apply hdr pipeline
            if (scene.HDRLighting)
            {
                //calculate adaptive luminace
                
                //optional : bloom

                //apply tone mapping operator


                Chain chain = new Chain();
                Effect hdrEffect = new Effect();
                hdrEffect.passes.Enqueue(new Pass(PassFunction.Luminance, new PassInput[] { PassInput.Lightmap }));
                int width = device.Width / 2; int height = height = device.Height / 2;
                //really should make sure it is 1x1
                while (width > 1 || height > 1)
                {

                    hdrEffect.passes.Enqueue(new Pass(PassFunction.Luminance_Downsize, new PassInput[] { PassInput.Pass }));
                    width /= 4; height /= 4;
                }

                
                hdrEffect.passes.Enqueue(new Pass(PassFunction.Tonemap, new PassInput[] { PassInput.Lightmap, PassInput.Pass }));

                chain.AppendEffect(hdrEffect);

                var x = renderTargetPool[device.Width, device.Height];
                x.Lock();
                PostProcess(x, chain, false);
                //x.Unlock();
                lightmap.Unlock();
                lightmap = x;
                lightmap.Lock();


            }


        }

        

    }
}
