﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConstantBuffer = GraphicsEngineBase.PooledResource<SlimDX.Direct3D11.Buffer>;
using GraphicsEngineBase;
using GraphicsEngineMath;

namespace GraphicsEngineDX11
{
    internal partial class DirectX11Renderer
    {

        private class ShadowMap
        {
            public PooledResource<SlimDX.Direct3D11.RenderTargetView> shadowtarget;
            public SlimDX.Matrix lightproj;
            public SlimDX.Matrix lightprojtranspose;
            public SlimDX.Matrix lightview;
            public SlimDX.Matrix lightviewtranspose;
        }

        private void CalculateLightMatrix(Camera camera, SlimDX.Matrix inverseView, Light light, 
            ShadowMap map)
        {

            SlimDX.Vector3 lightdir = new SlimDX.Vector3(light.Direction.X, light.Direction.Y, light.Direction.Z);
            Frustum frustum = camera.CalculateFrustumVS();
            SlimDX.Vector3[] corners = new SlimDX.Vector3[8];
            
            SlimDX.Matrix transform = SlimDX.Matrix.LookAtLH(new SlimDX.Vector3(0, 0, 0),
                lightdir, new SlimDX.Vector3(0, 1, 0));

            for(int i = 0; i < corners.Length; i++)
            {

                SlimDX.Vector3 v = new SlimDX.Vector3(frustum.Corners[i].X, frustum.Corners[i].Y, frustum.Corners[i].Z);
                corners[i] = SlimDX.Vector3.TransformCoordinate(v, inverseView);
                corners[i] = SlimDX.Vector3.TransformCoordinate(corners[i], transform);

            }

            Box b = new Box();
            foreach (var vec in corners)
                b.Union(new Vector3(vec.X, vec.Y, vec.Z));

            SlimDX.Matrix inverseTransform = SlimDX.Matrix.Invert(transform);

            SlimDX.Vector3 mid =
                (new SlimDX.Vector3(b.Min.X, b.Min.Y, b.Min.Z) + new SlimDX.Vector3(b.Max.X, b.Max.Y, b.Max.Z)) / 2;

            mid = SlimDX.Vector3.TransformCoordinate(mid, inverseTransform);


            SlimDX.Matrix lightview= SlimDX.Matrix.LookAtLH(mid, mid + lightdir, new SlimDX.Vector3(0,1,0));
            SlimDX.Matrix lightmat = SlimDX.Matrix.OrthoLH(b.Max.X - b.Min.X, b.Max.Z - b.Min.Z, 0.1f,
                b.Max.Z);

            map.lightproj = lightmat;
            map.lightview = lightview;

        }

        private void ShadowMapsRender(Scene scene, Frustum frustum, SlimDX.Matrix inverseView, System.Collections.Generic.Dictionary<Light, ShadowMap> shadowcollection)
        {
            
            Box box = new Box();
            SlimDX.Vector3 centre = new SlimDX.Vector3();

            if (scene.ShadowStrategy == ShadowRenderStrategy.FitToScene)
            {

                //calculate bounds of the scene
                box.Union(scene.Camera.Position);

                foreach (var obj in scene.Objects)
                {
                    if ((obj.Filter & RenderFilter.No_Shadow) == 0)
                    {
                        foreach (var subset in obj.Subsets)
                        {

                            Box bounds = subset.Mesh.CalculateBoundingBox();
                            Vector4 v1 = Matrix4x4.Transform(obj.Transform, new Vector4(bounds.Min.X, bounds.Min.Y, bounds.Min.Z, 1));
                            box.Union(new Vector3(v1.X, v1.Y, v1.Z));
                            Vector4 v2 = Matrix4x4.Transform(obj.Transform, new Vector4(bounds.Max.X, bounds.Max.Y, bounds.Max.Z, 1));
                            box.Union(new Vector3(v2.X, v2.Y, v2.Z));

                        }
                    }
                }

                foreach (var obj in scene.Instances)
                {
                    if ((obj.RenderFilter & RenderFilter.No_Shadow) == 0)
                    {
                        foreach (var subset in obj.Subsets)
                        {

                            Box bounds = subset.Mesh.CalculateBoundingBox();

                            for (int i = 0; i < obj.Instance.InstanceCount; i++)
                            {
                                Matrix4x4 m = new Matrix4x4() { data = obj.Instance.At(i).data };

                                Vector4 v1 = Matrix4x4.Transform(m, new Vector4(bounds.Min.X, bounds.Min.Y, bounds.Min.Z, 1));
                                box.Union(new Vector3(v1.X, v1.Y, v1.Z));
                                Vector4 v2 = Matrix4x4.Transform(m, new Vector4(bounds.Max.X, bounds.Max.Y, bounds.Max.Z, 1));
                                box.Union(new Vector3(v2.X, v2.Y, v2.Z));
                            }

                        }
                    }
                }

            }
            else if (scene.ShadowStrategy == ShadowRenderStrategy.FitToCamera)
            {

                SlimDX.Vector3[] corners = new SlimDX.Vector3[8];

                for (int i = 0; i < corners.Length; i++)
                {

                    SlimDX.Vector3 v = new SlimDX.Vector3(frustum.Corners[i].X, frustum.Corners[i].Y, frustum.Corners[i].Z);
                    corners[i] = SlimDX.Vector3.TransformCoordinate(v, inverseView);
                    box.Union(new Vector3(corners[i].X, corners[i].Y, corners[i].Z));
                }

            }

            centre.X = (box.Max.X - box.Min.X) / 2.0f;
            centre.Y = (box.Max.Y - box.Min.Y) / 2.0f;
            centre.Z = (box.Max.Z - box.Min.Z) / 2.0f;

            #region ShadowMapRender

            foreach (var light in scene.Lights)
            {
                if (light.IsShadowCaster)
                {
                    if (light.LightType == LightType.Directional)
                    {


                        ShadowMap shadowTarget = new ShadowMap()
                        {
                            shadowtarget = renderTargetPool[1024, 1024, RenderTargetFlags.DepthTarget]
                        };
                        shadowcollection.Add(light, shadowTarget);

                        shadowTarget.shadowtarget.Lock();

                        device.SetRenderTargets(shadowTarget.shadowtarget.Value);
                        device.Clear(new SlimDX.Color4(1, 1, 1, 0));

                        light.Direction.Normalize();

                        Vector3 tempLightPos = /*scene.Camera.Position*/ new Vector3(centre.X, centre.Y, centre.Z)
                            + -light.Direction * 10; //(scene.Camera.FarPlane - scene.Camera.NearPlane);

                        //set light view matrix
                        shadowTarget.lightview = SlimDX.Matrix.LookAtLH(new SlimDX.Vector3(tempLightPos.X,
                            tempLightPos.Y, tempLightPos.Z),
                            //new SlimDX.Vector3(scene.Camera.Position.X, scene.Camera.Position.Y, scene.Camera.Position.Z),
                            centre,
                            new SlimDX.Vector3(0, 1, 0));


                        SlimDX.Vector3 min = SlimDX.Vector3.TransformCoordinate(
                            new SlimDX.Vector3(box.Min.X, box.Min.Y, box.Min.Z),
                            shadowTarget.lightview);
                        SlimDX.Vector3 max = SlimDX.Vector3.TransformCoordinate(
                            new SlimDX.Vector3(box.Max.X, box.Max.Y, box.Max.Z),
                            shadowTarget.lightview);

                        float minsize = 25; //25

                        //set light proj matrix
                        shadowTarget.lightproj =
                            //    SlimDX.Matrix.OrthoLH(Math.Max(50, Math.Abs(max.X-min.X)),
                            //    Math.Max(50, Math.Abs(max.Y-min.Y)), 1, 2000);

                            SlimDX.Matrix.OrthoOffCenterLH(Math.Min(-minsize, Math.Min(min.X, max.X)),
                            Math.Max(minsize, Math.Max(min.X, max.X)),
                            Math.Min(-minsize, Math.Min(min.Y, max.Y)),
                            Math.Max(minsize, Math.Max(min.Y, max.Y)), 1, 1000);

                            //SlimDX.Matrix.PerspectiveFovLH((float)Math.PI / 2, 1.0f / 1.0f, 1, 500);


                        //CalculateLightMatrix(scene.Camera, inverseView, light, shadowTarget);

                        shadowTarget.lightviewtranspose = SlimDX.Matrix.Transpose(shadowTarget.lightview);
                        shadowTarget.lightprojtranspose = SlimDX.Matrix.Transpose(shadowTarget.lightproj);

                        device.SetPixelShader(shadowpixel);
                        device.SetVertexShader((DirectX11VertexShader)shadowvertex[(int)RenderVertexFlags.None]);

                        //render ALL scene to shadow map

                        #region Objects

                        foreach (var obj in scene.Objects)
                        {
                            if ((obj.Filter & RenderFilter.No_Shadow) == 0)
                            {
                                foreach (var subset in obj.Subsets)
                                {

                                    //set material

                                    //set transforms and transpose them

                                    SlimDX.DataStream data = new SlimDX.DataStream(48 * sizeof(float), true, true);

                                    Matrix4x4 worldTranspose = Matrix4x4.Transpose(obj.RootTransform * obj.Transform *
                                        subset.Transform);

                                    //world
                                    data.WriteRange<float>(worldTranspose.data);
                                    //proj
                                    data.Write<SlimDX.Matrix>(shadowTarget.lightprojtranspose);
                                    //view
                                    data.Write<SlimDX.Matrix>(shadowTarget.lightviewtranspose);

                                    data.Position = 0;

                                    PooledResource<SlimDX.Direct3D11.Buffer> buffer;

                                    buffer = device.CreateConstantBuffer(data);

                                    buffer.Lock();

                                    device.SetVertexConstantBuffer(buffer.Value, 0);
                                    device.SetPixelConstantBuffer(buffer.Value, 0);


                                    //render
                                    device.Render((DirectX11Mesh)subset.Mesh);

                                    buffer.Unlock();
                                }
                            }
                        }

                        #endregion

                        device.SetPixelShader(shadowpixel);
                        device.SetVertexShader((DirectX11VertexShader)shadowvertex[(int)RenderVertexFlags.Instance]);

                        #region Instance

                        foreach (var obj in scene.Instances)
                        {
                            if ((obj.RenderFilter & RenderFilter.No_Shadow) == 0)
                            {
                                foreach (var subset in obj.Subsets)
                                {
                                    //set material

                                    //set transforms and transpose them

                                    SlimDX.DataStream data = new SlimDX.DataStream(48 * sizeof(float), true, true);

                                    //world
                                    data.Write<SlimDX.Matrix>(SlimDX.Matrix.Identity);
                                    //proj
                                    data.Write<SlimDX.Matrix>(shadowTarget.lightprojtranspose);
                                    //view
                                    data.Write<SlimDX.Matrix>(shadowTarget.lightviewtranspose);

                                    data.Position = 0;

                                    PooledResource<SlimDX.Direct3D11.Buffer> buffer;

                                    buffer = device.CreateConstantBuffer(data);

                                    buffer.Lock();

                                    device.SetVertexConstantBuffer(buffer.Value, 0);
                                    device.SetPixelConstantBuffer(buffer.Value, 0);


                                    //render
                                    device.Render((DirectX11Mesh)subset.Mesh, (DirectX11Instance)obj.Instance);

                                    buffer.Unlock();

                                }
                            }
                        }

                        #endregion

                        foreach (var Object in scene.Particles)
                        {

                        }

                    }
                }
            }


            #endregion
        }





    }
}
