﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GraphicsEngineBase;
using ConstantBuffer = GraphicsEngineBase.PooledResource<SlimDX.Direct3D11.Buffer>;

namespace GraphicsEngineDX11
{
    internal partial class DirectX11Renderer
    {


        private void PostProcess(PooledResource<SlimDX.Direct3D11.RenderTargetView> final, Chain chain, bool tobackbuffer)
        {

            #region PostProcess

            //post-processing

            if (chain != null)
            {
                device.EnableZBuffer(false);

                Queue<PooledResource<SlimDX.Direct3D11.RenderTargetView>> targets
                    = new Queue<PooledResource<SlimDX.Direct3D11.RenderTargetView>>();

                foreach (var effect in chain.Effects)
                {

                    var passes = new Queue<Pass>(effect.passes);

                    while (passes.Count > 0)
                    {

                        var pass = passes.Dequeue();

                        PooledResource<SlimDX.Direct3D11.RenderTargetView> passtarget = null;

                        if (passes.Count > 0)
                        {

                            SlimDX.DXGI.SurfaceDescription surface;

                            if (targets.Count > 0)
                                surface = targets.Peek().Value.Resource.AsSurface().Description;
                            else
                                surface = final.Value.Resource.AsSurface().Description;

                            if (pass.function == PassFunction.Downsize)
                            {
                                passtarget = renderTargetPool[surface.Width / 4, surface.Height / 4];
                            }
                            else if (pass.function == PassFunction.Upsize)
                            {
                                passtarget = renderTargetPool[surface.Width * 4, surface.Height * 4];
                            }
                            else if (pass.function == PassFunction.Luminance)
                            {
                                passtarget = renderTargetPool[surface.Width / 2, surface.Width / 2, RenderTargetFlags.HDR];
                            }
                            else if (pass.function == PassFunction.Luminance_Downsize)
                            {
                                passtarget = renderTargetPool[surface.Width / 4, surface.Height / 4, RenderTargetFlags.HDR];
                            }
                            else
                            {
                                passtarget = renderTargetPool[surface.Width, surface.Height];
                            }

                            device.SetRenderTargets(passtarget.Value);
                            targets.Enqueue(passtarget);
                            passtarget.Lock();
                        }
                        else
                        {
                            if (tobackbuffer)
                                device.SetDefaultRenderTarget();
                            else
                                device.SetRenderTargets(final.Value);
                        }

                        //create constant buffer

                        SlimDX.DataStream data = new SlimDX.DataStream(16 * sizeof(float), true, true);

                        ConstantBuffer buffer = device.CreateConstantBuffer(data);

                        buffer.Lock();

                        //select pixel/vertex shaders

                        device.SetVertexShader((DirectX11VertexShader)postprocessvertex[(int)pass.function]);

                        device.SetPixelShader((DirectX11PixelShader)postprocesspixel[(int)pass.function]);

                        //set texture samples
                        device.SetSampler(sampler, 0);

                        for (int i = 0; i < pass.inputs.Length; i++)
                        {
                            if (pass.inputs[i] == PassInput.Pass)
                            {

                                PooledResource<SlimDX.Direct3D11.RenderTargetView> resource =
                                    targets.Dequeue();


                                device.SetPixelTexture(resource.Value, i);

                                resource.Unlock();

                            }
                            else if (pass.inputs[i] == PassInput.Final)
                                device.SetPixelTexture(final.Value, i);
                            else if (pass.inputs[i] == PassInput.Depthmap)
                                device.SetPixelTexture(depthmap, i);
                            else if (pass.inputs[i] == PassInput.Diffusemap)
                                device.SetPixelTexture(diffusemap, i);
                            else if (pass.inputs[i] == PassInput.Normalmap)
                                device.SetPixelTexture(normalmap, i);
                            else if (pass.inputs[i] == PassInput.Lightmap)
                                device.SetPixelTexture(lightmap.Value, i);

                        }

                        //set constant buffer

                        //render fullscreen quad
                        device.Render((DirectX11Mesh)fullscreenquad);

                        buffer.Unlock();

                    }


                }

                final.Unlock();
                device.EnableZBuffer(true);

            }

            #endregion
        }



    }
}
