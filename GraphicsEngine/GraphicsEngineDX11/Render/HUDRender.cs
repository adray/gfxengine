﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GraphicsEngineBase;
using ConstantBuffer = GraphicsEngineBase.PooledResource<SlimDX.Direct3D11.Buffer>;
using GraphicsEngineMath;

namespace GraphicsEngineDX11
{
    internal partial class DirectX11Renderer
    {


        private void RenderQuads(Scene scene)
        {

            #region QuadOverlay

            device.EnableAlphaDrawing(false);

            device.EnableZBuffer(false);

            device.SetPixelShader(quadpixel);
            device.SetVertexShader(quadvertex);


            foreach (QuadObject obj in scene.Quads)
            {

                float sx = obj.Width / device.Width;
                float sy = obj.Height / device.Height;

                RenderQuad(obj.Position.X * 2 / device.Width + sx - 1, -obj.Position.Y * 2 / device.Height - sy + 1,
                    sx, sy, ((DirectX11Texture)obj.Texture).texture);

            }


            device.EnableZBuffer(true);


            #endregion

        }

        private void RenderDebugOverlay(System.Collections.Generic.Dictionary<Light, ShadowMap> shadowcollection,
            System.Collections.Generic.Dictionary<PlaneObject, GraphicsEngineDX11.Render.Reflection> reflections)
        {

#if DEBUG

            //can render debug overlay here

            device.EnableZBuffer(false);

            device.SetPixelShader(quadpixel);
            device.SetVertexShader(quadvertex);

            RenderQuad(-0.7f, 0.7f, normalmap);

            RenderQuad(-0.7f, 0.2f, diffusemap);

            RenderQuad(-0.7f, -0.3f, lightmap.Value);

            RenderQuad(-0.2f, 0.7f, particlemap);

            RenderQuad(0.3f, 0.7f, depthmap);

            if (shadowcollection.Count > 0)
                RenderQuad(0.8f, 0.7f, shadowcollection.Values.First().shadowtarget.Value);

            if (reflections.Count > 0)
                RenderQuad(0.8f, 0.2f, reflections.Values.First().Target.Value);

            device.EnableAlphaDrawing(true);

            device.SetPixelShader(fontpixel);
            device.SetVertexShader(fontvertex);

            fontengine.MakeText(infotext.Text);

            {

                SlimDX.DataStream data = new SlimDX.DataStream(16 * sizeof(float), true, true);

                //postion
                data.WriteRange<float>(new float[] { infotext.Position.X, infotext.Position.Y, device.Width, device.Height });
                //color
                data.WriteRange<float>(new float[] { infotext.Colour.X, infotext.Colour.Y, infotext.Colour.Z, infotext.Colour.W });

                data.Position = 0;

                ConstantBuffer buffer = device.CreateConstantBuffer(data);
                buffer.Lock();

                device.SetVertexConstantBuffer(buffer.Value, 0);
                device.SetPixelConstantBuffer(buffer.Value, 0);

                //bind textures

                device.SetSampler(sampler, 0);

                device.SetPixelTexture(((DirectX11Texture)fontengine.FontTexture).texture, 0);

                device.Render((DirectX11Mesh)fontengine.FontMesh);

                buffer.Unlock();

            }

            device.EnableAlphaDrawing(false);

#endif
        }

        private void RenderText(Scene scene)
        {

            #region Text

            //draw user-defined text
            device.EnableAlphaDrawing(true);


            device.SetPixelShader(fontpixel);
            device.SetVertexShader(fontvertex);

            foreach (var text in scene.Text)
            {

                Vector2 rect = fontengine.MakeText(text.Text);

                SlimDX.DataStream data = new SlimDX.DataStream(16 * sizeof(float), true, true);

                Vector2 pos = text.Position;
                if (text.RenderOptions == TextRenderingOptions.Centered)
                    pos -= 0.5f * rect;

                //postion
                data.WriteRange<float>(new float[] { pos.X, -pos.Y, device.Width, device.Height });
                //color
                data.WriteRange<float>(new float[] { text.Colour.X, text.Colour.Y, text.Colour.Z, text.Colour.W });

                data.Position = 0;

                ConstantBuffer buffer = device.CreateConstantBuffer(data);
                buffer.Lock();

                device.SetVertexConstantBuffer(buffer.Value, 0);
                device.SetPixelConstantBuffer(buffer.Value, 0);

                //bind textures

                device.SetSampler(sampler, 0);

                device.SetPixelTexture(((DirectX11Texture)fontengine.FontTexture).texture, 0);

                device.Render((DirectX11Mesh)fontengine.FontMesh);

                buffer.Unlock();

            }

            #endregion
        }


    }
}
