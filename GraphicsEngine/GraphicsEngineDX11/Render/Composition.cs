﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConstantBuffer = GraphicsEngineBase.PooledResource<SlimDX.Direct3D11.Buffer>;
using GraphicsEngineBase;

namespace GraphicsEngineDX11
{
    internal partial class DirectX11Renderer
    {



        private PooledResource<SlimDX.Direct3D11.RenderTargetView> CompositionRender(Scene scene)
        {

            #region Composition

            //final composition
            PooledResource<SlimDX.Direct3D11.RenderTargetView> target = null;

            device.EnableZBuffer(false);

            if (chain != null)
            {

                target = renderTargetPool[device.Width, device.Height];

                device.SetRenderTargets(target.Value);

                target.Lock();
            }
            else
            {
                //render straight to the back buffer
                device.SetDefaultRenderTarget();

            }

            device.Clear(new SlimDX.Color4(0, 0.5f, 0.5f, 0.5f));

            CompositionFlags flags = CompositionFlags.None;
            //if (scene.HDRLighting)
            //    flags |= CompositionFlags.HDR;

            device.SetPixelShader((DirectX11PixelShader) finalpixel[(int)flags]);
            device.SetVertexShader(finalvertex);

            {

                SlimDX.DataStream data = new SlimDX.DataStream(16 * sizeof(float), true, true);

                //ambient
                data.WriteRange<float>(new float[] { scene.Ambient.X, scene.Ambient.Y, scene.Ambient.Z, scene.Ambient.W });

                data.Position = 0;

                ConstantBuffer buffer = device.CreateConstantBuffer(data);

                buffer.Lock();

                device.SetPixelConstantBuffer(buffer.Value, 0);

                //bind textures

                device.SetSampler(sampler, 1);
                device.SetSampler(sampler, 0);

                device.SetPixelTexture(lightmap.Value, 1);
                device.SetPixelTexture(diffusemap, 0);


                device.Render((DirectX11Mesh)fullscreenquad);

                buffer.Unlock();


            }

            if (scene.Particles.Count > 0)
            {

                //device.EnableAlphaDrawing(true);
                device.EnableParticleBlending(true);

                device.SetPixelShader(quadpixel);
                device.SetVertexShader(quadvertex);

                RenderQuad(0, 0, 1, 1, particlemap);

                //device.EnableAlphaDrawing(false);
                device.EnableParticleBlending(false);

            }

            #endregion
            return target;
        }



    }
}
