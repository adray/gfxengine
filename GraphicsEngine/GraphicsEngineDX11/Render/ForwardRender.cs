﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SlimDX.Direct3D11;

namespace GraphicsEngineDX11.Render
{
    internal struct ReflectionRenderParams
    {

        public GraphicsEngineBase.Scene scene { get; set; }
        public DirectX11Device device { get; set; }
        public DirectX11RenderTargetPool pool { get; set; }
        public SamplerState TextureSampler { get; set; }
        public GraphicsEngineBase.Shader pixel { get; set; }
        public GraphicsEngineBase.Shader vertex { get; set; }
        public GraphicsEngineMath.Frustum Frustum { get; set; }
        public SlimDX.Matrix view { get; set; }

    }

    internal class Reflection
    {

        public GraphicsEngineBase.PooledResource<RenderTargetView> Target { get; set; }
        public SlimDX.Matrix ViewTranspose { get; set; }
        public SlimDX.Matrix ProjTranspose { get; set; }

    }


    internal struct ForwardRenderParams
    {

        public GraphicsEngineBase.Scene scene { get; set;} 
        public DirectX11Device device { get; set; }
        public DirectX11RenderTargetPool pool { get; set; }
        public SamplerState TextureSampler { get; set; }
        public GraphicsEngineBase.Shader pixel { get; set; }
        public GraphicsEngineBase.Shader vertex { get; set; }
        public SlimDX.Matrix ViewTranspose { get; set; }
        public SlimDX.Matrix ProjTranspose { get; set; }

    }

    internal class ReflectionRender
    {

        public Dictionary<GraphicsEngineBase.PlaneObject, Reflection> Render(ReflectionRenderParams reflection_param)
        {
            var forward = new ForwardRender();
            var param = new ForwardRenderParams();
            param.scene = reflection_param.scene;
            param.pool = reflection_param.pool;
            param.TextureSampler = reflection_param.TextureSampler;
            param.vertex = reflection_param.vertex;
            param.pixel = reflection_param.pixel;
            param.device = reflection_param.device;

            var dictonary = new Dictionary<GraphicsEngineBase.PlaneObject, Reflection>();

            foreach (var plane in reflection_param.scene.Planes)
            {
                float radius = (float)Math.Sqrt(plane.Width * plane.Width + plane.Height * plane.Height) / 2;
                var centreVS = SlimDX.Vector3.TransformCoordinate(new SlimDX.Vector3(plane.Centre.X, plane.Centre.Y,
                    plane.Centre.Z), reflection_param.view);

                if (plane.IsReflected /*&& reflection_param.Frustum.Test(
                    new GraphicsEngineMath.Vector3(centreVS.X, centreVS.Y, centreVS.Z), radius)*/)
                {

                    //calculate camera postion

                    param.ProjTranspose =
                        SlimDX.Matrix.Transpose(
                        //SlimDX.Matrix.OrthoLH(plane.Width*2, plane.Height*2, 1, 1024));
                        SlimDX.Matrix.PerspectiveFovLH((float)Math.PI / 4, 4.0f / 3.0f, param.scene.Camera.NearPlane, param.scene.Camera.FarPlane));

                    SlimDX.Vector3 centre = new SlimDX.Vector3(plane.Centre.X, plane.Centre.Y, plane.Centre.Z);

                    SlimDX.Vector3 normal = new SlimDX.Vector3(plane.Plane.Normal.X,
                            plane.Plane.Normal.Y, plane.Plane.Normal.Z);
                    normal.Normalize();

                    SlimDX.Vector3 campos = new SlimDX.Vector3(param.scene.Camera.Position.X,
                        param.scene.Camera.Position.Y, param.scene.Camera.Position.Z);
                    
                    SlimDX.Vector3 reflected = SlimDX.Vector3.Reflect(SlimDX.Vector3.Normalize(centre - campos), normal);
                    reflected.Normalize();

                    SlimDX.Vector3 r = SlimDX.Vector3.Distance(centre, campos) * -reflected + centre;

                    

                    param.ViewTranspose =
                        SlimDX.Matrix.Transpose(SlimDX.Matrix.LookAtLH(r, centre,
                            new SlimDX.Vector3(0, 1, 0)));

                    Reflection reflect = new Reflection()
                    {
                        Target = forward.Render(param),
                        ViewTranspose = param.ViewTranspose,
                        ProjTranspose = param.ProjTranspose
                    };

                    dictonary.Add(plane, reflect);

                }


            }



            return dictonary;

        }

    }

    internal class ForwardRender
    {

        public GraphicsEngineBase.PooledResource<SlimDX.Direct3D11.RenderTargetView>
            Render(ForwardRenderParams param)
        {

            DirectX11Device device = param.device;

            var target = param.pool[device.Width, device.Height];
            target.Lock();

            device.SetRenderTargets(target.Value);
            device.Clear(new SlimDX.Color4(1, 0, 0, 0));

            device.EnableZBuffer(true);

            #region Objects
            foreach (var obj in param.scene.Objects)
            {

                if ((obj.Filter & GraphicsEngineBase.RenderFilter.No_Reflection) == 0)
                {

                    foreach (var subset in obj.Subsets)
                    {



                        GraphicsEngineBase.RenderFlags combined = subset.RenderFlags & obj.Flags;

                        if ((combined & GraphicsEngineBase.RenderFlags.DiffuseMap) == GraphicsEngineBase.RenderFlags.DiffuseMap)
                        {
                            device.SetPixelTexture(((DirectX11Texture)subset.Material.DiffuseTexture).texture, 0);
                            device.SetSampler(param.TextureSampler, 0);
                        }

                        //device.SetPixelTexture(((DirectX11Texture)subset.Material.BumpTexture).texture, 1);

                        //set shader
                        device.SetVertexShader((DirectX11VertexShader)param.vertex[(int)GraphicsEngineBase.RenderVertexFlags.None]);
                        device.SetPixelShader((DirectX11PixelShader)param.pixel[(int)combined]);

                        //set constant buffer

                        GraphicsEngineMath.Matrix4x4 worldTranspose = GraphicsEngineMath.Matrix4x4.Transpose(
                            obj.RootTransform * obj.Transform * subset.Transform);
                        SlimDX.DataStream data = new SlimDX.DataStream(sizeof(float) * (16 + 16 + 16 + 4 + (5 * 4)), true, true);
                        data.WriteRange<float>(worldTranspose.data);
                        data.Write<SlimDX.Matrix>(param.ViewTranspose);
                        data.Write<SlimDX.Matrix>(param.ProjTranspose);
                        data.WriteRange<float>(new float[] { param.scene.Camera.Position.X, param.scene.Camera.Position.Y,
                        param.scene.Camera.Position.Z, 0});

                        //including lights
                        for (int i = 0; i < 5; i++)
                        {
                            if (i < param.scene.Lights.Count)
                            {
                                data.WriteRange<float>(new float[] { param.scene.Lights[i].Direction.X,
                                param.scene.Lights[i].Direction.Y, param.scene.Lights[i].Direction.Z});
                                data.Write<int>(0);
                            }
                            else
                            {
                                data.WriteRange<float>(new float[] { 0, 0, 0 });
                                data.Write<int>(-1);
                            }
                        }

                        data.Position = 0;

                        var constantbuffer = device.CreateConstantBuffer(data);
                        constantbuffer.Lock();

                        device.SetVertexConstantBuffer(constantbuffer.Value, 0);
                        device.SetPixelConstantBuffer(constantbuffer.Value, 0);

                        //draw
                        device.Render((DirectX11Mesh)subset.Mesh);

                        constantbuffer.Unlock();
                    }
                }
            }

            #endregion

            #region Instance

            foreach (var obj in param.scene.Instances)
            {
                if ((obj.RenderFilter & GraphicsEngineBase.RenderFilter.No_Reflection) == 0)
                {
                    foreach (var subset in obj.Subsets)
                    {

                        GraphicsEngineBase.RenderFlags combined = obj.RenderFlags;

                        if ((combined & GraphicsEngineBase.RenderFlags.DiffuseMap) == GraphicsEngineBase.RenderFlags.DiffuseMap)
                        {
                            device.SetPixelTexture(((DirectX11Texture)subset.Material.DiffuseTexture).texture, 0);
                            device.SetSampler(param.TextureSampler, 0);
                        }

                        //device.SetPixelTexture(((DirectX11Texture)subset.Material.BumpTexture).texture, 1);

                        //set shader
                        device.SetVertexShader((DirectX11VertexShader)param.vertex[(int)GraphicsEngineBase.RenderVertexFlags.Instance]);
                        device.SetPixelShader((DirectX11PixelShader)param.pixel[(int)combined]);

                        //set constant buffer


                        SlimDX.DataStream data = new SlimDX.DataStream(sizeof(float) * (16 + 16 + 16 + 4 + (5 * 4)), true, true);
                        data.Write<SlimDX.Matrix>(SlimDX.Matrix.Identity);
                        data.Write<SlimDX.Matrix>(param.ViewTranspose);
                        data.Write<SlimDX.Matrix>(param.ProjTranspose);
                        data.WriteRange<float>(new float[] { param.scene.Camera.Position.X, param.scene.Camera.Position.Y,
                        param.scene.Camera.Position.Z, 0});

                        //including lights
                        for (int i = 0; i < 5; i++)
                        {
                            if (i < param.scene.Lights.Count)
                            {
                                data.WriteRange<float>(new float[] { param.scene.Lights[i].Direction.X,
                                param.scene.Lights[i].Direction.Y, param.scene.Lights[i].Direction.Z});
                                data.Write<int>(0);
                            }
                            else
                            {
                                data.WriteRange<float>(new float[] { 0, 0, 0 });
                                data.Write<int>(-1);
                            }
                        }

                        data.Position = 0;

                        var constantbuffer = device.CreateConstantBuffer(data);
                        constantbuffer.Lock();

                        device.SetVertexConstantBuffer(constantbuffer.Value, 0);
                        device.SetPixelConstantBuffer(constantbuffer.Value, 0);

                        //draw
                        device.Render((DirectX11Mesh)subset.Mesh, (DirectX11Instance)obj.Instance);

                        constantbuffer.Unlock();

                    }

                }

            }

            #endregion

            //target.Unlock();

            return target;
        }


    }
}
