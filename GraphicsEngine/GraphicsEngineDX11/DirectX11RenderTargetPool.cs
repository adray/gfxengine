﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Pool = System.Collections.Generic.List<GraphicsEngineBase.PooledResource<SlimDX.Direct3D11.RenderTargetView>>;
using GraphicsEngineBase;

namespace GraphicsEngineDX11
{
    internal enum RenderTargetFlags
    {
        None=0,
        DepthTarget=1,
        HDR=2
    }

    internal class DirectX11RenderTargetPool : IDisposable, System.Collections.IEnumerable
    {

        private DirectX11Device device;
        private System.Collections.Hashtable hashtable;

        public DirectX11RenderTargetPool(DirectX11Device device)
        {
            hashtable = new System.Collections.Hashtable();
            this.device = device;
        }

        public PooledResource<SlimDX.Direct3D11.RenderTargetView> this[int width, int height, RenderTargetFlags flags]
        {
            get
            {

                int f = (((width << 10) + height) << 1) + (flags==RenderTargetFlags.DepthTarget ? 0 : 1) +
                    (flags==RenderTargetFlags.HDR ? 0 : 2);

                //lookup for unused resources

                Pool resources = (Pool)hashtable[f];

                if (resources == null)
                {
                    Pool pool = new Pool();
                    hashtable.Add(f, pool);
                }
                else
                {

                    foreach (var resource in resources)
                    {
                        if (!resource.IsUsed)
                            return resource;
                    }

                }

                //create another

                SlimDX.Direct3D11.RenderTargetView rendertarget=null;

                if (flags == RenderTargetFlags.DepthTarget)
                    rendertarget = device.CreateDepthRenderTarget();
                else if (flags == RenderTargetFlags.HDR)
                    rendertarget = device.CreateHDRRenderTarget(width, height);
                else
                    rendertarget = device.CreateRenderTarget(width, height);

                PooledResource<SlimDX.Direct3D11.RenderTargetView> ret =
                    new PooledResource<SlimDX.Direct3D11.RenderTargetView>(rendertarget);

                ((Pool)hashtable[f]).Add(ret);

                return ret;
            }
        }

        public PooledResource<SlimDX.Direct3D11.RenderTargetView> this[int width, int height]
        {
            get
            {

                return this[width, height, RenderTargetFlags.None];
            }
        }

        public void Dispose()
        {
            foreach (var pool in hashtable.Values)
            {
                foreach (GraphicsEngineBase.PooledResource<SlimDX.Direct3D11.RenderTargetView> resource in
                    ((System.Collections.Generic.List<GraphicsEngineBase.PooledResource<SlimDX.Direct3D11.RenderTargetView>>)pool))
                    resource.Value.Dispose();
            }
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return hashtable.Values.GetEnumerator();
        }
    }
}
