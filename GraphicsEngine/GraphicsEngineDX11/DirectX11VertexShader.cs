﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GraphicsEngineDX11
{
    internal class DirectX11VertexShader : IDisposable
    {

        public SlimDX.Direct3D11.VertexShader Shader { get; private set; }
        public SlimDX.D3DCompiler.ShaderSignature Signature { get; private set; }

        public DirectX11VertexShader(SlimDX.Direct3D11.VertexShader shader, SlimDX.D3DCompiler.ShaderSignature signature)
        {
            Shader = shader;
            Signature = signature;
        }

        public void Dispose()
        {
            Shader.Dispose();
        }
    }
}
