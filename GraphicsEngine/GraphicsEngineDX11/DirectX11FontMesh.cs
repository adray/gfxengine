﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GraphicsEngineBase;
using System.Drawing;

namespace GraphicsEngineDX11
{
    internal class DirectX11FontMesh : IDisposable
    {

        internal Mesh FontMesh { get; private set; }
        internal Texture FontTexture { get; private set; }
        private Rectangle[] offsets;

        internal DirectX11FontMesh(DirectX11Device device, System.IO.Stream definition,  System.IO.Stream texture )
        {


            this.FontTexture = device.LoadTexture(texture);

            FontMesh = new DirectX11Mesh(VertexType.Position);


            offsets = new Rectangle[200];

            System.IO.BinaryReader reader = new System.IO.BinaryReader(definition);
            
            int i = 0;

            while (reader.BaseStream.Position < reader.BaseStream.Length)
            {

                i = reader.ReadInt32();

                offsets[i] = new Rectangle(
                    reader.ReadInt32(),
                    reader.ReadInt32(),
                    reader.ReadInt32(),
                    reader.ReadInt32());
            }


            reader.Close();

        }

        public GraphicsEngineMath.Vector2 MakeText(string text)
        {

            FontMesh.Clear();

            return AppendText(text);

        }

        public GraphicsEngineMath.Vector2 AppendText(string text)
        {

            float maxwidth=0;
            float maxheight = 0;
            float x=0,y=0;

            for (int i = 0; i < text.Length; i++)
            {

                if (text[i] == '\n')
                {
                    maxwidth = Math.Max(maxwidth, x);
                    x = 0;
                    y -= 40;
                    continue;
                }

                if (text[i] == ' ')
                {
                    x += 30;
                    continue;
                }

                Rectangle r = offsets[text[i]];

                FontMesh.AddTriangle(new Triangle(new Vertex(4).SetData(0, x, y - r.Height, r.X / 1024f, (r.Y + r.Height) / 1024f),
                    new Vertex(4).SetData(0, x, y, r.X / 1024f, r.Y / 1024f),
                    new Vertex(4).SetData(0, x + r.Width, y, (r.X + r.Width) / 1024f, r.Y / 1024f)));

                FontMesh.AddTriangle(new Triangle(new Vertex(4).SetData(0, x, y - r.Height, r.X / 1024f, (r.Y+r.Height) / 1024f),
                   new Vertex(4).SetData(0, x+r.Width, y, (r.X+r.Width) / 1024f, r.Y / 1024f),
                    new Vertex(4).SetData(0, x + r.Width, y - r.Height, (r.X + r.Width) / 1024f, (r.Y + r.Height) / 1024f)));

                x += r.Width;
                maxheight = Math.Min(maxheight, y-r.Height);

            }

            return new GraphicsEngineMath.Vector2(Math.Max(maxwidth, x)/2, -Math.Min(y, maxheight)/2);

        }


        public void Dispose()
        {
            this.FontMesh.Dispose();
            this.FontTexture.Dispose();
        }
    }
}
