﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SlimDX.Direct3D11;
using GraphicsEngineBase;

namespace GraphicsEngineDX11
{
    public class DirectX11Texture : Texture
    {

        internal ShaderResourceView texture { get; private set; }

        internal DirectX11Texture(ShaderResourceView texture)
        {
            this.texture = texture;
        }


        public void Dispose()
        {
            texture.Dispose();
        }

        public void SetItem(object obj)
        {
            texture = ((DirectX11Texture)obj).texture;
        }

        public byte[] Sample()
        {
            
            SlimDX.DXGI.Surface surface = texture.Resource.AsSurface();
            SlimDX.DataRectangle data = surface.Map(SlimDX.DXGI.MapFlags.Read);

            byte[] sample=new byte[data.Data.Length];

            data.Data.Read(sample, 0, (int)data.Data.Length);

            surface.Unmap();

            return sample;

        }
    }
}
