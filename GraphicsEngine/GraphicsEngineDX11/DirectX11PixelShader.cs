﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GraphicsEngineDX11
{
    internal class DirectX11PixelShader : IDisposable
    {

        public SlimDX.Direct3D11.PixelShader Shader { get; private set; }

        public DirectX11PixelShader(SlimDX.Direct3D11.PixelShader shader)
        {
            Shader = shader;
        }


        public void Dispose()
        {
            Shader.Dispose();
        }
    }
}
