﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GraphicsEngineBase;

namespace GraphicsEngineDX11
{
    public class DirectX11Mesh :  Mesh
    {


        private List<SlimDX.Direct3D11.Buffer> vbuffer;
        private List<SlimDX.Direct3D11.Buffer> ibuffer;

        internal DirectX11Mesh(VertexType vtype) : base(vtype)
        {

            ibuffer = new List<SlimDX.Direct3D11.Buffer>();
            vbuffer = new List<SlimDX.Direct3D11.Buffer>();
            VertexBuffer = new List<SlimDX.Direct3D11.VertexBufferBinding>();

        }



        public override void UpdateBuffer(Device device)
        {

            if (!dirty)
                return;

            Dispose();

            foreach (var subset in subsets)
            {

                SlimDX.DataStream vertices = new SlimDX.DataStream(subset.PrimitiveCount * DataCollection.VertexSize * 3, true, true);

                foreach (Triangle triangle in subset.Triangles)
                {

                    vertices.WriteRange<float>(triangle.vertices[0].data);
                    vertices.WriteRange<float>(triangle.vertices[1].data);
                    vertices.WriteRange<float>(triangle.vertices[2].data);

                }

                //slimdx bug
                vertices.Position = 0;



                SlimDX.Direct3D11.Buffer vertbuffer = ((DirectX11Device)device).CreateVertexBuffer(vertices);
                vbuffer.Add(vertbuffer);

                SlimDX.Direct3D11.VertexBufferBinding vbinding = new SlimDX.Direct3D11.VertexBufferBinding(vertbuffer, this.DataCollection.VertexSize, 0);
                VertexBuffer.Add(vbinding);

                SlimDX.DataStream indices = new SlimDX.DataStream(subset.PrimitiveCount * sizeof(short) * 3, true, true);

                short index = 0;

                foreach (Triangle triangle in subset.Triangles)
                {

                    indices.Write<short>(index++);
                    indices.Write<short>(index++);
                    indices.Write<short>(index++);

                }

                indices.Position = 0;

                ibuffer.Add(((DirectX11Device)device).CreateIndexBuffer(indices));

            }

            dirty = false;
        }

        internal List<SlimDX.Direct3D11.Buffer> IndexBuffer 
        {
            get 
            {
                return ibuffer;
            }
        }

        internal List<SlimDX.Direct3D11.VertexBufferBinding> VertexBuffer
        {
            get;
            private set;
        }

        public override void Dispose()
        {
            foreach (var buffer in VertexBuffer)
                if (buffer != null)
                    buffer.Buffer.Dispose();

            VertexBuffer.Clear();

            foreach (var buffer in IndexBuffer)
                buffer.Dispose();

            IndexBuffer.Clear();
        }

        public override void SetItem(object obj)
        {
            DirectX11Mesh m = (DirectX11Mesh)obj;

            this.Triangles = m.Triangles;
            //this.DataCollection = m.DataCollection;
            this.dirty = true;
        }

        public override int MaxPrimitivesPerSubset
        {
            get { return 20000; }
        }
    }
}
