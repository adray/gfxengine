﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GraphicsEngineBase;
using SlimDX.Windows;
using GraphicsEngineMath;
using ConstantBuffer = GraphicsEngineBase.PooledResource<SlimDX.Direct3D11.Buffer>;

namespace GraphicsEngineDX11
{
    [Flags]
    internal enum LightingFlags
    {
        None=0,
        Shadow=1
    }

    [Flags]
    internal enum CompositionFlags
    {
        None=0,
        HDR=1
    }

    internal partial class DirectX11Renderer : Renderer, IDisposable
    {

#if DEBUG

        private System.Diagnostics.Stopwatch watch
            = new System.Diagnostics.Stopwatch();
        private int frames = 0;
        private int triangles = 0;
        private int drawcalls = 0;
        private int texture_drawcalls = 0;
        private TextObject infotext=new TextObject();
#endif

        private Chain chain;

        private readonly DirectX11Device device;

        private DirectX11PixelShader clearpixel;
        private DirectX11VertexShader clearvertex;

        private Shader finalpixel;
        //private DirectX11PixelShader finalpixel;
        private DirectX11VertexShader finalvertex;

        private DirectX11PixelShader quadpixel;
        private DirectX11VertexShader quadvertex;

        private Shader geometrypixel;
        private Shader geometryvertex;

        private DirectX11VertexShader spotLightvertex;
        private DirectX11PixelShader spotLightpixel;

        private DirectX11VertexShader directionalLightvertex;
        private Shader directionalLightpixel;

        private DirectX11VertexShader pointLightvertex;
        private DirectX11PixelShader pointLightpixel;

        private DirectX11VertexShader fontvertex;
        private DirectX11PixelShader fontpixel;

        private DirectX11VertexShader billboardvertex;
        private DirectX11PixelShader billboardpixel;

        private Shader forwardpixelshader;
        private Shader forwardvertexshader;

        private Shader postprocesspixel;
        private Shader postprocessvertex;

        private Shader shadowvertex;
        private DirectX11PixelShader shadowpixel;

        private Shader terrainpixel;
        private DirectX11VertexShader terrainvertex;

        //render targets
        private SlimDX.Direct3D11.RenderTargetView normalmap;
        private SlimDX.Direct3D11.RenderTargetView diffusemap;
        private PooledResource<SlimDX.Direct3D11.RenderTargetView> lightmap;
        private SlimDX.Direct3D11.RenderTargetView particlemap;
        private SlimDX.Direct3D11.RenderTargetView depthmap;

        private DirectX11RenderTargetPool renderTargetPool;

        private Mesh fullscreenquad;
        private Mesh particlemesh;
        private Mesh unitsphere;
        private Mesh frustumquad;
        private Mesh unitcone;

        private SlimDX.Direct3D11.SamplerState sampler;
        private SlimDX.Direct3D11.SamplerState normalsampler;
        private SlimDX.Direct3D11.SamplerState shadowsampler;
        private SlimDX.Direct3D11.SamplerState reflectionsampler;

        private DirectX11FontMesh fontengine;

        public DirectX11Renderer(DirectX11Device device)
        {

            this.device = device;

            normalmap = device.CreateRenderTarget();
            diffusemap = device.CreateRenderTarget();
            //lightmap = device.CreateRenderTarget();
            particlemap = device.CreateRenderTarget();
            depthmap = device.CreateDepthRenderTarget();
            renderTargetPool = new DirectX11RenderTargetPool(device);

            clearpixel = device.LoadPixelShader("Deferred.fx", "ClearPixel");
            clearvertex = device.LoadVertexShader("Deferred.fx", "ClearVertex");

            CreateShaderPermutations();

            finalpixel = new Shader(device.LoadPixelShader, "Final.fx", "FinalPixel");
            finalpixel.Add(new List<RenderPermutation>(new RenderPermutation[]
            {
               // new RenderPermutation((int)CompositionFlags.HDR, "V_HDR")
            }));

            finalvertex = device.LoadVertexShader("Final.fx", "FinalVertex");

            quadpixel = device.LoadPixelShader("Quad.fx", "QuadPixel");
            quadvertex = device.LoadVertexShader("Quad.fx", "QuadVertex");

            directionalLightvertex = device.LoadVertexShader("Light.fx", "DirectionalLightVertex");
            directionalLightpixel = new Shader(new Compile(
                device.LoadPixelShader), "Light.fx", "DirectionalLightPixel");
            directionalLightpixel.Add(new List<RenderPermutation>(new RenderPermutation[] {
                new RenderPermutation((int)LightingFlags.Shadow, "V_SHADOWMAP")/*,
                new RenderPermutation((int)LightingFlags.HDR, "V_HDR")*/
            }));

            pointLightvertex = device.LoadVertexShader("Light.fx", "PointLightVertex");
            pointLightpixel = device.LoadPixelShader("Light.fx", "PointLightPixel");

            spotLightvertex = device.LoadVertexShader("Light.fx", "SpotLightVertex");
            spotLightpixel = device.LoadPixelShader("Light.fx", "SpotLightPixel");

            billboardpixel = device.LoadPixelShader("Billboard.fx", "BillboardPixel");
            billboardvertex = device.LoadVertexShader("Billboard.fx", "BillboardVertex");

            forwardpixelshader = new Shader(new Compile(device.LoadPixelShader), "Forward.fx", "RenderPixel");
            forwardpixelshader.Add(new List<RenderPermutation>(new RenderPermutation[]
            {
                new RenderPermutation((int)RenderFlags.DiffuseMap, "V_DIFFUSEMAP")
            }));
            forwardvertexshader = new Shader(new Compile(device.LoadVertexShader), "Forward.fx", "RenderVertex");
            forwardvertexshader.Add(new List<RenderPermutation>(new RenderPermutation[]
            {
                new RenderPermutation((int)RenderVertexFlags.Instance, "V_INSTANCE")
            }));

            terrainvertex = device.LoadVertexShader("Terrain.fx", "RenderVertex");
            terrainpixel = new Shader(new Compile(device.LoadPixelShader), "Terrain.fx", "RenderPixel");

            terrainpixel.Add(new List<RenderPermutation>(new RenderPermutation[]
            {
                new RenderPermutation((int)RenderFlags.DiffuseMap, "V_DIFFUSEMAP"),
                new RenderPermutation((int)RenderFlags.BumpMap, "V_BUMPMAP"),
            }));


            fullscreenquad = new DirectX11Mesh(VertexType.QuadPosition);
            fullscreenquad.AddTriangle(new Triangle(new Vertex(2).SetData(0, 1, -1),
                new Vertex(2).SetData(0, -1, -1),
                new Vertex(2).SetData(0, -1, 1)));
            fullscreenquad.AddTriangle(new Triangle(new Vertex(2).SetData(0, 1, 1),
                new Vertex(2).SetData(0, 1, -1),
                new Vertex(2).SetData(0, -1, 1)));

            frustumquad = new DirectX11Mesh(VertexType.QuadFrustum);
            frustumquad.AddTriangle(new Triangle(new Vertex(3).SetData(0, 1, -1, 2),
                new Vertex(3).SetData(0, -1, -1, 3),
                new Vertex(3).SetData(0, -1, 1, 1)));
            frustumquad.AddTriangle(new Triangle(new Vertex(3).SetData(0, 1, 1, 0),
                new Vertex(3).SetData(0, 1, -1, 2),
                new Vertex(3).SetData(0, -1, 1, 1)));

            particlemesh = new DirectX11Mesh(VertexType.SimpleParticle);
            particlemesh.AddTriangle(new Triangle(new Vertex(1).SetData(0, 0),
                new Vertex(1).SetData(0, 1),
                new Vertex(1).SetData(0, 2)));
            particlemesh.AddTriangle(new Triangle(new Vertex(1).SetData(0, 3),
                new Vertex(1).SetData(0, 2),
                new Vertex(1).SetData(0, 1)));

            System.IO.StreamReader unitstream = new System.IO.StreamReader(
                    System.Reflection.Assembly.GetCallingAssembly().GetManifestResourceStream("unitsphere.obj"));

            var factory = GraphicsEngineBase.Loader.ObjLoader.Load(device, VertexType.Position, unitstream);
            unitsphere = factory.Subsets[0].Mesh;

            unitstream.Close();

            System.IO.StreamReader conestream = new System.IO.StreamReader(
                    System.Reflection.Assembly.GetCallingAssembly().GetManifestResourceStream("unitcone.obj"));

            unitcone = GraphicsEngineBase.Loader.ObjLoader.Load(device, VertexType.Position, conestream).Subsets[0].Mesh;

            conestream.Close();

            sampler = device.CreateSampler();
            normalsampler = device.CreateNormalSampler();
            shadowsampler = device.CreateShadowSampler();
            reflectionsampler = device.CreateReflectionSampler();

            fontpixel = device.LoadPixelShader("Font.fx", "FontPixel");
            fontvertex = device.LoadVertexShader("Font.fx", "FontVertex");

            fontengine = new DirectX11FontMesh(device, 
                System.Reflection.Assembly.GetCallingAssembly().GetManifestResourceStream("s.bin"),
                System.Reflection.Assembly.GetCallingAssembly().GetManifestResourceStream("font.png")
                );
            
#if DEBUG
            infotext = new TextObject()
            {
                Colour = new Vector4(1f, 1f, 0.3f, 1)
            };
#endif

        }

        /// <summary>
        /// Compiles all the permutations of the geometry shaders.
        /// </summary>
        private void CreateShaderPermutations()
        {


            List<RenderPermutation> p = new List<RenderPermutation>();

            //pixel shaders

            p.Add(new RenderPermutation((int)RenderFlags.BumpMap, "V_BUMPMAP"));
            p.Add(new RenderPermutation((int)RenderFlags.DiffuseMap, "V_DIFFUSEMAP"));
            p.Add(new RenderPermutation((int)RenderFlags.Reflection, "V_REFLECTION"));

            geometrypixel = new Shader(new Compile(device.LoadPixelShader), "Geometry.fx", "RenderPixel");

            geometrypixel.Add(p);

            p.Clear();

            //vertex shaders

            p.Add(new RenderPermutation((int)RenderVertexFlags.None, ""));
            p.Add(new RenderPermutation((int)RenderVertexFlags.Instance, "V_INSTANCE"));
            p.Add(new RenderPermutation((int)RenderVertexFlags.Terrain, "V_TERRAIN"));
            p.Add(new RenderPermutation((int)RenderVertexFlags.Reflection, "V_REFLECTION"));

            geometryvertex = new Shader(new Compile(device.LoadVertexShader), "Geometry.fx", "RenderVertex");

            geometryvertex.AddSingle(p);

            p.Clear();

            //same for post processing

            //pixel shaders + vertex shaders

            p.Add(new RenderPermutation((int)PassFunction.VerticalBlur, "V_VERTICAL_BLUR"));
            p.Add(new RenderPermutation((int)PassFunction.Downsize, "V_DOWNSIZE"));
            p.Add(new RenderPermutation((int)PassFunction.HorizontalBlur, "V_HORIZONTAL_BLUR"));
            p.Add(new RenderPermutation((int)PassFunction.Upsize, "V_UPSIZE"));
            p.Add(new RenderPermutation((int)PassFunction.Inversion, "V_INVERSION"));
            p.Add(new RenderPermutation((int)PassFunction.Combine, "V_COMBINE"));
            p.Add(new RenderPermutation((int)PassFunction.Bright, "V_BRIGHT"));
            p.Add(new RenderPermutation((int)PassFunction.Tonemap, "V_TONEMAP"));
            p.Add(new RenderPermutation((int)PassFunction.Luminance, "V_LUM"));
            p.Add(new RenderPermutation((int)PassFunction.Luminance_Downsize, "V_LUMDOWNSIZE"));

            postprocesspixel = new Shader(new Compile(device.LoadPixelShader), "PostProcess.fx", "PassPixel");

            postprocesspixel.AddSingle(p);

            postprocessvertex = new Shader(new Compile(device.LoadVertexShader), "PostProcess.fx", "PassVertex");

            postprocessvertex.AddSingle(p);

            p.Clear();

            p.Add(new RenderPermutation((int)RenderVertexFlags.None, ""));
            p.Add(new RenderPermutation((int)RenderVertexFlags.Instance, "V_INSTANCE"));

            shadowvertex = new Shader(new Compile(device.LoadVertexShader), "Shadow.fx", "RenderVertex");
            shadowvertex.AddSingle(p);

            shadowpixel = device.LoadPixelShader("Shadow.fx", "RenderPixel");

        }



        public override void RenderScene(Scene scene)
        {

#if DEBUG

            if (!watch.IsRunning || watch.ElapsedMilliseconds > 1000)
            {
                watch.Stop();
                watch.Reset();
                watch.Start();
                infotext.Text = String.Format("FPS {0}\n{1}\n{2} Triangles\n{3} draw calls\n{4} diffuse", frames, device.HardwareDescription, triangles, drawcalls, texture_drawcalls);
                frames = 0;
            }

            frames++;
            triangles = 0;
            drawcalls = 0;
            texture_drawcalls = 0;
#endif

            device.SetRasterState(scene.BackFaceCulling, true);

            //draw geometry to render targets

            Frustum frustum = scene.Camera.CalculateFrustumVS();

            device.SetRasterState(scene.BackFaceCulling, !scene.WireFrameRendering);

            device.EnableZBuffer(true);

            SlimDX.Matrix proj = SlimDX.Matrix.PerspectiveFovLH((float)Math.PI / 4, 4.0f / 3.0f, scene.Camera.NearPlane, scene.Camera.FarPlane);
            SlimDX.Matrix view = SlimDX.Matrix.LookAtLH(
                new SlimDX.Vector3(scene.Camera.Position.X, scene.Camera.Position.Y, scene.Camera.Position.Z),
                new SlimDX.Vector3(scene.Camera.LookAt.X, scene.Camera.LookAt.Y, scene.Camera.LookAt.Z),
                new SlimDX.Vector3(0, 1, 0));

            SlimDX.Matrix projTranspose = SlimDX.Matrix.Transpose(proj);
            SlimDX.Matrix viewTranspose = SlimDX.Matrix.Transpose(view);

            SlimDX.Matrix inverseViewProj = projTranspose * viewTranspose;
            inverseViewProj.Invert();

            //render reflections
            var reflection = new Render.ReflectionRender();
            var reflections = reflection.Render(new Render.ReflectionRenderParams()
            {
                 device = device,
                 pixel = forwardpixelshader,
                 vertex = forwardvertexshader,
                 TextureSampler = sampler,
                 pool = renderTargetPool,
                 scene = scene,
                 Frustum = frustum,
                 view = view
            });

            //clear g-buffer

            device.SetRenderTargets(normalmap);

            device.Clear(new SlimDX.Color4(1, 0.5f, 0.5f, 0.5f));

            device.SetRenderTargets(diffusemap);

            device.Clear(new SlimDX.Color4(1, 0, 0, 0));

            device.SetRenderTargets(depthmap);

            device.Clear(new SlimDX.Color4(1, 1, 1, 1));

            device.SetRenderTargets(normalmap, diffusemap, depthmap);

            //render geometry to gbuffer

            GBufferRender(scene, frustum, view, projTranspose, viewTranspose, reflections);

            //render particles

            ParticlesRender(scene, projTranspose, viewTranspose);
            
            //shadow map
            System.Collections.Generic.Dictionary<Light, ShadowMap>
                shadowcollection = new Dictionary<Light, ShadowMap>();
            ShadowMapsRender(scene, frustum, SlimDX.Matrix.Invert(view), shadowcollection);

            //render lights
            LightsRender(scene, frustum, view, projTranspose, viewTranspose, shadowcollection);
            
            //compose lights+shadows+gbuffer
            PooledResource<SlimDX.Direct3D11.RenderTargetView> target = CompositionRender(scene);


            //post process
            PostProcess(target, chain, true);

            //render text/overlay

            RenderText(scene);
            RenderQuads(scene);

            RenderDebugOverlay(shadowcollection, reflections);

            foreach (var reflect in reflections)
                reflect.Value.Target.Unlock();


            lightmap.Unlock();

            //present

            device.Present(scene.VerticalSync ? 1:0);

        }

        private void RenderQuad(float x, float y, SlimDX.Direct3D11.RenderTargetView texture)
        {
            RenderQuad(x, y, 0.2f, 0.2f, texture);
        }

        private void RenderQuad(float x, float y, float sx, float sy, SlimDX.Direct3D11.RenderTargetView texture)
        {
            ConstantBuffer buffer;
            SetupQuad(x, y, sx, sy, out buffer);

            device.SetPixelTexture(texture, 0);

            device.Render((DirectX11Mesh)fullscreenquad);

            buffer.Unlock();
        }

        private void SetupQuad(float x, float y, float sx, float sy, out ConstantBuffer buffer)
        {
            SlimDX.DataStream data = new SlimDX.DataStream(16, true, true);

            //postion
            data.WriteRange<float>(new float[] { x, y });
            //scale
            data.WriteRange<float>(new float[] { sx, sy });

            data.Position = 0;

            buffer = device.CreateConstantBuffer(data);
            buffer.Lock();

            device.SetVertexConstantBuffer(buffer.Value, 0);

            //bind textures

            device.SetSampler(sampler, 0);
        }

        private void RenderQuad(float x, float y, float sx, float sy, SlimDX.Direct3D11.ShaderResourceView texture)
        {
            ConstantBuffer buffer;
            SetupQuad(x, y, sx, sy, out buffer);

            device.SetPixelTexture(texture, 0);

            device.Render((DirectX11Mesh)fullscreenquad);

            buffer.Unlock();
        }


        public override void Dispose()
        {
            this.forwardpixelshader.Dispose();
            this.forwardvertexshader.Dispose();

            this.shadowpixel.Dispose();
            this.shadowvertex.Dispose();

            this.clearpixel.Shader.Dispose();
            this.clearvertex.Shader.Dispose();
            this.finalpixel.Dispose();
            this.finalvertex.Shader.Dispose();
            this.geometryvertex.Dispose();
            this.geometrypixel.Dispose();
            this.quadpixel.Shader.Dispose();
            this.quadvertex.Shader.Dispose();
            this.fontpixel.Dispose();
            this.fontvertex.Dispose();
            this.billboardpixel.Dispose();
            this.billboardvertex.Dispose();

            this.directionalLightpixel.Dispose();
            this.directionalLightvertex.Dispose();
            this.postprocesspixel.Dispose();
            this.postprocessvertex.Dispose();
            this.spotLightpixel.Dispose();
            this.spotLightvertex.Dispose();
            this.pointLightpixel.Dispose();
            this.pointLightvertex.Dispose();

            this.depthmap.Dispose();
            this.diffusemap.Dispose();
            //this.lightmap.Dispose();
            this.normalmap.Dispose();
            this.sampler.Dispose();
            this.normalsampler.Dispose();
            this.particlemap.Dispose();
            this.shadowsampler.Dispose();
            this.reflectionsampler.Dispose();

            this.renderTargetPool.Dispose();

            this.fullscreenquad.Dispose();
            this.unitsphere.Dispose();
            this.unitcone.Dispose();

            this.fontengine.Dispose();
        }

        public override void SetPostProcessChain(Chain chain)
        {
            this.chain = chain;
        }
    }
}
