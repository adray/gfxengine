﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GraphicsEngineMath;
using GraphicsEngineBase;

namespace GraphicsEngineDX11
{
    public class DirectX11Instance : MeshInstance, IDisposable 
    {

        public DataCollection DataCollection { get; private set; }
        private List<Vertex> InstanceData { get; set; }
        internal SlimDX.Direct3D11.VertexBufferBinding InstanceBuffer { get; private set; }
        private bool dirty;

        internal DirectX11Instance(VertexType type)
        {
            dirty = true;
            DataCollection = new DataCollection(type);
            InstanceData = new List<Vertex>();
        }


        public void UpdateBuffer(DirectX11Device device)
        {

            if (!dirty)
                return;

            if (InstanceBuffer.Buffer != null)
                InstanceBuffer.Buffer.Dispose();

            SlimDX.DataStream stream = new SlimDX.DataStream(InstanceCount*DataCollection.VertexSize, true, true);

            foreach (Vertex v in InstanceData)
            {
                stream.WriteRange<float>(v.data);
            }

            stream.Position = 0;

            SlimDX.Direct3D11.Buffer buffer = device.CreateVertexBuffer(stream);

            InstanceBuffer = new SlimDX.Direct3D11.VertexBufferBinding(buffer, DataCollection.VertexSize, 0);


            dirty = false;
        }

        internal void AddInstance(Vertex vertex)
        {
            InstanceData.Add(vertex);
            dirty = true;
        }

        internal void RemoveInstance(Vertex vertex)
        {
            InstanceData.Remove(vertex);
            dirty = true;
        }

        public override Matrix4x4 At(int index)
        {
            return new Matrix4x4() { data = InstanceData[index].data };
        }

        public override int InstanceCount
        {
            get
            {
                return InstanceData.Count;
            }
       }

        internal void MakeDirty()
        {
            dirty = true;
        }

        public override void Dispose()
        {
            InstanceBuffer.Buffer.Dispose();
        }

        public override void Add(Matrix4x4 instance)
        {
            AddInstance(new Vertex(16).SetData(0, instance.data));
        }

        public override void Remove(int index)
        {
            this.InstanceData.RemoveAt(index);
            dirty = true;
        }

        public override void Set(int index, Matrix4x4 instance)
        {
            this.InstanceData[index] = new Vertex(16).SetData(0, instance.data);
            dirty = true;
        }
    }
}
