﻿


float4 ClearVertex(float2 position : POSITION) : SV_POSITION
{
    return float4(position, 0, 1);
}



void ClearPixel(in float4 position : SV_POSITION, out float4 normals : SV_TARGET, out float4 diffuse : SV_TARGET1)
{
    normals = float4(0.5f, 0.5f, 0.5f, 1.0f);
	diffuse = float4(0.0f, 0.0f, 0.0f, 0.0f);
}



