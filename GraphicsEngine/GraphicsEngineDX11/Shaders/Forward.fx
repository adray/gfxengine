﻿
#ifdef V_DIFFUSEMAP
Texture2D diffusemap : register (t0);
SamplerState diffusesampler : register (s0);
#endif

#define V_LIGHTCOUNT 5

struct Light
{
	float3 dir;
	int type;
};

cbuffer Constants : register (c0)
{

	matrix world;
	matrix view;
	matrix proj;

	float4 campos;
	Light lights[V_LIGHTCOUNT];

};

#ifdef V_INSTANCE
#define VERTEXSHADER

void RenderVertex(in float4 position : POSITION, in float4 normal : NORMAL, in float2 incoord : TEXCOORD,
	in float4 row1 : TEXCOORD1, in float4 row2 : TEXCOORD2, in float4 row3 : TEXCOORD3, in float4 row4 : TEXCOORD4,
	out float4 outpos : SV_POSITION, out float3 outnormal : NORMAL, out float2 outcoord : TEXCOORD )
{

	
	matrix world={row1,row2,row3,row4};

	float4 viewpos = mul(mul(position, world), view);
    outpos = mul(viewpos, proj);
	outnormal = mul(float4(normal.xyz,0), world);
	outcoord = incoord;

}

#endif

#ifndef VERTEXSHADER

void RenderVertex(in float4 pos : POSITION, in float4 normal : NORMAL, in float2 incoord : TEXCOORD0,
	out float4 outpos : SV_POSITION, out float3 outnormal : NORMAL, out float2 outcoord : TEXCOORD0)
{

	
	
	outpos = mul(mul(mul(pos, world), view), proj);
	outnormal = mul(float4(normal.xyz,0), world).xyz;
	outcoord = incoord;
	

} 

#endif

float4 RenderPixel(in float4 pos : SV_POSITION, in float3 normal : NORMAL, in float2 incoord : TEXCOORD0) : SV_TARGET0
{


	//loop through lights
	float lit = 0;

	[unroll]
	for (int i = 0; i < V_LIGHTCOUNT; i++)
	{
		if (lights[i].type == 0)
		{
			
			lit += saturate(dot(normalize(normal), -normalize(lights[i].dir)));

		}
		
		
	}

#ifdef V_DIFFUSEMAP
	//sample diffuse texture
	float4 diffuse = diffusemap.Sample(diffusesampler, incoord);
	float4 colour;
	
	colour.rgb = mul(diffuse.rgb, lit);
	colour.a = 1;

	return colour;
#else

	return lit;
#endif

}

