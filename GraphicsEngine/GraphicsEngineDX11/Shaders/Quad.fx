﻿

Texture2D display : register (t0);

SamplerState textureSampler : register (s0);

cbuffer Constants : register (c0)
{

	float2 pos;
	float2 scale;
	float4 pad1;
	float4 pad2;
	float4 pad3;

};

float4 QuadVertex(float2 position : POSITION, out float2 tex : TEXCOORD0) : SV_POSITION
{

	tex = (position.xy+1)/2;

    return float4(position*scale+pos, 0, 1);
}

void QuadPixel(in float4 pos : SV_POSITION, in float2 tex : TEXCOORD0, out float4 outcolor : SV_Target)
{

	tex.y=1-tex.y;
	outcolor = display.Sample(textureSampler, tex);
	
}



