﻿/*

The luminance constants are taken from : http://en.wikipedia.org/wiki/Luma_%28video%29
The Gaussian kernels values are based on the tables derived in the DirectX11 SDK.

*/

#define WIDTH 800
#define HEIGHT 600
#ifdef V_VERTICAL_BLUR
#define BLUR
#endif
#ifdef V_HORIZONTAL_BLUR
#define BLUR
#endif

Texture2D sample : register (t0);

#ifdef V_COMBINE
Texture2D sample2 : register (t1);
#endif
#ifdef V_TONEMAP
Texture2D sample2 : register (t1);
#endif

SamplerState sampleSampler : register (s0);

cbuffer Constants : register (c0)
{

	float data[16];

};

float4 PassVertex(float4 position : POSITION, out float2 tex : TEXCOORD0) : SV_POSITION
{
	tex = (position.xy*float2(-1,1)+1)/2;
	
    return float4(position.xy, 0, 1);
}

#ifdef BLUR

#define KERNEL_SIZE 13

static const float2 kernel[KERNEL_SIZE] = 
{

	{ 6, 0 },
	{ 5, 0 },
	{ 4, 0 },
	{ 3, 0 },
	{ 2, 0 },
	{ 1, 0 },
	{ 0, 0 },
	{ -1, 0},
	{ -2, 0},
	{ -3, 0},
	{ -4, 0},
	{ -5, 0},
	{ -6, 0}

};

//calculated from guassian function
static const float kernel_weights[KERNEL_SIZE] =
{

	0.002216,
    0.008764,
    0.026995,
    0.064759,
    0.120985,
    0.176033,
    0.199471,
    0.176033,
    0.120985,
    0.064759,
    0.026995,
    0.008764,
    0.002216,

};

void PassPixel(in float4 pos : SV_POSITION, in float2 tex : TEXCOORD0, out float4 outcolor : SV_Target)
{

	float sum=0;
	outcolor=float4(0,0,0,0);

	for (int i = 0; i < KERNEL_SIZE; i++)
	{

#ifdef V_VERTICAL_BLUR
		outcolor += sample.Sample(sampleSampler, -tex + kernel[i].yx / HEIGHT) * kernel_weights[i];
#else
		outcolor += sample.Sample(sampleSampler, -tex + kernel[i].xy / WIDTH) * kernel_weights[i];
#endif
		sum += kernel_weights[i];

	}

	outcolor /= sum;

	//outcolor.a=1;

}


#endif
#ifdef V_DOWNSIZE

void PassPixel(in float4 pos : SV_POSITION, in float2 tex : TEXCOORD0, out float4 outcolor : SV_Target)
{

	float4 color = sample.Sample(sampleSampler, -tex);

	outcolor.rgb = color.rgb;
	outcolor.a=color.a;

}

#endif
#ifdef V_UPSIZE

void PassPixel(in float4 pos : SV_POSITION, in float2 tex : TEXCOORD0, out float4 outcolor : SV_Target)
{

	outcolor = sample.Sample(sampleSampler, -tex);

}


#endif
#ifdef V_INVERSION

void PassPixel(in float4 pos : SV_POSITION, in float2 tex : TEXCOORD0, out float4 outcolor : SV_Target)
{

	outcolor = 1-sample.Sample(sampleSampler, -tex);

}

#endif
#ifdef V_COMBINE

void PassPixel(in float4 pos : SV_POSITION, in float2 tex : TEXCOORD0, out float4 outcolor : SV_Target)
{

	const float weighting = 0.5f;

	outcolor = sample.Sample(sampleSampler, -tex)*weighting + sample2.Sample(sampleSampler, -tex)*(1-weighting);

}

#endif
#ifdef V_BRIGHT

void PassPixel(in float4 pos : SV_POSITION, in float2 tex : TEXCOORD0, out float4 outcolor : SV_Target)
{

	outcolor = sample.Sample(sampleSampler, -tex);
	outcolor = outcolor * outcolor + outcolor*0.3f + 0.4f;
	outcolor /= 10.0f;
	outcolor = saturate(outcolor);

}

#endif
#ifdef V_LUM

#define LUMINANCE_VECTOR float3(0.2126, 0.7152, 0.0722)

void PassPixel(in float4 pos : SV_POSITION, in float2 tex : TEXCOORD0, out float4 outcolor : SV_Target)
{
	float avg=0;

	for (int i = -1; i <= 1; i++)
	{
		for (int j = -1; j <= 1; j++)
		{
			
			float lum = dot(sample.Sample(sampleSampler, -tex+float2(i/WIDTH,j/HEIGHT)).rgb, LUMINANCE_VECTOR);

			avg+= log(lum+0.0001);
		}

	}

	avg/=9.0;
	outcolor.rgb = float3(avg,avg,avg);
	outcolor.a = 1;

}


#endif
#ifdef V_LUMDOWNSIZE

void PassPixel(in float4 pos : SV_POSITION, in float2 tex : TEXCOORD0, out float4 outcolor : SV_Target)
{
	float avg=0;

	for (int i = -1; i < 1; i++)
	{
		for (int j = -1; j < 1; j++)
		{
			
			float lum = sample.Sample(sampleSampler, -tex+float2(i/WIDTH,j/HEIGHT)).r;

			avg+= lum;
		}

	}

	avg/=4.0;
	outcolor.rgb = float3(avg,avg,avg);
	outcolor.a = 1;

}

#endif
#ifdef V_TONEMAP

#define LUMINANCE_VECTOR float3(0.2126, 0.7152, 0.0722)

#define middle_grey 6.0f
#define max_lum 16.0f

void PassPixel(in float4 pos : SV_POSITION, in float2 tex : TEXCOORD0, out float4 outcolor : SV_Target)
{

	float avg_lum = exp(sample2.Sample(sampleSampler, float2(0,0)).r);
	float4 color = sample.Sample(sampleSampler, -tex);

	float lum = dot(color.rgb, LUMINANCE_VECTOR);
	float lum_scaled = avg_lum * lum / middle_grey;
	float lum_norm = (lum_scaled * ( 1.0 + (lum_scaled / (max_lum * max_lum)))) / (1.0 + lum_scaled);
	 
	outcolor = color * lum_norm;
}


#endif