﻿#ifdef V_FALLBACK

float4 FontVertex(float4 position : POSITION, out float2 tex : TEXCOORD0) : POSITION
{
	tex = position.zw;
    return float4(((position.xy+pos.xy*2)/pos.zw)+float2(-1,1), 0, 1);
}

void FontPixel(in float2 tex : TEXCOORD0, out float4 outcolor : COLOR)
{

	float4 color = fontmap.Sample(fontSampler, tex);

	outcolor.rgb = fontcolor.rgb;
	//outcolor.rgb=fontcolor.rgb;
	outcolor.a=color.a;

}

#else

Texture2D fontmap : register (t0);

SamplerState fontSampler : register (s0);

cbuffer Constants : register (c0)
{

	float4 pos;
	float4 fontcolor;
	float4 pad2;
	float4 pad3;

};

float4 FontVertex(float4 position : POSITION, out float2 tex : TEXCOORD0) : SV_POSITION
{
	tex = position.zw;
    return float4(((position.xy+pos.xy*2)/pos.zw)+float2(-1,1), 0, 1);
}

void FontPixel(in float4 pos : SV_POSITION, in float2 tex : TEXCOORD0, out float4 outcolor : SV_Target)
{

	float4 color = fontmap.Sample(fontSampler, tex);

	outcolor.rgb = fontcolor.rgb;
	//outcolor.rgb=fontcolor.rgb;
	outcolor.a=color.a;

}

#endif





