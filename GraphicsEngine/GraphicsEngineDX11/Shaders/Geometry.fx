﻿/*

The linear depth is based on the article:
http://mynameismjp.wordpress.com/2009/03/10/reconstructing-position-from-depth/

*/

#include "gfx.fxh"

#ifdef V_BUMPMAP 

Texture2D normalmap : register (t0);
SamplerState normalSampler : register (s0);

#endif

#ifdef V_DIFFUSEMAP

Texture2D diffusemap : register (t1);
SamplerState diffuseSampler : register (s1);

#endif

#ifdef V_TERRAIN

#define WIDTH 256
#define HEIGHT 256

Texture2D posmap : register (t2);
SamplerState posSampler : register (s2);

Texture2D blendtexture : register (t3);

#endif

#ifdef V_REFLECTION

Texture2D reflectionmap : register (t2);
SamplerState reflectionSampler : register (s2);

#endif

#ifdef V_DIFFUSEARRAY

Texture2DArray diffusemap : register (t0);
SamplerState diffuseSampler : register (s0);

#endif 

cbuffer Constants : register (c0)
{

	matrix world;
	matrix proj;
	matrix view;

#ifdef V_REFLECTION

	matrix reflection_viewproj;
	float3 position;
	float scalex;
	float3 normal;
	float scaley;

#endif


    float3 diffusecolour;
	float farclip;
	float specular;
	float shininess;

};

#ifdef V_INSTANCE
#define VERTEXSHADER

void RenderVertex(in float4 position : POSITION, in float4 normal : NORMAL, in float2 incoord : TEXCOORD,
	in float4 row1 : TEXCOORD1, in float4 row2 : TEXCOORD2, in float4 row3 : TEXCOORD3, in float4 row4 : TEXCOORD4,
	out float4 outpos : SV_POSITION, out float4 outnormal : NORMAL, out float2 outcoord : TEXCOORD )
{

	
	matrix world={row1,row2,row3,row4};

	float4 viewpos = mul(mul(position, world), view);
    outpos = mul(viewpos, proj);
	outnormal = float4(mul(mul(float4(normal.xyz,0), world), view).xyz, viewpos.z);
	outcoord = incoord;

}

#endif

#ifdef V_TERRAIN
#define VERTEXSHADER

void RenderVertex(in float2 incoord : POSITION, in float3 normal : NORMAL, out float4 outpos : SV_POSITION, out float4 outnormal : NORMAL,
	out float2 outcoord : TEXCOORD )
{
	
	float y = posmap.SampleLevel(posSampler, incoord, 0).r;
	float4 pos = float4(incoord.x, y, incoord.y, 1);
	//sample some other nearby pixels to guess normal

	//float2 var = incoord+float2(1.0f/WIDTH,0/HEIGHT);
	//float3 dist1 = normalize(float3(var.x, posmap.SampleLevel(posSampler, var, 0).r, var.y) - pos.xyz);

	//var = incoord+float2(0/WIDTH,1.0f/HEIGHT);
	//float3 dist2 = normalize(float3(var.x, posmap.SampleLevel(posSampler, var, 0).r, var.y) - pos.xyz);

	float4 viewpos = mul(mul(pos, world), view);
    outpos = mul(viewpos, proj);
	//outnormal = float4(mul(mul(float4(normal.xyz, 0), world), view).xyz, viewpos.z);
	outnormal = float4(mul(float4(normal.xyz, 0), view).xyz, viewpos.z);
	outcoord = incoord*float2(world._11/world._22, world._33/world._22);

}


#endif

#ifdef V_REFLECTION

#define VERTEXSHADER

void RenderVertex(in float index : POSITION,
	out float4 outpos : SV_POSITION, out float4 outnormal : NORMAL, out float2 outcoord : TEXCOORD,
	out float2 reflection : TEXCOORD1 )
{

	float4 worldpos;
	float3 vec1 = cross(normal, float3(1,0,0));
	float3 vec2 = cross(normal, vec1);
	vec1 = normalize(vec1);
	vec2 = normalize(vec2);

	if (index == 0)
	{
		worldpos = float4(position.xyz - scalex * vec1 - scaley * vec2, 1);
		outcoord = float2(0,0);
	}
	else if (index == 1)
	{
		worldpos = float4(position.xyz + scalex * vec1 - scaley * vec2, 1);
		outcoord = float2(1,0);
	}
	else if (index == 2)
	{
		worldpos = float4(position.xyz - scalex * vec1 + scaley * vec2, 1);
		outcoord = float2(0,1);
	}
	else
	{
		worldpos = float4(position.xyz + scalex * vec1 + scaley * vec2, 1);
		outcoord = float2(1,1);
	}


	float4 viewpos = mul(worldpos, view);
    outpos = mul(viewpos, proj);
	outnormal = float4(mul(float4(normal.xyz,0), view).xyz, viewpos.z);

	//float3 dir = reflect(normalize(viewpos.xyz), normalize(outnormal.xyz));

	float4 ref = mul(worldpos, reflection_viewproj);
	reflection = (ref.xy/ref.w + 1) /2;
	reflection.y = 1-reflection.y;

}

#endif

#ifndef VERTEXSHADER

void RenderVertex(in float4 position : POSITION, in float4 normal : NORMAL, in float2 incoord : TEXCOORD,
	out float4 outpos : SV_POSITION, out float4 outnormal : NORMAL, out float2 outcoord : TEXCOORD )
{

	

	float4 viewpos = mul(mul(position, world), view);
    outpos = mul(viewpos, proj);
	outnormal = float4(mul(mul(float4(normal.xyz,0), world), view).xyz, viewpos.z);
	outcoord = incoord;

}

#endif


void RenderPixel(in float4 position : SV_POSITION, in float4 innormal : NORMAL, in float2 incoord : TEXCOORD,

#ifdef V_REFLECTION
	in float2 reflectionpos : TEXCOORD1,
#endif

	out float4 normals : SV_TARGET, out float4 diffuse : SV_TARGET1, out float4 depth : SV_TARGET2)
{
#ifdef V_BUMPMAP
	float3 n = decodeNormal(normalmap.Sample(normalSampler, incoord)).xyz;

	//float3 helper = normalize(float3(innormal.x+0.001f, innormal.y+0.001f, -innormal.z+0.0001f));
	float3 helper = normalize(float3(0.5f, 0.25f, -0.7f));
	float3 binormal = cross(innormal.xyz, helper);
	float3 tangent = cross(innormal.xyz, binormal);

	innormal.xyz = innormal.xyz + n.y * binormal + n.x * tangent;

	normals.argb = encodeNormal(innormal).xyzw;
#else
    normals.argb = encodeNormal(innormal).xyzw;
#endif
normals.a = specular;

#ifdef V_DIFFUSEMAP
	diffuse.rgb = diffusemap.Sample(diffuseSampler, incoord).rgb;
#else
	diffuse.rgb = diffusecolour.rgb;
#endif
	diffuse.a = shininess;

#ifdef V_REFLECTION

	float3 reflection = reflectionmap.Sample(reflectionSampler, reflectionpos).rgb;

	diffuse.rgb = diffuse.rgb * 0.2f + reflection.rgb * 0.8f;

#endif


	depth.rgba = innormal.w/farclip;
	//depth.a = 1;
}
