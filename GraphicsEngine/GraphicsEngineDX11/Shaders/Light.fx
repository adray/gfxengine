﻿/*

The linear depth is based on the article:
http://mynameismjp.wordpress.com/2009/03/10/reconstructing-position-from-depth/

The deferred rendering is based on the articles:
http://www.catalinzima.com/tutorials/deferred-rendering-in-xna/

The percentage-closest filtering is based on the article:
http://http.developer.nvidia.com/GPUGems/gpugems_ch11.html

*/

#include "gfx.fxh"

Texture2D normalmap : register (t0);
Texture2D depthmap : register (t1);

//could use a single sampler
SamplerState normalSampler : register (s0);
SamplerState depthSampler : register (s1);

#ifdef V_SHADOWMAP

#define WIDTH 1024
#define HEIGHT 1024
Texture2D shadowmap : register (t2);

SamplerState shadowSampler : register (s2);

#endif

cbuffer Constants : register (b0)
{

	float4 campos;      //in VS is origin
	float4 lightpos;    //in VS
	float4 lightdir;    //in VS
	float4 lightcolor;
	matrix worldViewProj;
	matrix worldView;
    float4 farclip; //store as w component in spare
    matrix viewToLight;	//camInverseView
	matrix lightviewproj; //lightView * lightproj

};

cbuffer FrustumCorners : register (b1)
{
    float4 frustumCornerVS[4];
};

float4 DirectionalLightVertex(in float3 position : POSITION, out float2 tex : TEXCOORD0, out float3 ray : TEXCOORD1) : SV_POSITION
{
	tex = (position.xy+1)/2;
	ray = frustumCornerVS[position.z].xyz;
    return float4(position.xy, 0, 1);
}


float4 DirectionalLightPixel(in float4 position : SV_POSITION, in float2 tex : TEXCOORD0, in float3 ray : TEXCOORD1) : SV_TARGET
{
	
	float4 normal = decodeNormal(normalmap.Sample(normalSampler, float2(tex.x,1-tex.y)).rgba);
	//if (abs(normal.x) < 0.005 && abs(normal.y) < 0.005 && abs(normal.z) < 0.005)
		//normal.xyz *= 0;

	float dotlight = saturate(dot(-normalize(lightdir.xyz), normal.xyz));

	float depth = depthmap.Sample(depthSampler, float2(tex.x,1-tex.y)).r;

	#ifdef V_SHADOWMAP

	float3 viewpos = ray.xyz * depth ;
	
	float4 worldpos = mul(float4(viewpos,1), viewToLight);
	worldpos/=worldpos.w;

	float4 lightprojpos = mul(worldpos, lightviewproj);
	lightprojpos = lightprojpos/lightprojpos.w;
	float lightdepth = lightprojpos.z;
	
	float4 total=0;

	for (float i = -2; i <= 2; i++)
		for (float j = -2; j <= 2; j++) 
	{
	float shadowdepth=shadowmap.Sample(shadowSampler, float2(lightprojpos.x/2.0f+0.5f+i/WIDTH,-lightprojpos.y/2.0f+0.5f+j/HEIGHT)).r;



	[flatten]
	if (lightdepth-0.0005f < shadowdepth)
	{	//in light

	#endif

    float3 H = (-normalize(lightdir.xyz) + normalize(-ray));

	float specular = pow(saturate(dot(normalize(H), normal.xyz)), 10);


	float4 color;
	color.rgb = float3(dotlight * lightcolor.rgb);
	color.a = specular * normal.w;

	//color.rgb = //encodeNormal(normal).rgb;
	//		normalmap.Sample(normalSampler, float2(tex.x,-tex.y)).rgb;

	#ifdef V_SHADOWMAP
	total+=color;
	}
	else
	{
	total+=0.1f;
	}
	}
	return total/=25;
	
	#else

	return color;

	#endif


}

float4 PointLightVertex(in float4 position : POSITION, out float4 positionVS : TEXCOORD0, out float2 tex : TEXCOORD1 ) : SV_POSITION
{
	
	positionVS = mul(position, worldView);

	float4 ret = mul(position, worldViewProj);

	tex = ret.xy/ret.w;
	
    return ret;
}


float4 PointLightPixel(in float4 position : SV_POSITION, in float4 positionVS : TEXCOORD0, in float2 tex : TEXCOORD1) : SV_TARGET
{
	
	tex = (tex+1)/2;
	tex.y = 1-tex.y;

	float3 lightradius = lightdir.xyz;

	float4 normal = decodeNormal(normalmap.Sample(normalSampler, float2(tex.x,tex.y)));

	float depth = depthmap.Sample(depthSampler, float2(tex.x,tex.y)).r;

	float3 viewpos = positionVS.xyz * farclip.x/positionVS.z * depth;

	//return float4(1,0,0,0);

	float3 dist = viewpos.xyz - lightpos.xyz;

	//if (dist.x*dist.x / (lightradius.x*lightradius.x) + dist.y*dist.y / (lightradius.y*lightradius.y)
	//	+ dist.z*dist.z / (lightradius.z*lightradius.z) <= 1)
	{

		float3 clightdir = normalize(dist);

		float dotlight = saturate(dot(clightdir, -normal.xyz));

		float3 H = -clightdir.xyz + normalize(- viewpos.xyz);

		float specular = pow(saturate(dot(normalize(H), -normal.xyz)), 10);

		//float dist = length(lightradius.xyz) - length(viewpos.xyz-lightpos.xyz);

		float4 color;
		color.rgb = //worldpos.xyz;
			//lightcolor.rgb;
			float3(dotlight * lightcolor.rgb );
			//normalmap.Sample(normalSampler, float2(tex.x,tex.y)).rgb;
			//depthmap.Sample(depthSampler, float2(tex.x,tex.y)).r;
		color.a = 0;//specular;
		return color;

	}
	//else
	//{
	//	return float4(0,0,0,0);
	//}

	

}


float4 SpotLightVertex(in float4 position : POSITION, out float2 tex : TEXCOORD0, out float4 positionVS : TEXCOORD1) : SV_POSITION
{
	positionVS = mul(position, worldView);

	float4 ret = mul(position, worldViewProj);

	tex = ret.xy/ret.w;
	
    return ret;
}


float4 SpotLightPixel(in float4 position : SV_POSITION, in float2 tex : TEXCOORD0, in float4 positionVS : TEXCOORD1) : SV_TARGET
{
	
	tex = (tex+1)/2;
	tex.y = 1-tex.y;

	float4 normal = decodeNormal(normalmap.Sample(normalSampler, float2(tex.x,tex.y)));

	float depth = depthmap.Sample(depthSampler, float2(tex.x,tex.y)).r;

	float3 viewpos = positionVS.xyz * farclip.x/positionVS.z * depth;

	 

	float3 lightdir = normalize(viewpos.xyz-lightpos.xyz);

	float dotlight = saturate(dot(lightdir, normal.xyz));

    float3 H = -lightdir.xyz + normalize(- viewpos.xyz);

	float specular = pow(saturate(dot(normalize(H), -normal.xyz)), 10);

	float dist = 0.3f - length(viewpos.xyz-lightpos.xyz);

	float4 color;
	color.rgb = //worldpos.xyz;
		//lightcolor.rgb;
		float3(dotlight * lightcolor.rgb / (1 - dist) );
		//normalmap.Sample(normalSampler, float2(tex.x,tex.y)).rgb;
		//depthmap.Sample(depthSampler, float2(tex.x,tex.y)).r;
	color.a = 0;//specular;

	return color;

}
