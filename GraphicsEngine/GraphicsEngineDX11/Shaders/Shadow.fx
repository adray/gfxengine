﻿
cbuffer Constants : register (c0)
{

	matrix world;
	matrix proj;
	matrix view;

};

#ifndef V_INSTANCE

void RenderVertex(float4 position : POSITION, in float4 normal : NORMAL, in float2 incoord : TEXCOORD,
	out float4 outpos : SV_POSITION )
{

	float4 viewpos = mul(mul(position, world), view);
    outpos = mul(viewpos, proj);

}

#else

void RenderVertex(float4 position : POSITION, in float4 normal : NORMAL, in float2 incoord : TEXCOORD,
	in float4 row1 : TEXCOORD1, in float4 row2 : TEXCOORD2, in float4 row3 : TEXCOORD3, in float4 row4 : TEXCOORD4,
	out float4 outpos : SV_POSITION )
{

	
	matrix world={row1,row2,row3,row4};

	float4 viewpos = mul(mul(position, world), view);
    outpos = mul(viewpos, proj);

}

#endif


void RenderPixel(in float4 position : SV_POSITION, out float4 depth : SV_TARGET)
{

	depth.rgb = position.z/position.w;
	depth.a = 1;
}
