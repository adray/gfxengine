﻿
//Functions for encoding and decoding normals

float4 encodeNormal(float4 normal)
{

	return float4(0, ((normalize(normal.xyz)+1)/2));

}

float4 decodeNormal(float4 normal)
{
	return float4(normal.rgb*2-1,normal.a);
}

