﻿#include "gfx.fxh"

#define WIDTH 256
#define HEIGHT 256

Texture2D posmap : register (t1);
SamplerState posSampler : register (s1);

#ifdef V_BUMPMAP 

Texture2D normalmap : register (t0);
SamplerState normalSampler : register (s0);

#endif

#ifdef V_DIFFUSEMAP

SamplerState diffuseSampler : register (s2);


Texture2D blendtexture1 : register (t2);
Texture2D blendtexture2 : register (t3);

#endif

Texture2D blendmap : register (t4);


cbuffer Constants : register (c0)
{

	matrix world;
	matrix proj;
	matrix view;


    float3 diffusecolour;
	float farclip;
	float specular;
	float shininess;

};


void RenderVertex(in float2 incoord : POSITION, in float3 normal : NORMAL, out float4 outpos : SV_POSITION, out float4 outnormal : NORMAL,
	out float2 outcoord : TEXCOORD, out float4 blend : TEXCOORD1 )
{
	
	float y = posmap.SampleLevel(posSampler, incoord, 0).r;
	float4 pos = float4(incoord.x, y, incoord.y, 1);
	//sample some other nearby pixels to guess normal

	//float2 var = incoord+float2(1.0f/WIDTH,0/HEIGHT);
	//float3 dist1 = normalize(float3(var.x, posmap.SampleLevel(posSampler, var, 0).r, var.y) - pos.xyz);

	//var = incoord+float2(0/WIDTH,1.0f/HEIGHT);
	//float3 dist2 = normalize(float3(var.x, posmap.SampleLevel(posSampler, var, 0).r, var.y) - pos.xyz);

	float4 viewpos = mul(mul(pos, world), view);
    outpos = mul(viewpos, proj);
	//outnormal = float4(mul(mul(float4(normal.xyz, 0), world), view).xyz, viewpos.z);
	outnormal = float4(mul(float4(normal.xyz, 0), view).xyz, viewpos.z);
	outcoord = incoord*float2(world._11/world._22, world._33/world._22);

	//lookup blending
	blend = blendmap.SampleLevel(posSampler, incoord, 0);
}


void RenderPixel(in float4 position : SV_POSITION, in float4 innormal : NORMAL, in float2 incoord : TEXCOORD,

	in float4 blend : TEXCOORD1,

	out float4 normals : SV_TARGET, out float4 diffuse : SV_TARGET1, out float4 depth : SV_TARGET2)
{
#ifdef V_BUMPMAP
	float3 n = decodeNormal(normalmap.Sample(normalSampler, incoord)).xyz;

	//float3 helper = normalize(float3(innormal.x+0.001f, innormal.y+0.001f, -innormal.z+0.0001f));
	float3 helper = normalize(float3(0.5f, 0.25f, -0.7f));
	float3 binormal = cross(innormal.xyz, helper);
	float3 tangent = cross(innormal.xyz, binormal);

	innormal.xyz = innormal.xyz + n.y * binormal + n.x * tangent;

	normals.argb = encodeNormal(innormal).xyzw;
#else
    normals.argb = encodeNormal(innormal).xyzw;
#endif
normals.a = specular;

#ifdef V_DIFFUSEMAP
	diffuse.rgb = blendtexture1.Sample(diffuseSampler, incoord).rgb * blend.a + blendtexture2.Sample(diffuseSampler, incoord).rgb * blend.r;
#else
	diffuse.rgb = diffusecolour.rgb;
#endif
	diffuse.a = shininess;

	depth.rgba = innormal.w/farclip;

}





