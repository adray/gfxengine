﻿
#define V_INSTANCE

Texture2D diffusemap : register (t0);

SamplerState diffuseSampler : register (s0);

cbuffer Constants : register (c0)
{

	matrix proj;
	matrix view;
	
};

cbuffer Constants : register (c1)
{

	float4 position;
	float4 campos;
	float4 scale;
	float4 pad3;

}

#ifdef V_INSTANCE
#define V_VERTEX

float4 BillboardVertex(float index : POSITION, in float3 position : TEXCOORD0, out float2 tex : TEXCOORD0 ) : SV_POSITION
{

	float4 worldpos;
	float3 dir = normalize(position.xyz - campos.xyz);
	float3 vec1 = normalize( cross( dir, float3(0,1,0) ) );
	float3 vec2 = normalize( cross( vec1, dir ) );

	if (index == 0)
	{
		worldpos = float4(position.xyz - scale.x * vec1 - scale.y * vec2, 1);
		tex = float2(0,0);
	}
	else if (index == 1)
	{
		worldpos = float4(position.xyz + scale.x * vec1 - scale.y * vec2, 1);
		tex = float2(1,0);
	}
	else if (index == 2)
	{
		worldpos = float4(position.xyz - scale.x * vec1 + scale.y * vec2, 1);
		tex = float2(0,1);
	}
	else
	{
		worldpos = float4(position.xyz + scale.x * vec1 + scale.y * vec2, 1);
		tex = float2(1,1);
	}


    return mul(mul(worldpos, view), proj);
}

#endif

#ifndef V_VERTEX
float4 BillboardVertex(float index : POSITION, out float2 tex : TEXCOORD0) : SV_POSITION
{

	float4 worldpos;
	float3 dir = normalize(position.xyz - campos.xyz);
	float3 vec1 = normalize( cross( dir, float3(0,1,0) ) );
	float3 vec2 = normalize( cross( vec1, dir ) );

	if (index == 0)
	{
		worldpos = float4(position.xyz - scale.x * vec1 - scale.y * vec2, 1);
		tex = float2(0,0);
	}
	else if (index == 1)
	{
		worldpos = float4(position.xyz + scale.x * vec1 - scale.y * vec2, 1);
		tex = float2(1,0);
	}
	else if (index == 2)
	{
		worldpos = float4(position.xyz - scale.x * vec1 + scale.y * vec2, 1);
		tex = float2(0,1);
	}
	else
	{
		worldpos = float4(position.xyz + scale.x * vec1 + scale.y * vec2, 1);
		tex = float2(1,1);
	}


    return mul(mul(worldpos, view), proj);
}
#endif

void BillboardPixel(in float4 pos : SV_POSITION, in float2 tex : TEXCOORD0, out float4 outcolor : SV_Target)
{

	float4 diffuse = diffusemap.Sample(diffuseSampler, tex);

	outcolor.rgb = diffuse.rgb;
	outcolor.a = diffuse.r*scale.w;
	
}





