﻿

Texture2D diffusemap : register (t0);
Texture2D lightmap : register (t1);

//could use a single sampler
SamplerState diffuseSampler : register (s0);
SamplerState lightSampler : register (s1);

cbuffer Constants : register (c0)
{

	float4 ambient;
	float4 pad1;
	float4 pad2;
	float4 pad3;

};

float4 FinalVertex(float2 position : POSITION, out float2 tex : TEXCOORD0) : SV_POSITION
{
	tex = (position.xy+1)/2;
    return float4(position, 0, 1);
}

void FinalPixel(in float4 pos : SV_POSITION, in float2 tex : TEXCOORD0, out float4 outcolor : SV_Target)
{

	tex.y = 1-tex.y;
	float4 light = lightmap.Sample(lightSampler, tex);
	
	float4 diffuse = diffusemap.Sample(diffuseSampler, tex);

	outcolor.rgb = saturate( (light * diffuse).rgb /*+ ambient.rgb*/ + light.a);
	
	outcolor.a = 1;
	
}


