﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GraphicsEngineBase;
using SlimDX.Direct3D11;
using SlimDX.Windows;
using System.Windows.Forms;
using SlimDX;
using GraphicsEngineBase.Loader;

namespace GraphicsEngineDX11
{
    internal sealed class DeviceDescription : GraphicsEngineBase.DeviceDescription
    {
        internal DeviceDescription()
            : base("DirectX11")
        {

        }

        public override GraphicsEngineBase.Device MakeDevice(IntPtr window)
        {
            return new DirectX11Device(window);
        }

        public static DeviceDescription GetDeviceDescription()
        {
            return new DeviceDescription();
        }
    }

    public sealed class DirectX11Device : GraphicsEngineBase.Device
    {

        private Form window;
        private SlimDX.Direct3D11.Device device;
        private SlimDX.DXGI.SwapChain swapchain;
        private RenderTargetView rendertargetview;
        private RenderTargetView[] currenttargets;
        private DepthStencilView depthview;
        private Renderer renderer;
        private SlimDX.D3DCompiler.ShaderSignature signature;
        private Viewport viewport;

        private const SlimDX.DXGI.Format renderTargetFormat = //SlimDX.DXGI.Format.R32G32B32A32_Float;
                                                               SlimDX.DXGI.Format.R8G8B8A8_UNorm;
        private const SlimDX.DXGI.Format hdrRenderTargetFormat = SlimDX.DXGI.Format.R16G16B16A16_Float;
           // SlimDX.DXGI.Format.R32G32B32A32_Float;

        private System.Collections.Hashtable constantBufferPool;

        //cached buffers
        private VertexShader vertexshader;
        private PixelShader pixelshader;
        private ShaderResourceView[] texture_pix;
        private SamplerState[] samplers;
        private Dictionary<SlimDX.D3DCompiler.ShaderSignature,InputLayout> layouts;

        private class IncludeHandler : SlimDX.D3DCompiler.Include
        {

            public void Close(System.IO.Stream stream)
            {
                stream.Close();
            }

            public void Open(SlimDX.D3DCompiler.IncludeType type, string fileName, System.IO.Stream parentStream, out System.IO.Stream stream)
            {

                System.IO.StreamReader reader = new System.IO.StreamReader(System.Reflection.Assembly.GetExecutingAssembly()
                    .GetManifestResourceStream("Shaders." + fileName));

                string data =reader.ReadToEnd();
                
                stream = new System.IO.MemoryStream(System.Text.Encoding.UTF8.GetBytes(data));

            }
        }

        public static GraphicsEngineBase.DeviceDescription DeviceDescription()
        {
            return new DeviceDescription();
        }

        /// <summary>
        /// 
        /// </summary>
        internal DirectX11Device(IntPtr handle)
        {

            this.window = (Form) Form.FromHandle(handle);

            try
            {

                SlimDX.DXGI.SwapChainDescription description = new SlimDX.DXGI.SwapChainDescription
                {
                    BufferCount = 2,
                    Flags = SlimDX.DXGI.SwapChainFlags.AllowModeSwitch,
                    IsWindowed = true,
                    ModeDescription = new SlimDX.DXGI.ModeDescription(0, 0, new SlimDX.Rational(60,1), SlimDX.DXGI.Format.R8G8B8A8_UNorm),
                    OutputHandle = window.Handle,
                    SampleDescription = new SlimDX.DXGI.SampleDescription(1, 0),
                    SwapEffect = SlimDX.DXGI.SwapEffect.Discard,
                    Usage = SlimDX.DXGI.Usage.RenderTargetOutput
                };

                SlimDX.DXGI.Factory factory = new SlimDX.DXGI.Factory();
#if DEBUG
                System.Console.WriteLine(String.Format("Adapters found: {0}", factory.GetAdapterCount()));
#endif
                SlimDX.DXGI.Adapter adapter=null;
                for (int i = 0; i < factory.GetAdapterCount(); i++)
                {
                    adapter = factory.GetAdapter(i);
                    System.Console.WriteLine(String.Format("Adapter {0} is {1}", i+1, adapter.Description.Description));
                }


#if DEBUG

                //attempt to create a device with debugging enabled
                try
                {
                    
                    SlimDX.Direct3D11.Device.CreateWithSwapChain(adapter,
                        //DriverType.Hardware,
                        DeviceCreationFlags.Debug,
                        description,
                        out device,
                        out swapchain);

                }
                catch (Direct3D11Exception ex)
                {
                    Console.WriteLine("Debug not supported, falling back on regular device.");
                    //fall back on the regular device
                    SlimDX.Direct3D11.Device.CreateWithSwapChain(adapter,
                        //DriverType.Hardware,
                        DeviceCreationFlags.None,
                        description,
                        out device,
                        out swapchain);
                }

 #else               
                SlimDX.Direct3D11.Device.CreateWithSwapChain(adapter,
                        //DriverType.Hardware,
                        DeviceCreationFlags.None,
                        description,
                        out device,
                        out swapchain);
#endif

                factory.Dispose();

            }
            catch (Direct3D11Exception ex)
            {

                throw new DeviceCouldNotBeCreatedException();

            }

            

            //check requirements - perhaps create a different renderer if support is not met

            FormatSupport colourRenderTargetSupport = device.CheckFormatSupport(DirectX11Device.renderTargetFormat);
            if (!((colourRenderTargetSupport & FormatSupport.RenderTarget) == FormatSupport.RenderTarget))
                throw new DeviceCouldNotBeCreatedException();

            FormatSupport depthRenderTargetSupport = device.CheckFormatSupport(SlimDX.DXGI.Format.R32_Float);
            if (!((depthRenderTargetSupport & FormatSupport.RenderTarget) == FormatSupport.RenderTarget))
                throw new DeviceCouldNotBeCreatedException();

            FormatSupport depthStencilSupport = device.CheckFormatSupport(SlimDX.DXGI.Format.D24_UNorm_S8_UInt);
            if (!((depthStencilSupport & FormatSupport.DepthStencil) == FormatSupport.DepthStencil))
                throw new DeviceCouldNotBeCreatedException();

            FormatSupport hdrRenderTargetSupport = device.CheckFormatSupport(hdrRenderTargetFormat);
            if (!((hdrRenderTargetSupport & FormatSupport.RenderTarget) == FormatSupport.RenderTarget))
                throw new DeviceCouldNotBeCreatedException();

            using (var resource = Resource.FromSwapChain<Texture2D>(swapchain, 0))
                rendertargetview = new RenderTargetView(device, resource);

#if !DEBUG
            //for some reason PIX doesn't like this
            using (var resource = swapchain.GetParent<SlimDX.DXGI.Factory>())
                resource.SetWindowAssociation(this.window.Handle, SlimDX.DXGI.WindowAssociationFlags.IgnoreAltEnter);

#endif

            using (var resource = new Texture2D(device,
                new Texture2DDescription()
                {
                    ArraySize = 1,
                    BindFlags = BindFlags.DepthStencil,//|BindFlags.ShaderResource,
                    CpuAccessFlags = CpuAccessFlags.None,
                    Format = SlimDX.DXGI.Format.D24_UNorm_S8_UInt,
                    Height = rendertargetview.Resource.AsSurface().Description.Height,
                    Width = rendertargetview.Resource.AsSurface().Description.Width,
                    MipLevels = 1,
                    OptionFlags = ResourceOptionFlags.None,
                    SampleDescription = new SlimDX.DXGI.SampleDescription(1, 0),
                    Usage = ResourceUsage.Default
                }))
            {

                depthview = new DepthStencilView(device, resource);
                //swapchain.Description.ModeDescription.Width
            }

            SetStencilBuffer();

            //device.ImmediateContext.OutputMerger.SetTargets(depthview, rendertargetview);

            viewport = new Viewport(0, 0, Width, Height);

            device.ImmediateContext.Rasterizer.SetViewports(viewport);

            SetRasterState(true, true);

            constantBufferPool = new System.Collections.Hashtable();
            texture_pix = new ShaderResourceView[8];
            samplers = new SamplerState[8];
            layouts = new Dictionary<SlimDX.D3DCompiler.ShaderSignature, InputLayout>();

            renderer = new DirectX11Renderer(this);

            window.KeyDown += (o, e) =>
                {
                    if (e.Alt && e.KeyCode == Keys.Enter)
                        SetFullscreen(!swapchain.IsFullScreen);
                };

            window.ResizeEnd += (o, e) =>
                {
                    //Resize();
                };
        }

        /// <summary>
        /// Sets whether the window is fullscreen.
        /// </summary>
        /// <param name="fullscreen"></param>
        public void SetFullscreen(bool fullscreen)
        {

            //SlimDX.DXGI.Output target = SlimDX.DXGI.Output.FromPointer(swapchain.Description.OutputHandle);

            swapchain.SetFullScreenState(fullscreen, null);
            //Resize();

        }

        /// <summary>
        /// Resizes the window.
        /// </summary>
        private void Resize()
        {
            throw new NotImplementedException();

            rendertargetview.Dispose();

            //swapchain.ResizeTarget(swapchain.Description.ModeDescription);

            swapchain.ResizeBuffers(2, 0, 0,
                SlimDX.DXGI.Format.R8G8B8A8_UNorm, SlimDX.DXGI.SwapChainFlags.AllowModeSwitch);

            using (var resource = Resource.FromSwapChain<Texture2D>(swapchain, 0))
                rendertargetview = new RenderTargetView(device, resource);


            depthview.Dispose();

            using (var resource = new Texture2D(device,
                new Texture2DDescription()
                {
                    ArraySize = 1,
                    BindFlags = BindFlags.DepthStencil,
                    CpuAccessFlags = CpuAccessFlags.None,
                    Format = SlimDX.DXGI.Format.D24_UNorm_S8_UInt,
                    Height = Height,
                    Width = Width,
                    MipLevels = 1,
                    OptionFlags = ResourceOptionFlags.None,
                    SampleDescription = new SlimDX.DXGI.SampleDescription(1, 0),
                    Usage = ResourceUsage.Default
                }))
                depthview = new DepthStencilView(device, resource);

            device.ImmediateContext.OutputMerger.SetTargets(depthview, rendertargetview);

            viewport.Width = Width;
            viewport.Height = Height;

        }

        /// <summary>
        /// Sets how the rasters strategy.
        /// </summary>
        /// <param name="backfaceculling">True, to enable backface culling.</param>
        /// <param name="solid">True, to enable filling the traingles, rather than wireframe.</param>
        internal void SetRasterState(bool backfaceculling, bool solid)
        {
            device.ImmediateContext.Rasterizer.State = RasterizerState.FromDescription(device,
                new RasterizerStateDescription()
                {
                    CullMode = backfaceculling ? CullMode.Front : CullMode.None,
                    DepthBias = 0,
                    DepthBiasClamp = 0,
                    FillMode = solid ? FillMode.Solid : FillMode.Wireframe,
                    IsAntialiasedLineEnabled = false,
                    IsDepthClipEnabled = true,
                    IsFrontCounterclockwise = true,
                    IsMultisampleEnabled = false,
                    IsScissorEnabled = false,
                    SlopeScaledDepthBias = 0
                });
        }


        /// <summary>
        /// Gets the DirectX11 based renderer.
        /// </summary>
        /// <returns></returns>
        public override Renderer Renderer()
        {

            return renderer;

        }

        /// <summary>
        /// Creates a HDR render target
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <returns></returns>
        internal RenderTargetView CreateHDRRenderTarget(int width, int height)
        {

            Texture2DDescription description = new Texture2DDescription
            {
                ArraySize = 1,
                BindFlags = BindFlags.RenderTarget | BindFlags.ShaderResource,
                CpuAccessFlags = CpuAccessFlags.None,
                Format = hdrRenderTargetFormat,
                Height = height,
                Width = width,
                MipLevels = 1,
                OptionFlags = ResourceOptionFlags.None,
                SampleDescription = new SlimDX.DXGI.SampleDescription(1, 0),
                Usage = ResourceUsage.Default
            };

            RenderTargetView view = CreateRenderTarget(description);

            return view;

        }

        /// <summary>
        /// Creates a render target with the same dimensions as the window.
        /// </summary>
        /// <returns></returns>
        internal RenderTargetView CreateRenderTarget()
        {

            return CreateRenderTarget(this.Width, this.Height);

        }

        /// <summary>
        /// Creates a render target with specific dimenisons.
        /// </summary>
        /// <param name="width">Width of the render target.</param>
        /// <param name="height">Height of the render target.</param>
        /// <returns></returns>
        internal RenderTargetView CreateRenderTarget(int width, int height)
        {

            Texture2DDescription description = new Texture2DDescription
            {
                ArraySize=1,
                BindFlags=BindFlags.RenderTarget|BindFlags.ShaderResource,
                CpuAccessFlags=CpuAccessFlags.None,
                Format=renderTargetFormat,
                Height = height,
                Width = width,
                MipLevels=1,
                OptionFlags=ResourceOptionFlags.None,
                SampleDescription=new SlimDX.DXGI.SampleDescription(1,0),
                Usage=ResourceUsage.Default
            };

            RenderTargetView view = CreateRenderTarget(description);

            return view;

        }


        /// <summary>
        /// Creates a render target from the format.
        /// </summary>
        /// <param name="description"></param>
        /// <returns></returns>
        private RenderTargetView CreateRenderTarget(Texture2DDescription description)
        {
            RenderTargetView view;

            using (Texture2D texture = new Texture2D(device, description))
            {

                //view = new RenderTargetView(device, texture,
                //    new RenderTargetViewDescription()
                //    {
                //        Format = renderTargetFormat,
                //        Dimension = RenderTargetViewDimension.Texture2D,
                //        MipSlice = 0
                //    });

                view = new RenderTargetView(device, texture);

            }
            return view;
        }



        /// <summary>
        /// Clears render target and depth stencil target.
        /// </summary>
        /// <param name="colour"></param>
        internal void Clear(SlimDX.Color4 colour)
        {

            foreach (RenderTargetView target in currenttargets)
            {

                device.ImmediateContext.ClearRenderTargetView(target, colour);
                
            }

            if (device.ImmediateContext.OutputMerger.GetDepthStencilView() != null)
            {

                device.ImmediateContext.ClearDepthStencilView(device.ImmediateContext.OutputMerger.GetDepthStencilView(),
                    DepthStencilClearFlags.Depth | DepthStencilClearFlags.Stencil, 1, 0);

            }
        }

        /// <summary>
        /// Clears the render targets without clearing the depth stencil buffer.
        /// </summary>
        /// <param name="colour"></param>
        internal void ClearRenderTargets(Color4 colour)
        {
            foreach (RenderTargetView target in currenttargets)
            {

                device.ImmediateContext.ClearRenderTargetView(target, colour);

            }
        }

        /// <summary>
        /// Present scene to the back buffer.
        /// </summary>
        internal void Present(int syncInterval)
        {
            try
            {
                swapchain.Present(syncInterval, SlimDX.DXGI.PresentFlags.None);

            }
            catch (Exception ex)
            {
                Console.Write(ex.StackTrace);
            }

        }

        /// <summary>
        /// Disposes of resources
        /// </summary>
        public override void Dispose()
        {
            base.Dispose();

            renderer.Dispose();

            foreach (var list in constantBufferPool.Values)
                foreach (var buffer in (List<PooledResource<SlimDX.Direct3D11.Buffer>>)list)
                ((PooledResource<SlimDX.Direct3D11.Buffer>)buffer).Value.Dispose();

            foreach (var v in this.layouts)
                v.Value.Dispose();


            depthview.Dispose();
            rendertargetview.Dispose();
            swapchain.Dispose();
            device.Dispose();

        }

        /// <summary>
        /// Creates an vertex buffer.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        internal SlimDX.Direct3D11.Buffer CreateVertexBuffer(DataStream data)
        {

            return new SlimDX.Direct3D11.Buffer(device, data, (int)data.Length, ResourceUsage.Immutable, BindFlags.VertexBuffer,
                CpuAccessFlags.None, ResourceOptionFlags.None, 0);

        }

        /// <summary>
        /// Creates an index buffer.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        internal SlimDX.Direct3D11.Buffer CreateIndexBuffer(DataStream data)
        {

            return new SlimDX.Direct3D11.Buffer(device, data, (int)data.Length, ResourceUsage.Immutable, BindFlags.IndexBuffer,
                CpuAccessFlags.None, ResourceOptionFlags.None, 0);

        }

        /// <summary>
        /// Creates a constant buffer.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        internal PooledResource<SlimDX.Direct3D11.Buffer> CreateConstantBuffer(DataStream data)
        {
            PooledResource<SlimDX.Direct3D11.Buffer> c;

            List<PooledResource<SlimDX.Direct3D11.Buffer>> pool = (List<PooledResource<SlimDX.Direct3D11.Buffer>>)
                constantBufferPool[data.Length];

            if (pool != null)
            {

                foreach (var buffer in pool)
                {

                    if (!buffer.IsUsed)
                    {
                        DataBox b = device.ImmediateContext.MapSubresource(buffer.Value, MapMode.WriteDiscard, MapFlags.None);

                        data.Position = 0;
                        b.Data.Position = 0;
                        b.Data.WriteRange(data.DataPointer, data.Length);
                        b.Data.Position = 0;

                        device.ImmediateContext.UnmapSubresource(buffer.Value, 0);

                        return buffer;
                    }

                }
            }
            else
            {
                pool = new List<PooledResource<SlimDX.Direct3D11.Buffer>>();
                constantBufferPool.Add(data.Length, pool);
            }

            SlimDX.Direct3D11.Buffer constant =

            new SlimDX.Direct3D11.Buffer(device,
                    data,
                    new SlimDX.Direct3D11.BufferDescription((int)data.Length,
                        SlimDX.Direct3D11.ResourceUsage.Dynamic,
                        SlimDX.Direct3D11.BindFlags.ConstantBuffer,
                        SlimDX.Direct3D11.CpuAccessFlags.Write,
                        SlimDX.Direct3D11.ResourceOptionFlags.None,
                        0));

            c =  new PooledResource<SlimDX.Direct3D11.Buffer>(constant);

            pool.Add(c);

            return c;

        }

        /// <summary>
        /// Loads a vertex shader from a file.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="method"></param>
        /// <param name="defines"></param>
        /// <returns></returns>
        internal DirectX11VertexShader LoadVertexShader(string filename, string method, params string[] defines)
        {

            SlimDX.D3DCompiler.ShaderMacro[] def = new SlimDX.D3DCompiler.ShaderMacro[defines.Length];

            for (int i = 0; i < defines.Length; i++)
                def[i] = new SlimDX.D3DCompiler.ShaderMacro(defines[i]);

            System.IO.StreamReader sr = new System.IO.StreamReader(System.Reflection.Assembly.GetExecutingAssembly().
                GetManifestResourceStream("Shaders." + filename));

            string profile = "vs_4_0";

            using (var v = SlimDX.D3DCompiler.ShaderBytecode.Compile(sr.ReadToEnd(), method, profile,
                SlimDX.D3DCompiler.ShaderFlags.None, SlimDX.D3DCompiler.EffectFlags.None, def, new IncludeHandler()))
            {
                return new DirectX11VertexShader(new VertexShader(device, v),
                    SlimDX.D3DCompiler.ShaderSignature.GetInputSignature(v));
            }

        }

        /// <summary>
        /// Loads a pixel shader from a file.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="method"></param>
        /// <param name="defines"></param>
        /// <returns></returns>
        internal DirectX11PixelShader LoadPixelShader(string filename, string method, params string[] defines)
        {   

            SlimDX.D3DCompiler.ShaderMacro[] def = new SlimDX.D3DCompiler.ShaderMacro[defines.Length];

            for (int i = 0; i < defines.Length; i++)
                def[i] = new SlimDX.D3DCompiler.ShaderMacro(defines[i]);

            System.IO.StreamReader sr = new System.IO.StreamReader(System.Reflection.Assembly.GetExecutingAssembly().
                GetManifestResourceStream("Shaders." + filename));

            string errors;
            string profile = "ps_4_0";

            using (var v = SlimDX.D3DCompiler.ShaderBytecode.Compile(sr.ReadToEnd(), method, profile,
                SlimDX.D3DCompiler.ShaderFlags.None, SlimDX.D3DCompiler.EffectFlags.None,
                def, new IncludeHandler(), out errors))
            {
#if DEBUG
                System.Console.WriteLine(errors);
#endif
                return new DirectX11PixelShader(new PixelShader(device, v));
            }

        }

        /// <summary>
        /// Binds a pixel shader.
        /// </summary>
        /// <param name="shader"></param>
        internal void SetPixelShader(DirectX11PixelShader shader)
        {
            System.Diagnostics.Debug.Assert(shader != null, "Attempted to set pixel shader to null.");

            if (shader.Shader != pixelshader)
            {
                device.ImmediateContext.PixelShader.Set(shader.Shader);

                pixelshader = shader.Shader;
            }
        }

        /// <summary>
        /// Binds a vertex shader.
        /// </summary>
        /// <param name="shader"></param>
        internal void SetVertexShader(DirectX11VertexShader shader)
        {
            System.Diagnostics.Debug.Assert(shader != null, "Attempted to set vertex shader to null.");

            if (shader.Shader != vertexshader)
            {
                device.ImmediateContext.VertexShader.Set(shader.Shader);

                vertexshader = shader.Shader;
            }
            signature = shader.Signature;
        }

        /// <summary>
        /// Gets the semantic string from the type.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private string GetSemantic(DataType type)
        {

            switch (type)
            {
                case DataType.Colour:
                    return "COLOR";
                case DataType.Normal:
                    return "NORMAL";
                case DataType.Position:
                    return "POSITION";
                case DataType.Texcoord:
                    return "TEXCOORD";
                default:
                    throw new Exception("Semantic not found.");
            }

        }

        /// <summary>
        /// Gets the buffer format.
        /// </summary>
        /// <param name="format"></param>
        /// <returns></returns>
        private SlimDX.DXGI.Format GetFormat(DataFormat format)
        {

            switch (format)
            {
                case DataFormat.Float1:
                    return SlimDX.DXGI.Format.R32_Float;
                case DataFormat.Float2:
                    return SlimDX.DXGI.Format.R32G32_Float;
                case DataFormat.Float3:
                    return SlimDX.DXGI.Format.R32G32B32_Float;
                case DataFormat.Float4:
                    return SlimDX.DXGI.Format.R32G32B32A32_Float;
                default:
                    throw new Exception("Format not found.");
            }

        }

        /// <summary>
        /// Submits a mesh to the gpu.
        /// </summary>
        /// <param name="mesh"></param>
        internal void Render(DirectX11Mesh mesh)
        {

           

            if (mesh.PrimitiveCount == 0)
                return;

            mesh.UpdateBuffer(this);
            
            device.ImmediateContext.InputAssembler.PrimitiveTopology = PrimitiveTopology.TriangleList;

            InputLayout layout=null;

            if (layouts.ContainsKey(signature))
            {

                layout = layouts[signature];

            }
            else
            {
                //don't keep recreating this

                int elementcount = mesh.DataCollection.Format.Count;

                var elements = new InputElement[elementcount];

                System.Collections.Hashtable table = new System.Collections.Hashtable();

                for (int i = 0; i < elementcount; i++)
                {
                    DataType type = mesh.DataCollection.Type[i];
                    int index = 0;

                    if (table.ContainsKey(type))
                        table[type] = ((int)table[type]) + 1;
                    else
                        table.Add(type, 0);

                    elements[i] = new InputElement(GetSemantic(type), index, GetFormat(mesh.DataCollection.Format[i]), 0);
                }

                layout = new InputLayout(device, signature, elements);
                layouts.Add(signature, layout);
            }
            
            device.ImmediateContext.InputAssembler.InputLayout = layout;

            for (int i = 0; i < mesh.VertexBuffer.Count; i++)
            {

                //device.ImmediateContext.InputAssembler.SetIndexBuffer(mesh.IndexBuffer[i], SlimDX.DXGI.Format.R16_UInt, 0);
                device.ImmediateContext.InputAssembler.SetVertexBuffers(0, mesh.VertexBuffer[i]);

                //device.ImmediateContext.DrawIndexed(mesh.subsets[i].PrimitiveCount * 3, 0, 0);
                device.ImmediateContext.Draw(mesh.subsets[i].PrimitiveCount * 3, 0);

            }

            //layout.Dispose();

        }

        /// <summary>
        /// Submits an instance mesh to the gpu.
        /// </summary>
        /// <param name="mesh"></param>
        internal void Render(DirectX11Mesh dm, DirectX11Instance instance)
        {

            if (dm.PrimitiveCount == 0 || instance.InstanceCount == 0)
                return;

            dm.UpdateBuffer(this);

            instance.UpdateBuffer(this);

            device.ImmediateContext.InputAssembler.PrimitiveTopology = PrimitiveTopology.TriangleList;


            //don't keep recreating this

            int elementcount = dm.DataCollection.Format.Count;
            int instanceElementcount = instance.DataCollection.Format.Count;

            var elements = new InputElement[elementcount+instanceElementcount];

            System.Collections.Hashtable table = new System.Collections.Hashtable();

            for (int i = 0; i < elementcount; i++)
            {
                DataType type = dm.DataCollection.Type[i];
                int index = 0;

                if (table.ContainsKey(type))
                {
                    index = ((int)table[type]) + 1;
                    table[type] = index;
                }
                else
                    table.Add(type, 0);

                elements[i] = new InputElement(GetSemantic(type), index, GetFormat(dm.DataCollection.Format[i]), 0);

            }
            
            for (int i = 0; i < instanceElementcount; i++)
            {
                DataType type = instance.DataCollection.Type[i];
                int index = 0;

                if (table.ContainsKey(type))
                {
                    index = ((int)table[type]) + 1;
                    table[type] = index;
                }
                else
                    table.Add(type, 0);

                elements[elementcount + i] = new InputElement(GetSemantic(type), index,
                    GetFormat(instance.DataCollection.Format[i]), InputElement.AppendAligned, 1, InputClassification.PerInstanceData, 1);

            }

            var layout = new InputLayout(device, signature, elements);


            device.ImmediateContext.InputAssembler.InputLayout = layout;

            for (int i = 0; i < dm.VertexBuffer.Count; i++)
            {

                device.ImmediateContext.InputAssembler.SetIndexBuffer(dm.IndexBuffer[i], SlimDX.DXGI.Format.R16_UInt, 0);
                device.ImmediateContext.InputAssembler.SetVertexBuffers(0, dm.VertexBuffer[i]);
                device.ImmediateContext.InputAssembler.SetVertexBuffers(1, instance.InstanceBuffer);

                device.ImmediateContext.DrawIndexedInstanced(dm.subsets[i].PrimitiveCount * 3, instance.InstanceCount, 0, 0, 0);

            }

            layout.Dispose();

        }

        /// <summary>
        /// Sets some amount of render target.
        /// </summary>
        /// <param name="rendertargets"></param>
        internal void SetRenderTargets(params RenderTargetView[] rendertargets)
        {

#if DEBUG

            foreach (RenderTargetView v in rendertargets)
            {
                System.Diagnostics.Debug.Assert(v != null, "Should not set render target to null.");

                System.Diagnostics.Debug.Assert(v.Resource.AsSurface().Description.Width <= 
                    depthview.Resource.AsSurface().Description.Width, "Depth & render target dimensions different.");
            }

#endif

            if (currenttargets != null && currenttargets.Length > 1)
            {
                RenderTargetView[] toset = new RenderTargetView[Math.Max(currenttargets.Length, rendertargets.Length)];

                Array.Copy(rendertargets, toset, rendertargets.Length);

                device.ImmediateContext.OutputMerger.SetTargets(depthview, toset);
            }
            else
                device.ImmediateContext.OutputMerger.SetTargets(depthview, rendertargets);

            Viewport[] viewports=new Viewport[rendertargets.Length];

            for (int i = 0; i < rendertargets.Length; i++)
            {
                SlimDX.DXGI.SurfaceDescription surface = rendertargets[i].Resource.AsSurface().Description;
                viewports[i] = new Viewport(0, 0, surface.Width, surface.Height);
            }

            device.ImmediateContext.Rasterizer.SetViewports(viewports);

            currenttargets = rendertargets;

        }

        /// <summary>
        /// Sets the render target to the back buffer.
        /// </summary>
        internal void SetDefaultRenderTarget()
        {

#if DEBUG
            System.Diagnostics.Debug.Assert(rendertargetview.Resource.AsSurface().Description.Width <=
                    depthview.Resource.AsSurface().Description.Width, "Depth & render target dimensions different.");
#endif

            if (currenttargets != null && currenttargets.Length > 1)
            {
                RenderTargetView[] toset = new RenderTargetView[Math.Max(currenttargets.Length, 1)];

                toset[0] = rendertargetview;

                device.ImmediateContext.OutputMerger.SetTargets(depthview, toset);
            }
            else
                device.ImmediateContext.OutputMerger.SetTargets(depthview, rendertargetview);

            device.ImmediateContext.Rasterizer.SetViewports(viewport);

            currenttargets = new RenderTargetView[] { rendertargetview };
            
        }

        /// <summary>
        /// Create a texture sampler.
        /// </summary>
        /// <returns></returns>
        internal SamplerState CreateSampler()
        {

            return SamplerState.FromDescription(device,
                new SamplerDescription() {
                    AddressU=TextureAddressMode.Wrap,
                    AddressV=TextureAddressMode.Wrap,
                    AddressW=TextureAddressMode.Wrap,
                    ComparisonFunction=Comparison.Always,
                    Filter=Filter.MinMagMipLinear,
                    MaximumAnisotropy=0,
                    MinimumLod=0,
                    MaximumLod=float.MaxValue,
                    MipLodBias=0
                });

        }

        /// <summary>
        /// Create a reflection texture sampler.
        /// </summary>
        /// <returns></returns>
        internal SamplerState CreateReflectionSampler()
        {

            return SamplerState.FromDescription(device,
                new SamplerDescription()
                {
                    AddressU = TextureAddressMode.Clamp,
                    AddressV = TextureAddressMode.Clamp,
                    AddressW = TextureAddressMode.Clamp,
                    ComparisonFunction = Comparison.Always,
                    Filter = Filter.MinMagMipLinear,
                    MaximumAnisotropy = 0,
                    MinimumLod = 0,
                    MaximumLod = float.MaxValue,
                    MipLodBias = 0,
                    //BorderColor=new Color4(0,0,0)
                });

        }

        /// <summary>
        /// Creates a normal map texture sampler.
        /// </summary>
        /// <returns></returns>
        internal SamplerState CreateNormalSampler()
        {
            return SamplerState.FromDescription(device,
                new SamplerDescription()
                {
                    AddressU = TextureAddressMode.Wrap,
                    AddressV = TextureAddressMode.Wrap,
                    AddressW = TextureAddressMode.Wrap,
                    ComparisonFunction = Comparison.Always,
                    //Filter=Filter.MinMagMipLinear,
                    Filter = SlimDX.Direct3D11.Filter.MinMagMipLinear,
                    MaximumAnisotropy = 0,
                    MinimumLod = 0,
                    MaximumLod = float.MaxValue,
                    MipLodBias = 0
                });
        }

        /// <summary>
        /// Creates a shadow map texture sampler.
        /// </summary>
        /// <returns></returns>
        internal SamplerState CreateShadowSampler()
        {
            return SamplerState.FromDescription(device,
                new SamplerDescription()
                {
                    AddressU = TextureAddressMode.Border,
                    AddressV = TextureAddressMode.Border,
                    AddressW = TextureAddressMode.Wrap,
                    ComparisonFunction = Comparison.Always,
                    //Filter=Filter.MinMagMipLinear,
                    Filter = SlimDX.Direct3D11.Filter.MinMagMipLinear,
                    MaximumAnisotropy = 5,
                    MinimumLod = 0,
                    MaximumLod = 0,
                    MipLodBias = 0,
                    BorderColor=new Color4(1,0,0)
                });
        }

        /// <summary>
        /// Binds a sampler to a pixel shader slot.
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="slot"></param>
        internal void SetSampler(SamplerState sampler, int slot)
        {

            System.Diagnostics.Debug.Assert(sampler != null, "Tried to set a sample to null.");

            if (samplers[slot] != sampler)
            {
                samplers[slot] = sampler;
                device.ImmediateContext.PixelShader.SetSampler(sampler, slot);
            }
        }

        /// <summary>
        /// Binds a constant buffer to a pixel shader slot.
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="slot"></param>
        internal void SetPixelConstantBuffer(SlimDX.Direct3D11.Buffer buffer, int slot)
        {

            System.Diagnostics.Debug.Assert(buffer != null, "Tried to set a constant buffer to null.");
            //System.Diagnostics.Debug.Assert(buffer.Description., "Tried to set a constant buffer to null.");

            device.ImmediateContext.PixelShader.SetConstantBuffer(buffer, slot);

        }

        /// <summary>
        /// Binds a constant buffer to a vertex shader.
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="slot"></param>
        internal void SetVertexConstantBuffer(SlimDX.Direct3D11.Buffer buffer, int slot)
        {

            System.Diagnostics.Debug.Assert(buffer != null, "Tried to set a constant buffer to null.");

            device.ImmediateContext.VertexShader.SetConstantBuffer(buffer, slot);

        }

        /// <summary>
        /// Binds a render target to a slot on the pixel shader.
        /// </summary>
        /// <param name="texture"></param>
        /// <param name="slot"></param>
        internal void SetPixelTexture(RenderTargetView texture, int slot)
        {


            System.Diagnostics.Debug.Assert(texture != null, "Tried to set a texture to null.");

            ShaderResourceView resource = new ShaderResourceView(device, texture.Resource, new
                ShaderResourceViewDescription()
                {
                    Format = texture.Description.Format,
                    Dimension = ShaderResourceViewDimension.Texture2D,
                    MipLevels = 1,
                    MostDetailedMip = 0
                });



            using (resource)
            {
                SetPixelTexture(resource, slot);
            }


        }

        /// <summary>
        /// Binds a texture to a slot on the pixel shader.
        /// </summary>
        /// <param name="resource"></param>
        /// <param name="slot"></param>
        internal void SetPixelTexture(ShaderResourceView resource, int slot)
        {
            System.Diagnostics.Debug.Assert(resource != null, "Tried to set texture to null.");

            if (texture_pix[slot] != resource)
            {
                texture_pix[slot] = resource;
                device.ImmediateContext.PixelShader.SetShaderResource(resource, slot);
            }
        }

        /// <summary>
        /// Enables or disables depth testing.
        /// </summary>
        /// <param name="flag"></param>
        internal void EnableZBuffer(bool flag)
        {

            DepthStencilStateDescription state = new DepthStencilStateDescription()
            {
                DepthWriteMask = flag ? SlimDX.Direct3D11.DepthWriteMask.All : DepthWriteMask.Zero,
                IsDepthEnabled = flag,
                DepthComparison = Comparison.Less,

                BackFace = new DepthStencilOperationDescription()
                {
                    Comparison=Comparison.Always
                }
            };

            device.ImmediateContext.OutputMerger.DepthStencilState =
                DepthStencilState.FromDescription(device, state);

        }

        /// <summary>
        /// Enable a z-buffer without writing to it. This allows particles to be drawn in any order and blended together
        /// in addition to rejecting the particles occluded by other geometry. 
        /// </summary>
        /// <param name="flag"></param>
        internal void EnableZParticleBuffer(bool flag)
        {

            DepthStencilStateDescription state = new DepthStencilStateDescription()
            {
                DepthWriteMask = DepthWriteMask.Zero,
                IsDepthEnabled = flag,
                DepthComparison = Comparison.Less,

                BackFace = new DepthStencilOperationDescription()
                {
                    Comparison = Comparison.Always
                }
            };

            device.ImmediateContext.OutputMerger.DepthStencilState =
                DepthStencilState.FromDescription(device, state);

        }

        /// <summary>
        /// Enables blending. Used for mixing deferred lights.
        /// </summary>
        /// <param name="flag"></param>
        internal void EnableLightBlending(bool flag)
        {

            if (!flag)
            {
                device.ImmediateContext.OutputMerger.BlendState = null;
                return;
            }

            BlendStateDescription description = new BlendStateDescription()
                {
                    IndependentBlendEnable=false
                };

            description.RenderTargets[0] = new RenderTargetBlendDescription()
            {
                BlendEnable = flag,
                BlendOperation = BlendOperation.Add,
                DestinationBlend = BlendOption.One,
                SourceBlend = BlendOption.One,
                RenderTargetWriteMask = ColorWriteMaskFlags.All,
                BlendOperationAlpha = BlendOperation.Add,
                SourceBlendAlpha = BlendOption.One,
                DestinationBlendAlpha = BlendOption.One
            };



            device.ImmediateContext.OutputMerger.BlendState = BlendState.FromDescription(
                device, description);


        }

        /// <summary>
        /// Enables or disables alpha blending.
        /// </summary>
        /// <param name="flag"></param>
        internal void EnableAlphaDrawing(bool flag)
        {

            if (!flag)
            {
                device.ImmediateContext.OutputMerger.BlendState = null;
                return;
            }

            BlendStateDescription description = new BlendStateDescription()
            {
                IndependentBlendEnable = false
            };

            description.RenderTargets[0] = new RenderTargetBlendDescription()
            {
                BlendEnable = flag,
                BlendOperation = BlendOperation.Add,
                DestinationBlend = BlendOption.InverseSourceAlpha,
                SourceBlend = BlendOption.SourceAlpha,
                RenderTargetWriteMask = ColorWriteMaskFlags.All,
                BlendOperationAlpha = BlendOperation.Add,
                SourceBlendAlpha = BlendOption.SourceAlpha,
                DestinationBlendAlpha = BlendOption.DestinationAlpha
            };



            device.ImmediateContext.OutputMerger.BlendState = BlendState.FromDescription(
                device, description);


        }

        /// <summary>
        /// Enables or disables blending particles.
        /// </summary>
        /// <param name="flag"></param>
        internal void EnableParticleBlending(bool flag)
        {

            if (!flag)
            {
                device.ImmediateContext.OutputMerger.BlendState = null;
                return;
            }

            BlendStateDescription description = new BlendStateDescription()
            {
                IndependentBlendEnable = false
            };

            description.RenderTargets[0] = new RenderTargetBlendDescription()
            {
                BlendEnable = flag,
                BlendOperation = BlendOperation.Add,
                DestinationBlend = BlendOption.One,
                SourceBlend = BlendOption.One,
                RenderTargetWriteMask = ColorWriteMaskFlags.All,
                BlendOperationAlpha = BlendOperation.Add,
                SourceBlendAlpha = BlendOption.One,
                DestinationBlendAlpha = BlendOption.Zero
            };



            device.ImmediateContext.OutputMerger.BlendState = BlendState.FromDescription(
                device, description);


        }

        private Texture LoadTextureFromPointer(DataStream stream, int width, int height)
        {

            

            lock (device.ImmediateContext)
            {

                var recs = new DataRectangle[(int)Math.Log(Math.Max(width, height), 2) + 1];
                
                int i = 0;
                recs[i] = new DataRectangle(width * 4, stream);
                i++;
                do
                {
                    if (width > 1)
                        width /= 2;
                    if (height > 1)
                        height /= 2;
                    recs[i++] = new DataRectangle(width * 4, new DataStream(width * height * 4, true, true));
                } while (width != 1 || height != 1);

                Texture2D tga = new Texture2D(device, new Texture2DDescription()
                {
                    Width = width,
                    Height = height,
                    Format = SlimDX.DXGI.Format.R8G8B8A8_UNorm,
                    MipLevels = 0,
                    BindFlags = BindFlags.ShaderResource | BindFlags.RenderTarget,
                    CpuAccessFlags = CpuAccessFlags.None,
                    Usage = ResourceUsage.Default,
                    ArraySize = 1,
                    SampleDescription = new SlimDX.DXGI.SampleDescription(1, 0),
                    OptionFlags = ResourceOptionFlags.GenerateMipMaps,
                }, recs);


                Result res = Texture2D.FilterTexture(device.ImmediateContext, tga, 0, FilterFlags.Box);

                ShaderResourceView r = new ShaderResourceView(device, tga);
                device.ImmediateContext.GenerateMips(r);
                return new DirectX11Texture(r);
            }


        }

        /// <summary>
        /// Loads a texture from a file.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public override Texture LoadTexture(string filename)
        {
            Texture texture = null;

            if (System.Text.RegularExpressions.Regex.IsMatch(filename, ".*\\.tga"))
            {
                System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();


                TGAData data = TGALoader.Load(filename);

                var stream = new DataStream(data.Width * data.Height * 4, true, true);
                stream.Write(data.Data, 0, data.Data.Length);
                stream.Position = 0;

                LoadTextureFromPointer(stream, data.Width, data.Height);

            }
            else
                texture = new DirectX11Texture(ShaderResourceView.FromFile(device, filename));

            AddWatchedItem(new Watch(texture, filename, new Reload(
                (n) => {
                    try
                    {
                        return new DirectX11Texture(ShaderResourceView.FromFile(device, n));
                    }
                    catch (SlimDXException ex)
                    {
                        return texture; //keep with what we have
                    }
                }  )));

            return texture;

        }

        /// <summary>
        /// Loads a texture from a stream.
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        public override Texture LoadTexture(System.IO.Stream stream)
        {

            return new DirectX11Texture(ShaderResourceView.FromStream(device, stream, (int) stream.Length));
            
        }

        /// <summary>
        /// Loads a texture from a Bitmap.
        /// </summary>
        /// <param name="pointer"></param>
        /// <returns></returns>
        public override Texture LoadTexture(System.Drawing.Bitmap bitmap)
        {
            var pointer = bitmap.LockBits(new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height), System.Drawing.Imaging.ImageLockMode.ReadOnly,
                System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            
            var stream = new DataStream(pointer.Scan0, bitmap.Width*bitmap.Height*pointer.Stride, true, true);

            var texture = LoadTextureFromPointer(stream, bitmap.Width, bitmap.Height);

            bitmap.UnlockBits(pointer);

            return texture;
        }
        
        /// <summary>
        /// Gets the width of the back buffer.
        /// </summary>
        public override int Width
        {
            get
            {
                return swapchain.Description.ModeDescription.Width; //this.window.Width;
            }
        }

        /// <summary>
        /// Gets the height of the back buffer.
        /// </summary>
        public override int Height
        {
            get
            {
                return swapchain.Description.ModeDescription.Height; // this.window.Height;
            }
        }

        /// <summary>
        /// Gets the hardware description.
        /// </summary>
        public override string HardwareDescription
        {
            get
            {
                return this.device.Factory.GetAdapter(0).Description.Description;
            }
        }

        /// <summary>
        /// Gets the depth render target.
        /// </summary>
        /// <returns></returns>
        internal RenderTargetView CreateDepthRenderTarget()
        {

            Texture2DDescription description = new Texture2DDescription
            {
                ArraySize = 1,
                BindFlags = BindFlags.RenderTarget | BindFlags.ShaderResource,
                CpuAccessFlags = CpuAccessFlags.None,
                Format = SlimDX.DXGI.Format.R32_Float,
                Height = Height,
                Width = Width,
                //Height=256,
                //Width=256,
                MipLevels = 1,
                OptionFlags = ResourceOptionFlags.None,
                SampleDescription = new SlimDX.DXGI.SampleDescription(1, 0),
                Usage = ResourceUsage.Default
            };

            Texture2D texture = new Texture2D(device, description);

            return new RenderTargetView(device, texture,
                new RenderTargetViewDescription()
                {
                    Format = SlimDX.DXGI.Format.R32_Float,
                    Dimension = RenderTargetViewDimension.Texture2D,
                    MipSlice = 0
                });

        }

        /// <summary>
        /// Binds a stencil buffer.
        /// </summary>
        internal void SetStencilBuffer()
        {

            DepthStencilStateDescription desc = new DepthStencilStateDescription()
            {
                BackFace = new DepthStencilOperationDescription()
                {
                    Comparison= Comparison.Always,
                    DepthFailOperation=StencilOperation.Keep,
                    FailOperation=StencilOperation.Keep,
                    PassOperation=StencilOperation.Increment
                },
                FrontFace = new DepthStencilOperationDescription()
                {
                    Comparison=Comparison.Always,
                    DepthFailOperation=StencilOperation.Keep,
                    FailOperation=StencilOperation.Keep,
                    PassOperation=StencilOperation.Increment
                },
                DepthComparison=Comparison.Less,
                DepthWriteMask=DepthWriteMask.All,
                IsDepthEnabled=true,
                IsStencilEnabled=true,
                StencilReadMask=0xFF,
                StencilWriteMask=0xFF,
            };

            this.device.ImmediateContext.OutputMerger.DepthStencilState =
                DepthStencilState.FromDescription(device, desc);

        }

        /// <summary>
        /// Creates an empty mesh with the vertex description.
        /// </summary>
        /// <param name="vertextype"></param>
        /// <returns></returns>
        public override Mesh CreateMesh(VertexType vertextype)
        {

            return new DirectX11Mesh(vertextype);

        }

        public override ObjectFactory LoadObject(System.IO.Stream stream)
        {

            //if (System.Text.RegularExpressions.Regex.IsMatch(filename, ".*\\.obj"))
            {
                this.SuspendWatcher();
                var m = ObjLoader.Load(this, VertexType.PositionNormalTextured, new System.IO.StreamReader(stream));

                //AddWatchedItem(new Watch(m, filename, new Reload(
                //(n) =>
                //{
                //    try
                //    {
                //        return ObjLoader.Load(this, vertextype, n);
                //    }
                //    catch (SlimDXException ex)
                //    {
                //        return m; //keep with what we have
                //    }
                //})));

                this.ResumeWatcher();

                return m;
            }
            //else if (System.Text.RegularExpressions.Regex.IsMatch(filename, ".*\\.ply"))
            //{

            //    var mesh = PlyLoader.Load(this, VertexType.PositionNormalTextured, stream);

            //    var subsets = new List<ObjectSubset>();
            //    subsets.Add(new ObjectSubset()
            //    {
            //        Mesh = mesh
            //    });

            //    var instances = new List<ObjectInstanceSubset>();
            //    instances.Add(new ObjectInstanceSubset()
            //    {
            //        Mesh = mesh
            //    });

            //    GraphicsEngineBase.ObjectFactory factory = new ObjectFactory(this, subsets, instances);


            //    //AddWatchedItem(new Watch(m, filename, new Reload(
            //    //(n) =>
            //    //{
            //    //    try
            //    //    {
            //    //        return PlyLoader.Load(this, vertextype, n);
            //    //    }
            //    //    catch (SlimDXException ex)
            //    //    {
            //    //        return m; //keep with what we have
            //    //    }
            //    //})));

            //    return factory;
            //}

            throw new Exception("Failed to load mesh, format not recognized.");

        }

        /// <summary>
        /// Loads a mesh from a file with the vertex description.
        /// </summary>
        /// <param name="vertextype"></param>
        /// <param name="filename"></param>
        /// <returns></returns>
        public override GraphicsEngineBase.ObjectFactory LoadObject(string filename)
        {

            if (System.Text.RegularExpressions.Regex.IsMatch(filename, ".*\\.obj"))
            {
                this.SuspendWatcher();
                var m =ObjLoader.Load(this, VertexType.PositionNormalTextured, filename);

                //AddWatchedItem(new Watch(m, filename, new Reload(
                //(n) =>
                //{
                //    try
                //    {
                //        return ObjLoader.Load(this, vertextype, n);
                //    }
                //    catch (SlimDXException ex)
                //    {
                //        return m; //keep with what we have
                //    }
                //})));

                this.ResumeWatcher();

                return m;
            }
            else if (System.Text.RegularExpressions.Regex.IsMatch(filename, ".*\\.ply"))
            {

                var mesh = PlyLoader.Load(this, VertexType.PositionNormalTextured, filename);

                var subsets = new List<ObjectSubset>();
                subsets.Add(new ObjectSubset()
                {
                    Mesh = mesh
                });

                var instances = new List<ObjectInstanceSubset>();
                instances.Add(new ObjectInstanceSubset()
                {
                    Mesh = mesh
                });

                GraphicsEngineBase.ObjectFactory factory = new ObjectFactory(this, subsets, instances); 
                

                //AddWatchedItem(new Watch(m, filename, new Reload(
                //(n) =>
                //{
                //    try
                //    {
                //        return PlyLoader.Load(this, vertextype, n);
                //    }
                //    catch (SlimDXException ex)
                //    {
                //        return m; //keep with what we have
                //    }
                //})));

                return factory;
            }

            throw new Exception("Failed to load mesh, format not recognized.");
        }

        /// <summary>
        /// Loads a scene from a file.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public override Scene LoadScene(string filename)
        {

            if (System.Text.RegularExpressions.Regex.IsMatch(filename, ".*\\.dae"))
                return ColladaLoader.Load(this, filename);

            throw new Exception("Failed to load scene, format not recognized.");

        }

        /// <summary>
        /// Loads a heightmap from a file.
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public override TerrainObject LoadTerrain(string image)
        {
            return GraphicsEngineBase.Loader.TerrainLoader.Load(this, image);
        }

        /// <summary>
        /// Binds a texture to a vertex shader.
        /// </summary>
        /// <param name="texture"></param>
        /// <param name="slot"></param>
        internal void SetVertexTexture(Texture texture, int slot)
        {

            device.ImmediateContext.VertexShader.SetShaderResource(((DirectX11Texture)texture).texture, slot);

        }

        /// <summary>
        /// Binds a texture sampler to a vertex shader.
        /// </summary>
        /// <param name="sampler"></param>
        /// <param name="slot"></param>
        internal void SetVertexSampler(SamplerState sampler, int slot)
        {

            device.ImmediateContext.VertexShader.SetSampler(sampler, slot);

        }

        /// <summary>
        /// Creates a mesh instance.
        /// </summary>
        /// <returns></returns>
        public override MeshInstance CreateInstance()
        {

            return new DirectX11Instance(VertexType.Matrix);

        }

    }
}
