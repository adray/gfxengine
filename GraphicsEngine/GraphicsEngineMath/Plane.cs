﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GraphicsEngineMath
{
    /// <summary>
    /// Represents a 3-dimensional plane.
    /// </summary>
    public class Plane
    {

        public Vector3 Normal { get; private set; }
        public float D { get; private set; }

        public Plane(Vector3 normal, float d)
        {

            Normal = normal;
            D = -d;

        }

        public Plane(Vector3 pos, Vector3 dir1, Vector3 dir2)
        {

            Normal = Vector3.Cross(dir1, dir2);
            Normal.Normalize();
            
            D = -Vector3.Dot(Normal, pos);

        }

        public float Distance(Vector3 pos)
        {

            return ((Vector3.Dot(Normal, pos) + D) / Normal.Length());

        }

    }
}
