﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GraphicsEngineMath
{
    public class Quaternion
    {

        public float W { get; set; }
        public float X { get; set; }
        public float Y { get; set; }
        public float Z { get; set; }

        public Quaternion()
        {
            X = 0;
            Y = 0;
            Z = 0;
            W = 0;
        }

        public Quaternion(Vector3 v, float theta)
        {
            float rad = (float)Math.PI * 2 * theta / 360.0f;

            float c = (float)Math.Cos(rad/2);
            W = (float)Math.Sin(rad / 2);
            X = c * v.X;
            Y = c * v.Y;
            Z = c * v.Z;
        }

        public void Invert()
        {
            X = -X;
            Y = -Y;
            Z = -Z;
        }

        public void Normalize()
        {

            float mag = X * X + Y * Y + Z * Z + W * W;
            mag = (float)Math.Sqrt(mag);

            if (mag > 0)
            {

                X /= mag;
                Y /= mag;
                Z /= mag;
                W /= mag;

            }

        }

        public static Quaternion FromAngles(Vector3 angles)
        {

            Quaternion r = new Quaternion();

            r.X = (float)Math.Cos(angles.X * (float)Math.PI / 360.0f);
            r.Y = (float)Math.Cos(angles.Y * (float)Math.PI / 360.0f);
            r.Z = (float)Math.Cos(angles.Z * (float)Math.PI / 360.0f);
            r.W = 0;

            return r;
        }

        public Vector3 ToAngles()
        {
            const float e = 0.00001f;

            Vector3 angles = new Vector3();
            angles.X = Math.Abs(1/X) > e ? (float)Math.Acos((float)1/X) * 360 / (float)Math.PI : 0;
            angles.Y = Math.Abs(1/Y) > e ? (float)Math.Acos((float)1/Y) * 360 / (float)Math.PI : 0;
            angles.Z = Math.Abs(1/Z) > e ? (float)Math.Acos((float)1/Z) * 360 / (float)Math.PI : 0;

            return angles;
        }

        public void ToAngleAxis(out Vector3 axis, out float angle)
        {

            angle = (float)Math.Asin((float)W) * 360 / (float)Math.PI;

            float rad = (float)Math.PI * 2 * angle / 360.0f;
            float c = (float)Math.Cos(rad / 2);
            
            axis = new Vector3();

            axis.X = X / c;
            axis.Y = Y / c;
            axis.Z = Z / c;

        }

        public static Quaternion Nlerp(Quaternion q1, Quaternion q2, float t)
        {

            Quaternion r = Lerp(q1, q2, t);
            r.Normalize();

            return r;

        }

        public static float Dot(Quaternion q1, Quaternion q2)
        {
            return q1.X * q2.X + q1.Y * q2.Y + q1.Z * q2.Z + q1.W * q2.W;
        }

        public static Quaternion Slerp(Quaternion q1, Quaternion q2, float t)
        {
            
            float dot = Dot(q1, q2);

            float theta = (float)Math.Acos(dot);
            float sin = (float)Math.Sin(theta);

            float w1 = (float)Math.Sin((1-t) * theta) / sin;
            float w2 = (float)Math.Sin(t * theta) / sin;

            Quaternion r = new Quaternion();
            r.X = q1.X * w1 + q2.X * w2;
            r.Y = q1.Y * w1 + q2.Y * w2;
            r.Z = q1.Z * w1 + q2.Z * w2;
            r.W = q1.W * w1 + q2.W * w2;

            return r;

        }

        public static Quaternion Lerp(Quaternion q1, Quaternion q2, float t)
        {

            Quaternion r = new Quaternion();
            r.X = q2.X * t + q1.X * (1 - t);
            r.Y = q2.Y * t + q1.Y * (1 - t);
            r.Z = q2.Z * t + q1.Z * (1 - t);
            r.W = q2.W * t + q1.W * (1 - t);

            return r;

        }

        public override string ToString()
        {
            return String.Format("X:{0}, Y:{1}, Z:{2}, W:{3}", X, Y, Z, W);
        }

    }
}
