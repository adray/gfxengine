﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GraphicsEngineMath
{
    /// <summary>
    /// Represents a 3-dimensional box.
    /// </summary>
    public class Box
    {

        /// <summary>
        /// Maximum position.
        /// </summary>
        public Vector3 Max { get; set; }
        /// <summary>
        /// Minimum position.
        /// </summary>
        public Vector3 Min { get; set; }

        public Box()
        {
            Min = new Vector3();
            Max = new Vector3();
            Min.X = float.MaxValue;
            Min.Y = float.MaxValue;
            Min.Z = float.MaxValue;
            Max.X = float.MinValue;
            Max.Y = float.MinValue;
            Max.Z = float.MinValue;
        }

        /// <summary>
        /// Finds a box which encapsulates the minimum, maximum and the new vector.
        /// </summary>
        /// <param name="vector"></param>
        public void Union(Vector3 vector)
        {

            if (vector.X < Min.X)
                Min.X = vector.X;
            if (vector.Y < Min.Y)
                Min.Y = vector.Y;
            if (vector.Z < Min.Z)
                Min.Z = vector.Z;

            if (vector.X > Max.X)
                Max.X = vector.X;
            if (vector.Y > Max.Y)
                Max.Y = vector.Y;
            if (vector.Z > Max.Z)
                Max.Z = vector.Z;

        }

    }
}
