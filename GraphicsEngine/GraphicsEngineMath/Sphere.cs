﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GraphicsEngineMath
{
    /// <summary>
    /// Represents a 3-dimensional sphere.
    /// </summary>
    public class Sphere
    {
        public Vector3 Position { get; private set; }
        public float Radius { get; private set; }

        public Sphere(Vector3 position, float radius)
        {
            Position = position;
            Radius = radius;
        }
    }
}
