﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GraphicsEngineMath
{
    /// <summary>
    /// Represents a frustum shape.
    /// </summary>
    public class Frustum
    {

        public Vector3[] Corners { get; private set; }
        public Plane[] Planes { get; private set; }

        public Frustum()
        {
        }

        /// <summary>
        /// Creates a frustum in view space.
        /// </summary>
        /// <param name="fov">Field of view.</param>
        /// <param name="ratio">Ratio between width and height of planes.</param>
        /// <param name="nearplane">The distance to the near plane.</param>
        /// <param name="farplane">The distance to the far plane.</param>
        public Frustum(float fov, float ratio, float nearplane, float farplane)
        {

            Corners = new Vector3[8];

            //camera view direction
            Vector3 dir = new Vector3(0, 0, 1);

            float halfnearheight = (float)Math.Tan(fov/2) * nearplane;
            float halfnearwidth = halfnearheight * ratio;

            float halffarheight = (float)Math.Tan(fov/2) * farplane;
            float halffarwidth = halffarheight * ratio;

            Vector3[] centre = new Vector3[] { dir * nearplane, dir * farplane};

            Corners[0] = centre[1] + new Vector3(1, 0, 0) * halffarwidth  +  new Vector3(0, 1, 0) * halffarheight;
            Corners[1] = centre[1] - new Vector3(1, 0, 0) * halffarwidth + new Vector3(0, 1, 0) * halffarheight;
            Corners[2] = centre[1] + new Vector3(1, 0, 0) * halffarwidth - new Vector3(0, 1, 0) * halffarheight;
            Corners[3] = centre[1] - new Vector3(1, 0, 0) * halffarwidth - new Vector3(0, 1, 0) * halffarheight;

            Corners[4] = centre[0] + new Vector3(1, 0, 0) * halfnearwidth + new Vector3(0, 1, 0) * halfnearheight;
            Corners[5] = centre[0] - new Vector3(1, 0, 0) * halfnearwidth + new Vector3(0, 1, 0) * halfnearheight;
            Corners[6] = centre[0] + new Vector3(1, 0, 0) * halfnearwidth - new Vector3(0, 1, 0) * halfnearheight;
            Corners[7] = centre[0] - new Vector3(1, 0, 0) * halfnearwidth - new Vector3(0, 1, 0) * halfnearheight;

            Planes = new Plane[6];

            Planes[0] = new Plane(Corners[0], Corners[2] - Corners[0], Corners[1] - Corners[0]);    //far
            Planes[1] = new Plane(Corners[4], Corners[5] - Corners[4], Corners[6] - Corners[4]);    //near

            Planes[2] = new Plane(Corners[0], Corners[1] - Corners[0], Corners[4] - Corners[0]);    //top
            Planes[3] = new Plane(Corners[3], Corners[2] - Corners[3], Corners[7] - Corners[3]);    //bottom

            Planes[4] = new Plane(Corners[1], Corners[3] - Corners[1], Corners[5] - Corners[1]);    //left
            Planes[5] = new Plane(Corners[0], Corners[4] - Corners[0], Corners[2] - Corners[0]);    //right
        }

        public Frustum(Matrix4x4 worldviewproj)
        {

            Planes = new Plane[6];

            Planes[0]=new Plane(new Vector3(worldviewproj.M11+worldviewproj.M14, worldviewproj.M21+worldviewproj.M24,
                worldviewproj.M31+worldviewproj.M34), worldviewproj.M41+worldviewproj.M44);

        }

        public bool Test(Vector3 positionVS)
        {
            foreach (var plane in Planes)
                if (plane.Distance(positionVS) < 0)
                    return false;


            return true;
        }

        public bool Test(Vector3 positionVS, float radius)
        {
            
            foreach (var plane in Planes)
                if (plane != null && plane.Distance(positionVS) + radius < 0)
                    return false;


            return true;
        }
    }
}
