﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GraphicsEngineMath
{
    /// <summary>
    /// Represents a 3-dimensional vector.
    /// </summary>
    public class Vector3 : IEquatable<Vector3>
    {

        public float X { get; set; }
        public float Y { get; set; }
        public float Z { get; set; }

        public Vector3()
        {

        }

        public Vector3(float x, float y, float z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public static Vector3 operator /(Vector3 left, float s)
        {
            return new Vector3(left.X / s, left.Y / s, left.Z / s);
        }

        public static Vector3 operator *(Vector3 left, float s)
        {
            return new Vector3(left.X * s, left.Y * s, left.Z * s);
        }

        public static Vector3 operator *(Vector3 left, Vector3 right)
        {
            return new Vector3(left.X * right.X, left.Y * right.Y, left.Z * right.Z);
        }

        public static Vector3 operator +(Vector3 left, Vector3 right)
        {
            return new Vector3(left.X + right.X, left.Y + right.Y, left.Z + right.Z);
        }

        public static Vector3 operator -(Vector3 left, Vector3 right)
        {
            return new Vector3(left.X - right.X, left.Y - right.Y, left.Z - right.Z);
        }

        public static Vector3 operator -(Vector3 source)
        {
            return new Vector3(-source.X, -source.Y, -source.Z);
        }

        public static Vector3 Cross(Vector3 left, Vector3 right)
        {

            left.Normalize(); right.Normalize();

            return new Vector3(left.Y * right.Z - left.Z * right.Y,
                -(left.X * right.Z - left.Z * right.X),
                left.X * right.Y - left.Y * right.X);

        }

        public void Normalize()
        {

            float length = (float)Math.Sqrt(X * X + Y * Y + Z * Z);

            X /= length;
            Y /= length;
            Z /= length;

        }

        public static float Dot(Vector3 left, Vector3 right)
        {
            return left.X * right.X + left.Y * right.Y + left.Z * right.Z;
        }

        public float Length()
        {
            return (float)Math.Sqrt(X * X + Y * Y + Z * Z);
        }

        public override string ToString()
        {
            return String.Format("X: {0} Y: {1} Z: {2}", X, Y, Z);
        }

        public bool Equals(Vector3 other)
        {
            return X == other.X && Y == other.Y && Z == other.Z;
        }
    }
}
