﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GraphicsEngineMath
{
    /// <summary>
    /// Represents a 4x4 dimensional matrix.
    /// </summary>
    public class Matrix4x4
    {

        public float[] data;

        public Matrix4x4()
        {
            data = new float[16];
        }

        //[M11 M12 M13 M14]
        //[M21 M22 M23 M24]
        //[M31 M32 M33 M34]
        //[M41 M42 M43 M44]

        public float M11 { get { return data[0]; } set { data[0] = value; } }
        public float M12 { get { return data[1]; } set { data[1] = value; } }
        public float M13 { get { return data[2]; } set { data[2] = value; } }
        public float M14 { get { return data[3]; } set { data[3] = value; } }
        public float M21 { get { return data[4]; } set { data[4] = value; } }
        public float M22 { get { return data[5]; } set { data[5] = value; } }
        public float M23 { get { return data[6]; } set { data[6] = value; } }
        public float M24 { get { return data[7]; } set { data[7] = value; } }
        public float M31 { get { return data[8]; } set { data[8] = value; } }
        public float M32 { get { return data[9]; } set { data[9] = value; } }
        public float M33 { get { return data[10]; } set { data[10] = value; } }
        public float M34 { get { return data[11]; } set { data[11] = value; } }
        public float M41 { get { return data[12]; } set { data[12] = value; } }
        public float M42 { get { return data[13]; } set { data[13] = value; } }
        public float M43 { get { return data[14]; } set { data[14] = value; } }
        public float M44 { get { return data[15]; } set { data[15] = value; } }

        /// <summary>
        /// The identity matrix.
        /// </summary>
        /// <returns></returns>
        public static Matrix4x4 Identity()
        {

            Matrix4x4 matrix = new Matrix4x4();

            matrix.M11 = 1;
            matrix.M22 = 1;
            matrix.M33 = 1;
            matrix.M44 = 1;

            return matrix;

        }

        /// <summary>
        /// Finds the inverse of the source matrix.
        /// </summary>
        /// <param name="matrix"></param>
        /// <returns></returns>
        public static Matrix4x4 Inverse(Matrix4x4 matrix)
        {

            System.Diagnostics.Debug.Assert(matrix.M11 != 0);

            //use LU decomposition

            //AX = I

            //LU = A
            //Lyi = Ii
            //Uxi = yi

            Matrix4x4 l = Matrix4x4.Identity();
            Matrix4x4 u = new Matrix4x4();


            u.M11 = matrix.M11; //g
            u.M12 = matrix.M12; //h
            u.M13 = matrix.M13; //i
            u.M14 = matrix.M14; //j

            l.M21 = matrix.M21 / u.M11; //a
            l.M31 = matrix.M31 / u.M11; //b
            l.M41 = matrix.M41 / u.M11; //d

            u.M22 = matrix.M22 - l.M21 * u.M12; //k

            System.Diagnostics.Debug.Assert(u.M22 != 0);

            l.M32 = (matrix.M32 - l.M31 * u.M12) / u.M22; //c
            l.M42 = (matrix.M42 - l.M41 * u.M12) / u.M22; //e

            u.M23 = matrix.M23 - l.M21 * u.M13; //l
            u.M24 = matrix.M24 - l.M21 * u.M14; //m

            u.M33 = matrix.M33 - l.M31 * u.M13 - l.M32 * u.M23; //o
            u.M34 = matrix.M34 - l.M31 * u.M14 - l.M32 * u.M24; //p

            System.Diagnostics.Debug.Assert(matrix.M33 != 0);

            l.M43 = (matrix.M43 - l.M41 * u.M13 - l.M42 * u.M23) / u.M33; //f

            u.M44 = matrix.M44 - l.M41 * u.M14 - l.M42 * u.M24 - l.M43 * u.M34; //q -dj -em - fp


            //find inverse by back propagation

            Matrix4x4 y = new Matrix4x4();
            Matrix4x4 I = Matrix4x4.Identity(); 

            //solve Ly = b

            for (int i = 0; i < 4; i++)
            {

                y.data[i] = I.data[i*4]; //xi
                y.data[i+4] = I.data[i*4+1] - l.M21 * y.data[i]; //yi
                y.data[i+8] = I.data[i*4+2] - l.M31 * y.data[i] - l.M32 * y.data[i+4];    //zi
                y.data[i+12] = I.data[i*4+3] - l.M41 * y.data[i] - l.M42 * y.data[i+4] - l.M43 * y.data[i+8]; //wi

            }

#if DEBUG

            Matrix4x4 res = l * y;

            for (int i = 0; i < 16; i++)
            {
                System.Diagnostics.Debug.Assert(Math.Abs(res.data[i] - I.data[i]) < 0.01f, "Ly!=b\n" + res.ToString());
            }

#endif

            //solve Ux = y

            y.Transpose();
            Matrix4x4 inverse = new Matrix4x4();

            for (int i = 0; i < 4; i++)
            {

                inverse.data[i + 12] = y.data[i * 4 + 3] / u.M44;
                inverse.data[i + 8] = (y.data[i * 4 + 2] - u.M34 * inverse.data[i+12]) / u.M33;
                inverse.data[i + 4] = (y.data[i * 4 + 1] - u.M23 * inverse.data[i + 8] - u.M24 * inverse.data[i + 12]) / u.M22;
                inverse.data[i] = (y.data[i * 4] - u.M12 * inverse.data[i + 4]
                    - u.M13 * inverse.data[i + 8] - u.M14 * inverse.data[i + 12]) / u.M11;

            }

#if DEBUG

            y.Transpose();
            res = u * inverse;

            for (int i = 0; i < 16; i++)
            {
                System.Diagnostics.Debug.Assert(Math.Abs(res.data[i] - y.data[i]) < 0.01f, "Ux!=y\n got:" +
                    res.ToString() + "\n wanted:" + y.ToString());
            }

#endif

            return inverse;


        }

        /// <summary>
        /// Transposes the matrix in place.
        /// </summary>
        public void Transpose()
        {

            for (int i = 0; i < 4; i++)
                for (int j = 0; j < 4; j++)
                {
                    if (i > j)
                    {
                        float temp = data[i + j * 4];
                        data[i + j * 4] = data[j + i * 4];
                        data[j + i * 4] = temp;
                    }
                }
            

        }

        /// <summary>
        /// Finds the transpose of the source matrix.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static Matrix4x4 Transpose(Matrix4x4 source)
        {
            Matrix4x4 transpose = new Matrix4x4();

            for (int i = 0; i < 4; i++)
                for (int j = 0; j < 4; j++)
                {
                     transpose.data[i + j * 4] = source.data[j + i * 4];
                }

            return transpose;
        }

        /// <summary>
        /// Multiplies two matrices.
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static Matrix4x4  operator * (Matrix4x4 left, Matrix4x4 right)
        {

            Matrix4x4 result = new Matrix4x4();

            result.M11 = left.M11 * right.M11 + left.M12 * right.M21 + left.M13 * right.M31 + left.M14 * right.M41;
            result.M12 = left.M11 * right.M12 + left.M12 * right.M22 + left.M13 * right.M32 + left.M14 * right.M42;
            result.M13 = left.M11 * right.M13 + left.M12 * right.M23 + left.M13 * right.M33 + left.M14 * right.M43;
            result.M14 = left.M11 * right.M14 + left.M12 * right.M24 + left.M13 * right.M34 + left.M14 * right.M44;

            result.M21 = left.M21 * right.M11 + left.M22 * right.M21 + left.M23 * right.M31 + left.M24 * right.M41;
            result.M22 = left.M21 * right.M12 + left.M22 * right.M22 + left.M23 * right.M32 + left.M24 * right.M42;
            result.M23 = left.M21 * right.M13 + left.M22 * right.M23 + left.M23 * right.M33 + left.M24 * right.M43;
            result.M24 = left.M21 * right.M14 + left.M22 * right.M24 + left.M23 * right.M34 + left.M24 * right.M44;

            result.M31 = left.M31 * right.M11 + left.M32 * right.M21 + left.M33 * right.M31 + left.M34 * right.M41;
            result.M32 = left.M31 * right.M12 + left.M32 * right.M22 + left.M33 * right.M32 + left.M34 * right.M42;
            result.M33 = left.M31 * right.M13 + left.M32 * right.M23 + left.M33 * right.M33 + left.M34 * right.M43;
            result.M34 = left.M31 * right.M14 + left.M32 * right.M24 + left.M33 * right.M34 + left.M34 * right.M44;

            result.M41 = left.M41 * right.M11 + left.M42 * right.M21 + left.M43 * right.M31 + left.M44 * right.M41;
            result.M42 = left.M41 * right.M12 + left.M42 * right.M22 + left.M43 * right.M32 + left.M44 * right.M42;
            result.M43 = left.M41 * right.M13 + left.M42 * right.M23 + left.M43 * right.M33 + left.M44 * right.M43;
            result.M44 = left.M41 * right.M14 + left.M42 * right.M24 + left.M43 * right.M34 + left.M44 * right.M44;

            return result;
        }

        /// <summary>
        /// Calculates a matrix describing a rotation about the Z-axis. 
        /// </summary>
        /// <param name="theta">Angle in radians.</param>
        /// <returns></returns>
        public static Matrix4x4 RotationZ(float theta)
        {

            Matrix4x4 matrix = Matrix4x4.Identity();

            matrix.M11 = (float)Math.Cos(theta);
            matrix.M12 = (float)-Math.Sin(theta);
            matrix.M21 = (float)Math.Sin(theta);
            matrix.M22 = (float)Math.Cos(theta);

            return matrix;

        }

        /// <summary>
        /// Calculates a matrix describing a rotation about the Y-axis. 
        /// </summary>
        /// <param name="theta">Angle in radians.</param>
        /// <returns></returns>
        public static Matrix4x4 RotationY(float theta)
        {

            Matrix4x4 matrix = Matrix4x4.Identity();

            matrix.M11 = (float)Math.Cos(theta);
            matrix.M13 = (float)Math.Sin(theta);
            matrix.M31 = (float)-Math.Sin(theta);
            matrix.M33 = (float)Math.Cos(theta);

            return matrix;

        }

        /// <summary>
        /// Calculates a matrix describing a rotation about the X-axis. 
        /// </summary>
        /// <param name="theta">Angle in radians.</param>
        /// <returns></returns>
        public static Matrix4x4 RotationX(float theta)
        {

            Matrix4x4 matrix = Matrix4x4.Identity();

            matrix.M22 = (float)Math.Cos(theta);
            matrix.M23 = (float)-Math.Sin(theta);
            matrix.M32 = (float)Math.Sin(theta);
            matrix.M33 = (float)Math.Cos(theta);

            return matrix;

        }

        public override string ToString()
        {


            return String.Format("\n[{0} {1} {2} {3}]\n", M11, M12, M13, M14) +
                String.Format("[{0} {1} {2} {3}]\n", M21, M22, M23, M24) +
                String.Format("[{0} {1} {2} {3}]\n", M31, M32, M33, M34) +
                String.Format("[{0} {1} {2} {3}]\n", M41, M42, M43, M44);
        }


        public static Matrix4x4 Translate(float x, float y, float z)
        {

            Matrix4x4 transform = Matrix4x4.Identity();

            transform.M41 = x;
            transform.M42 = y;
            transform.M43 = z;

            return transform;

        }

        public static Matrix4x4 Scale(float x, float y, float z)
        {

            Matrix4x4 matrix = Matrix4x4.Identity();

            matrix.M11 = x;
            matrix.M22 = y;
            matrix.M33 = z;

            return matrix;

        }

        public static Matrix4x4 Scale(float s)
        {

            Matrix4x4 matrix = Matrix4x4.Identity();

            matrix.M11 = s;
            matrix.M22 = s;
            matrix.M33 = s;

            return matrix;

        }

        public static Vector4 Transform(Matrix4x4 s, Vector4 vector)
        {

            Matrix4x4 source = Matrix4x4.Transpose(s);

            return new Vector4(source.M11 * vector.X + source.M12 * vector.Y + source.M13 * vector.Z + source.M14 * vector.W,
                source.M21 * vector.X + source.M22 * vector.Y + source.M23 * vector.Z + source.M24 * vector.W,
                source.M31 * vector.X + source.M32 * vector.Y + source.M33 * vector.Z + source.M34 * vector.W,
                source.M41 * vector.X + source.M42 * vector.Y + source.M43 * vector.Z + source.M44 * vector.W);

        }
    }
}
