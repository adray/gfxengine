﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GraphicsEngineBase
{
    public struct RenderPermutation
    {
        public int flag;
        public string define;

        public RenderPermutation(int flag, string define)
        {
            this.flag = flag;
            this.define = define;
        }
    }

    public delegate IDisposable Compile(string file, string method, params string[] defines);

    public class Shader : IDisposable
    {
        private System.Collections.Hashtable Permutation { get; set; }
        private Compile compile;
        private string file;
        private string method;

        public Shader(Compile compile, string file, string method)
        {
            Permutation = new System.Collections.Hashtable();

            this.compile = compile;
            this.file = file;
            this.method = method;
        }

        public IDisposable this[int flags]
        {
            get
            {
                return (IDisposable)Permutation[flags];
            }
            set
            {
                Permutation[flags] = value;
            }
        }

        /// <summary>
        /// Compiles all the combinations of a list of shader permutations.
        /// </summary>
        /// <param name="flags"></param>
        public void Add(List<RenderPermutation> flags)
        {
            CompileShaderPermutations(0, flags);
        }

        /// <summary>
        /// Compiles a shader permutation.
        /// </summary>
        /// <param name="flags"></param>
        /// <param name="defines"></param>
        public void Add(int flags, params String[] defines)
        {
            System.Diagnostics.Debug.Assert(!Permutation.ContainsKey(flags));

            Permutation.Add(flags, compile.Invoke(file, method, defines));
        }

        /// <summary>
        /// Compiles the list of shaders.
        /// </summary>
        /// <param name="flags"></param>
        public void AddSingle(List<RenderPermutation> flags)
        {

            foreach (RenderPermutation p in flags)
                Add(p.flag, p.define);

        }

        /// <summary>
        /// Compiles all the combinations.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="permutations"></param>
        private void CompileShaderPermutations(int x, List<RenderPermutation> permutations)
        {

            if (x < permutations.Count)
            {

                //flip bit
                List<RenderPermutation> zero = new List<RenderPermutation>(permutations);
                zero.RemoveAt(x);

                CompileShaderPermutations(x, zero);

                List<RenderPermutation> one = new List<RenderPermutation>(permutations);

                CompileShaderPermutations(x + 1, one);
            }
            else
            {

                int flag = (int)RenderFlags.None;
                string[] defines = new string[permutations.Count];
                int count = 0;

                foreach (RenderPermutation p in permutations)
                {
                    flag |= p.flag;
                    defines[count++] = p.define;
                }

                Add(flag, defines);

            }


        }

        /// <summary>
        /// Disposes of the shaders.
        /// </summary>
        public void Dispose()
        {
            foreach (IDisposable dispose in Permutation.Values)
                dispose.Dispose();
        }

    }
}
