﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GraphicsEngineBase
{
    public class DeviceCouldNotBeCreatedException : Exception
    {

        public DeviceCouldNotBeCreatedException()
            : base("Device failed to be created. Hardware not supported.")
        {

        }

    }
}
