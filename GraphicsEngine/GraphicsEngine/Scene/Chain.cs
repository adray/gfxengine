﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GraphicsEngineBase
{
    /// <summary>
    /// Represents a chain of effects, which will be applied as a post-process effect in the order specified.
    /// </summary>
    public class Chain
    {

        public List<Effect> Effects { get; private set; }

        public Chain()
        {
            Effects = new List<Effect>();
        }

        public void AppendEffect(Effect effect)
        {
            Effects.Add(effect);
        }

    }
}
