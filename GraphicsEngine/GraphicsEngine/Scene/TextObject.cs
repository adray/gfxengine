﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GraphicsEngineMath;

namespace GraphicsEngineBase
{
    public enum TextRenderingOptions
    {
        None,
        Centered
    }

    /// <summary>
    /// Represents renderable text.
    /// </summary>
    public class TextObject
    {

        public Vector2 Scale { get; set; }
        public Vector2 Position { get; set; }
        public String Text { get; set; }
        public Vector4 Colour { get; set; }
        public TextRenderingOptions RenderOptions { get; set; }

        public TextObject()
        {
            Scale = new Vector2(1,1);
            Text = "";
            Position = new Vector2();
            Colour = new Vector4();
            RenderOptions = TextRenderingOptions.None;
        }

    }
}
