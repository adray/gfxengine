﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GraphicsEngineMath;

namespace GraphicsEngineBase
{
    public class TerrainTriangle
    {
        public Vector3[] Positions { get; set; }

        public TerrainTriangle()
        {
            Positions = new Vector3[3];
        }

        public TerrainTriangle(Vector3 v1, Vector3 v2, Vector3 v3)
        {
            Positions = new Vector3[] { v1, v2, v3 };
        }
        
    }

    /// <summary>
    /// Represents a virtual landscape.
    /// </summary>
    public sealed class TerrainObject
    {

        public Material Material { get; set; }
        public Mesh Mesh { get; private set; }
        public Texture Heightmap { get; private set; }
        public GraphicsEngineMath.Vector3 Position { get; set; }
        private Vector3 scale;
        public GraphicsEngineMath.Vector3 Scale { get { return scale; } set { scale = value; CalculateNormals(); } }
        public RenderFlags Flags { get; set; }
        private System.Drawing.Bitmap Image { get; set; }
        private Vector3[] v;
        public List<TerrainTriangle> HeightMesh { get; private set; }
        public Texture BlendWeight { get; private set; }   //default weightmap

        internal TerrainObject(Mesh mesh, Texture heightmap, Texture weightmap, System.Drawing.Bitmap img, Vector3[] v)
        {
            this.scale = new Vector3(1,1,1);
            this.Mesh = mesh;
            Material = new Material();
            Position = new GraphicsEngineMath.Vector3();
            Flags = RenderFlags.None;
            Heightmap = heightmap;
            Image = img;
            this.v = v;
            HeightMesh = new List<TerrainTriangle>();
            BlendWeight = weightmap;

            CalculateNormals();

        }

        private void CalculateNormals()
        {
            Mesh.Clear();
            HeightMesh.Clear();

            Vector3[] normals = CalculateNormals(v, Image.Width, Image.Height);

            for (int i = 0; i < Image.Width - 1; i++)
                for (int j = 0; j < Image.Height - 1; j++)
                {

                    int[] index = new int[]{i + Image.Width * j, (i + 1) + Image.Width * (j + 1),
                        i + 1 + Image.Width * j, i + Image.Width * (j + 1) };

                    Vertex v1 = new Vertex(5).SetData(0, v[index[0]].X, v[index[0]].Z).SetData(2,
                        normals[index[0]]);
                    Vertex v2 = new Vertex(5).SetData(0, v[index[1]].X, v[index[1]].Z).SetData(2,
                        normals[index[1]]);
                    Vertex v3 = new Vertex(5).SetData(0, v[index[2]].X, v[index[2]].Z).SetData(2,
                        normals[index[2]]);
                    Vertex v4 = new Vertex(5).SetData(0, v[index[3]].X, v[index[3]].Z).SetData(2,
                        normals[index[3]]);

                    Mesh.AddTriangle(new Triangle(v1, v2, v3));

                    Mesh.AddTriangle(new Triangle(v2, v1, v4));

                    HeightMesh.Add(new TerrainTriangle(v[index[0]], v[index[1]], v[index[2]]));
                    HeightMesh.Add(new TerrainTriangle(v[index[1]], v[index[0]], v[index[3]]));

                }

        }

        private Vector3[] CalculateNormals(Vector3[] vertices, int width, int height)
        {
            Vector3[] normals = new Vector3[width * height];

            for (int i = 0; i < width * height; i++)
                normals[i] = new Vector3(0, 1, 0);

            for (int i = 1; i < width - 1; i++)
                for (int j = 1; j < height - 1; j++)
                {
                    Vector3 vert = vertices[i + j * width] * Scale;

                    Vector3 a = vertices[(i + 1) + j * width] * Scale - vert;
                    Vector3 b = vertices[i + (j - 1) * width] * Scale - vert;
                    Vector3 c = vertices[(i - 1) + j * width] * Scale - vert;
                    Vector3 d = vertices[i + (j + 1) * width] * Scale - vert;
                    a.Normalize(); b.Normalize(); c.Normalize(); d.Normalize();

                    Vector3 v1 = Vector3.Cross(d, a);
                    Vector3 v2 = Vector3.Cross(c, d);
                    Vector3 v3 = Vector3.Cross(a, b);
                    Vector3 v4 = Vector3.Cross(b, c);
                    v1.Normalize(); v2.Normalize(); v3.Normalize(); v4.Normalize();

                    Vector3 normal = v1 + v2 + v3 + v4;
                    normal.Normalize();
                    normals[i + j * width] = normal;

                    System.Diagnostics.Debug.Assert(Vector3.Dot(normals[i + j * width], new Vector3(0, 1, 0)) > 0, normals[i + j * width].ToString());

                }

            //for (int i = 0; i < width * height; i++)
            //    System.Diagnostics.Debug.Assert(normals[i].Equals(new Vector3(0, 1, 0)), normals[i].ToString());

            return normals;
        }

    }
}
