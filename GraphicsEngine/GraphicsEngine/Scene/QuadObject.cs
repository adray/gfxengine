﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GraphicsEngineMath;

namespace GraphicsEngineBase
{
    /// <summary>
    /// Represents a 2D rectangular object.
    /// </summary>
    public class QuadObject
    {

        public Vector2 Position { get; set; }
        public Vector2 Scale { get; set; }
        public float Width { get; set; }
        public float Height { get; set; }
        public Texture Texture { get; set; }

        public QuadObject()
        {
            Position = new Vector2();
            Scale = new Vector2(1, 1);
            Texture = null;
            Width = 100;
            Height = 100;
        }

    }
}
