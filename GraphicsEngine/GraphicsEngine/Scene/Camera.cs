﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GraphicsEngineMath;

namespace GraphicsEngineBase
{
    /// <summary>
    /// Represents a virtual camera, where the scene is rendered from.
    /// </summary>
    public class Camera
    {

        public Vector3 Position { get; set; }
        public Vector3 LookAt { get; set; }
        public float NearPlane { get; set; }
        public float FarPlane { get; set; }

        public Camera()
        {

            Position = new Vector3();
            LookAt = Position + new Vector3(0, 0, 1);
            NearPlane = 1;
            FarPlane = 100;

        }

        public Frustum CalculateFrustumVS()
        {

            return new Frustum((float)Math.PI / 4, 4.0f / 3.0f, NearPlane, FarPlane);

        }

    }
}
