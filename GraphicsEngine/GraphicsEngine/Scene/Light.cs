﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GraphicsEngineMath;

namespace GraphicsEngineBase
{
    public enum LightType
    {
        Directional,
        Point,
        Spot
    }

    /// <summary>
    /// Represents a virtual light in the scene.
    /// </summary>
    public class Light
    {

        public Vector3 Position {get; set;}
        public Vector3 Direction { get; set; }
        public Vector4 Color { get; set; }
        public LightType LightType { get; set; }
        public Vector3 Radius { get; set; }
        public bool IsShadowCaster { get; set; }

        public Light()
        {
            Position = new Vector3();
            Direction = new Vector3();
            Color = new Vector4();
            Radius = new Vector3(1, 1, 1);
            IsShadowCaster = false;
        }
        
    }
}
