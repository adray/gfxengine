﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GraphicsEngineBase
{
    public enum ResourceFlags
    {
        BumpMap = 0,
        DiffuseMap = 1,
        Blend1,
        Blend2,
        BlendWeight,
    }

    /// <summary>
    /// Represents a collection of constants which describe the material of a renderable object.
    /// </summary>
    public class Material
    {

        public Texture BlendWeightTexture { get; set; }
        public Texture[] BlendTextures { get; set; }
        public Texture DiffuseTexture { get; set; }
        public Texture BumpTexture { get; set; }
        public GraphicsEngineMath.Vector3 DiffuseConstant { get; set; }
        public float Specular { get; set; }
        public float Shininess { get; set; }
        
        public Material()
        {
            SetDefaults();
        }

        public Material(Texture texture, ResourceFlags flag)
        {

            SetDefaults();
            AssignResource(texture, flag);

        }

        public Material(Texture[] textures, ResourceFlags[] flags)
        {

            SetDefaults();
            for (int i = 0; i < textures.Length; i++)
            {

                AssignResource(textures[i], flags[i]);

            }
        }

        private void SetDefaults()
        {
            BlendTextures = new Texture[8];
            DiffuseConstant = new GraphicsEngineMath.Vector3(0.5f, 0.5f, 0.5f);
            Specular = 0.5f;
            Shininess = 10;
        }

        private void AssignResource(Texture texture, ResourceFlags flag)
        {

                switch (flag)
                {
                    case ResourceFlags.BumpMap:
                        this.BumpTexture = texture;
                        break;
                    case ResourceFlags.DiffuseMap:
                        this.DiffuseTexture = texture;
                        break;
                    case ResourceFlags.Blend1:
                        this.BlendTextures[0] = texture;
                        break;
                    case ResourceFlags.Blend2:
                        this.BlendTextures[1] = texture;
                        break;
                    case ResourceFlags.BlendWeight:
                        this.BlendWeightTexture = texture;
                        break;
                    default:
                        throw new Exception("Invaild resource.");

                }

        }

    }
}
