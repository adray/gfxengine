﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GraphicsEngineBase
{
    [Flags]
    public enum RenderFlags
    {
        None = 0,
        DiffuseMap = 1,
        BumpMap = 2,
        Reflection = 4,
    }

    public enum RenderVertexFlags
    {
        None = 0,
        Instance = 1,
        Terrain = 2,
        Reflection = 3
    }
    
    [Flags]
    public enum RenderFilter
    {
        None=0,
        No_Render=1,
        No_Shadow=2,
        No_Reflection=4
    }

    public sealed class ObjectSubset
    {

        public String ID { get; set; }
        public Material Material { get; set; }
        public Mesh Mesh { get; set; }
        public GraphicsEngineMath.Matrix4x4 Transform { get; set; }
        public RenderFlags RenderFlags { get; set; }

        public ObjectSubset()
        {
            ID = "";
            Material = new Material();
            Transform = GraphicsEngineMath.Matrix4x4.Identity();
            RenderFlags = GraphicsEngineBase.RenderFlags.None;
        }

    }

    /// <summary>
    /// Constructs objects from specified mesh. The objects can be both instanced and ordinary objects.
    /// </summary>
    public class ObjectFactory
    {

        public List<ObjectSubset> Subsets { get; private set; }
        public List<ObjectInstanceSubset> Instance { get; private set; }
        private Device Device { get; set; }

        public ObjectFactory(Device device, List<ObjectSubset> subset, List<ObjectInstanceSubset> instance)
        {
            Subsets=subset;
            Instance = instance;
            Device = device;
        }

        public Object CreateObject()
        {

            return new Object(Subsets, RenderFlags.None);

        }

        public ObjectInstance CreateInstanceObject()
        {

            return new ObjectInstance(Instance, RenderFlags.None, Device.CreateInstance());

        }

    }

    /// <summary>
    /// Represents a virtual object.
    /// </summary>
    public class Object
    {

        public List<ObjectSubset> Subsets { get; set; }

        public GraphicsEngineMath.Matrix4x4 Transform { get; set; }
        public GraphicsEngineMath.Matrix4x4 RootTransform { get; set; }
        public RenderFlags Flags { get; set; }
        public RenderFilter Filter { get; set; }

        public Object(Mesh mesh, Material material, RenderFlags flags)
        {
            
            Transform = GraphicsEngineMath.Matrix4x4.Identity();
            RootTransform = GraphicsEngineMath.Matrix4x4.Identity();
            this.Flags = flags;

            Subsets = new List<ObjectSubset>();
            Subsets.Add(new ObjectSubset() { Mesh = mesh, Material = new Material() {
                BlendTextures = material.BlendTextures,
                BlendWeightTexture = material.BlendWeightTexture,
                BumpTexture = material.BumpTexture,
                DiffuseConstant = material.DiffuseConstant,
                DiffuseTexture = material.DiffuseTexture,
                Shininess = material.Shininess,
                Specular = material.Specular,
            } });

            Filter = RenderFilter.None;
        }

        public Object(List<ObjectSubset> subset, RenderFlags flags)
        {

            Transform = GraphicsEngineMath.Matrix4x4.Identity();
            RootTransform = GraphicsEngineMath.Matrix4x4.Identity();

            Subsets = new List<ObjectSubset>();

            foreach (var sub in subset)
                Subsets.Add(new ObjectSubset()
                {
                    Mesh = sub.Mesh,
                    Material = new Material()
                    {
                        BlendTextures = sub.Material.BlendTextures,
                        BlendWeightTexture = sub.Material.BlendWeightTexture,
                        BumpTexture = sub.Material.BumpTexture,
                        DiffuseConstant = sub.Material.DiffuseConstant,
                        DiffuseTexture = sub.Material.DiffuseTexture,
                        Shininess = sub.Material.Shininess,
                        Specular = sub.Material.Specular,
                    }
                });

            Flags = flags;
            
            Filter = RenderFilter.None;

        }

        public Object()
        {
            Subsets = new List<ObjectSubset>();
            Transform = GraphicsEngineMath.Matrix4x4.Identity();
            RootTransform = GraphicsEngineMath.Matrix4x4.Identity();

            Flags = RenderFlags.None;
            Filter = RenderFilter.None;
        }

    }

    public sealed class ObjectInstanceSubset
    {

        public Material Material { get; set; }
        public Mesh Mesh { get; set; }

        public ObjectInstanceSubset()
        {
            Material = new Material();
        }

    }

    /// <summary>
    /// Represents a collection of virtual objects.
    /// </summary>
    public class ObjectInstance
    {

        public List<ObjectInstanceSubset> Subsets { get; private set; }
        public RenderFlags RenderFlags { get; set; }
        public RenderFilter RenderFilter { get; set; }
        public MeshInstance Instance { get; private set; }

        public ObjectInstance(List<ObjectInstanceSubset> subset, RenderFlags flags, MeshInstance instance)
        {
            this.RenderFlags = flags;

            this.Subsets = new List<ObjectInstanceSubset>();
            foreach (var x in subset)
            {

                var obj = new ObjectInstanceSubset();
                obj.Material.BumpTexture = x.Material.BumpTexture;
                obj.Material.DiffuseConstant = x.Material.DiffuseConstant;
                obj.Material.DiffuseTexture = x.Material.DiffuseTexture;
                obj.Material.Shininess = x.Material.Shininess;
                obj.Material.Specular = x.Material.Specular;
                obj.Mesh = x.Mesh;
                this.Subsets.Add(obj);

            }

            //this.Subsets = subset;
            
            this.RenderFilter = GraphicsEngineBase.RenderFilter.None;
            this.Instance = instance;
        }

    }


    public class AnimatedMesh
    {

        public List<ObjectSubset> Subsets { get; set; }
        public RenderFlags Flags { get; set; }
        public RenderFilter Filter { get; set; }
        public List<GraphicsEngineMath.Matrix4x4> Matrices { get; set; }

        public AnimatedMesh( List<ObjectSubset> subset )
        {

            this.Subsets = new List<ObjectSubset>();
            foreach (var x in subset)
            {

                var obj = new ObjectSubset();
                obj.Material.BumpTexture = x.Material.BumpTexture;
                obj.Material.DiffuseConstant = x.Material.DiffuseConstant;
                obj.Material.DiffuseTexture = x.Material.DiffuseTexture;
                obj.Material.Shininess = x.Material.Shininess;
                obj.Material.Specular = x.Material.Specular;
                obj.Mesh = x.Mesh;
                this.Subsets.Add(obj);

            }

        }

    }

}
