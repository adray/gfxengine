﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GraphicsEngineBase
{
    /// <summary>
    /// Represents a texture.
    /// </summary>
    public interface Texture : IDisposable, IWatchable
    {

        byte[] Sample();

    }
}
