﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GraphicsEngineBase
{

    public enum PassInput
    {
        Pass,
        Normalmap,
        Diffusemap,
        Depthmap,
        Final,
        Lightmap
    }

    public enum PassConstants
    {
        View,
        Proj,
        CameraPos
    }

    public enum PassFunction
    {
        Downsize,
        Upsize,
        HorizontalBlur,
        VerticalBlur,
        Bright,
        Inversion,
        Combine,
        Tonemap,
        Luminance,
        Luminance_Downsize,
        Fog,
    }

    /// <summary>
    /// Represents a sequence of passes, which make up an effect.
    /// </summary>
    public sealed class Effect
    {

        public Queue<Pass> passes { get; private set; }
        public String Name { get; private set; }

        public Effect()
        {
            passes = new Queue<Pass>();
        }

        public static List<Effect> FromFile(Device device, String file)
        {

            List<Effect> effects = new List<Effect>();


            System.Xml.XmlTextReader parser=null;
            

            try
            {

                parser = new System.Xml.XmlTextReader(file);

            }
            catch (Exception ex)
            {

            }

            while (!parser.EOF)
            {

                parser.Read();

                if (parser.NodeType == System.Xml.XmlNodeType.Element)
                {


                    if (parser.Name == "EFFECT")
                    {

                        effects.Add(ParseEffect(parser, device));
                        

                    }


                }


            }

            parser.Close();

            return effects;

        }

        private static Effect ParseEffect(System.Xml.XmlTextReader parser, Device device)
        {

            Effect effect = new Effect();
            effect.Name = parser.GetAttribute("NAME");

            

            while (!(parser.NodeType == System.Xml.XmlNodeType.EndElement && parser.Name == "EFFECT"))
            {

                parser.Read();

                if (parser.Name == "PASS")
                {

                    effect.passes.Enqueue(ParsePass(parser, device));

                }

            }

            return effect;
        }

        private static Pass ParsePass(System.Xml.XmlTextReader parser, Device device)
        {

            PassFunction function=PassFunction.Bright;

            String attribute = parser.GetAttribute("PASSFUNCTION");

            try
            {
                bool found = false;
                foreach (string s in Enum.GetNames(typeof(PassFunction)))
                    if (s.ToLower() == attribute.ToLower())
                    {
                        function = (PassFunction)Enum.Parse(typeof(PassFunction), attribute, true);
                        found = true;
                        break;
                    }
                if (!found)
                    throw new Exception();
            }
            catch (Exception ex)
            //if (!Enum.TryParse<PassFunction>(attribute, true, out function))
            {
                switch (attribute)
                {
                    case "BLURH":
                        function = PassFunction.HorizontalBlur;
                        break;
                    case "BLURV":
                        function = PassFunction.VerticalBlur;
                        break;
                    default:
                        throw new Exception();
                }
            }

            Pass p = new Pass(function);


            List<Pass> passes = new List<Pass>();

            while (!(parser.NodeType == System.Xml.XmlNodeType.EndElement && parser.Name == "PASS"))
            {

                parser.Read();

                if (parser.NodeType == System.Xml.XmlNodeType.Element)
                {

                    if (parser.Name == "PASSINPUT")
                    {
                        PassInput input;
                        int slot;

                        input = (PassInput)Enum.Parse(typeof(PassInput), parser.GetAttribute("INPUT"), true);
                        //if (!Enum.TryParse<PassInput>(parser.GetAttribute("INPUT"), true, out input))
                        //    throw new Exception();

                        if (!Int32.TryParse(parser.GetAttribute("SLOT"), out slot))
                            throw new Exception();


                        p.SetPassInput(slot, input);
                    }
                }

            }

            parser.Read();

            return p;

        }

    }

    /// <summary>
    /// Represents a single full-screen post process pass.
    /// A pass takes some input (usually the previous render), applies the pass to an output texture.
    /// </summary>
    public sealed class Pass
    {

        //inputs -> render targets -> pass/normals/diffuse/depth/final
        public PassInput[] inputs { get; private set; }

        //output -> a render target

        //constants -> buffer
        float[] data;
        PassConstants[] constants;

        //function -> shader
        public PassFunction function { get; private set; }

        public Pass(PassFunction function, params PassInput[] passinputs)
        {
            this.function = function;

            switch (function)
            {
                case PassFunction.Bright:
                    inputs = new PassInput[1];
                    break;
                case PassFunction.Combine:
                    inputs = new PassInput[2];
                    break;
                case PassFunction.Downsize:
                    inputs = new PassInput[1];
                    break;
                case PassFunction.HorizontalBlur:
                    inputs = new PassInput[1];
                    break;
                case PassFunction.Inversion:
                    inputs = new PassInput[1];
                    break;
                case PassFunction.Upsize:
                    inputs = new PassInput[1];
                    break;
                case PassFunction.VerticalBlur:
                    inputs = new PassInput[1];
                    break;
                case PassFunction.Tonemap:
                    inputs = new PassInput[2];
                    break;
                case PassFunction.Luminance:
                    inputs = new PassInput[1];
                    break;
                case PassFunction.Luminance_Downsize:
                    inputs = new PassInput[1];
                    break;
                case PassFunction.Fog:
                    inputs = new PassInput[2];
                    break;
                default:
                    throw new Exception("Pass type not found.");
            }

            int i = 0;
            foreach (var input in passinputs)
                SetPassInput(i++, input);
        }

        public void SetPassInput(int index, PassInput input)
        {
            System.Diagnostics.Debug.Assert(index < inputs.Length);

            inputs[index] = input;
        }

    }
}
