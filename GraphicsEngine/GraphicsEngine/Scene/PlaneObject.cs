﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GraphicsEngineBase
{
    /// <summary>
    /// Represents a cross-section of a plane.
    /// </summary>
    public class PlaneObject
    {

        public Texture DiffuseMap { get; set; }
        public GraphicsEngineMath.Plane Plane { get; set; }
        public RenderFlags Flags = RenderFlags.None;
        public bool IsReflected { get; set; }

        public GraphicsEngineMath.Vector3 Centre = 
            new GraphicsEngineMath.Vector3(0,0,0);
        public float Width { get; set; }
        public float Height { get; set; }

    }
}
