﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GraphicsEngineMath;

namespace GraphicsEngineBase
{
    /// <summary>
    /// Represents a virtual particle.
    /// </summary>
    public class Particle
    {

        public Vector3 Velocity { get; set; }
        public Vector3 Position { get; set; }
        public Vector2 Scale { get; set; }
        public Texture DiffuseTexture { get; set; }
        public float Opacity { get; set; }

        public Particle(Texture diffusetexture)
        {
            Scale = new Vector2(1, 1);
            Position = new Vector3();
            DiffuseTexture = diffusetexture;
            Opacity = 1;
            Velocity = new Vector3();
        }

    }
}
