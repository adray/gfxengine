﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GraphicsEngineMath;

namespace GraphicsEngineBase
{
    public enum ShadowRenderStrategy
    {
        FitToScene,
        FitToCamera,
    }

    /// <summary>
    /// Represents the virtual scene.
    /// </summary>
    public class Scene
    {

        public List<Object> Objects { get; private set;}
        public Camera Camera { get; set; }
        public Vector4 Ambient { get; set; }
        public List<Light> Lights { get; private set; }
        public List<Particle> Particles { get; private set; }
        public List<ObjectInstance> Instances { get; private set; }
        public List<QuadObject> Quads { get; private set; }
        public List<TextObject> Text { get; private set; }
        public List<TerrainObject> Terrain { get; private set; }
        public List<PlaneObject> Planes { get; private set; }
        /// <summary>
        /// Enables/Disables the renders using back-face culling.
        /// </summary>
        public bool BackFaceCulling { get; set; }
        /// <summary>
        /// Enables/Disables wireframe rendering.
        /// </summary>
        public bool WireFrameRendering { get; set; }
        /// <summary>
        /// Enables/Disables vertical sync.
        /// </summary>
        public bool VerticalSync { get; set; }
        /// <summary>
        /// Enables/Disables rendering the scene with HDR.
        /// </summary>
        public bool HDRLighting { get; set; }
        /// <summary>
        /// The shadow render strategy.
        /// </summary>
        public ShadowRenderStrategy ShadowStrategy { get; set; }
        /// <summary>
        /// Enables/Disables rendering objects with fog.
        /// </summary>
        public bool Fog { get; set; } 

        public Scene()
        {
            Objects = new List<Object>();
            Camera = new Camera();
            Ambient = new Vector4(0, 0.5f, 0f, 0f);
            Lights = new List<Light>();
            Particles = new List<Particle>();
            Instances = new List<ObjectInstance>();
            Quads = new List<QuadObject>();
            Text = new List<TextObject>();
            Terrain = new List<TerrainObject>();
            Planes = new List<PlaneObject>();
            BackFaceCulling = true;
            WireFrameRendering = false;
            VerticalSync = true;
            HDRLighting = false;
            ShadowStrategy = ShadowRenderStrategy.FitToScene;
            Fog = false;
        }

        /// <summary>
        /// Adds an object into the scene.
        /// </summary>
        /// <param name="obj"></param>
        public void Add(Object obj)
        {
            Objects.Add(obj);
        }

        /// <summary>
        /// Adds a light to the scene.
        /// </summary>
        /// <param name="light"></param>
        public void Add(Light light)
        {
            Lights.Add(light);
        }

        /// <summary>
        /// Adds a particle to the scene.
        /// </summary>
        /// <param name="particle"></param>
        public void Add(Particle particle)
        {
            Particles.Add(particle);
        }

        /// <summary>
        /// Adds a object instance to the scene.
        /// </summary>
        /// <param name="instance"></param>
        public void Add(ObjectInstance instance)
        {
            Instances.Add(instance);
        }

        /// <summary>
        /// Adds a quad to the scene.
        /// </summary>
        /// <param name="quad"></param>
        public void Add(QuadObject quad)
        {
            Quads.Add(quad);
        }

        /// <summary>
        /// Adds a text object to the scene.
        /// </summary>
        /// <param name="text"></param>
        public void Add(TextObject text)
        {
            Text.Add(text);
        }

        /// <summary>
        /// Adds terrain to the scene.
        /// </summary>
        /// <param name="terrain"></param>
        public void Add(TerrainObject terrain)
        {
            Terrain.Add(terrain);
        }

        /// <summary>
        /// Adds a plane to the scene.
        /// </summary>
        /// <param name="plane"></param>
        public void Add(PlaneObject plane)
        {
            Planes.Add(plane);
        }

        /// <summary>
        /// Removes an object from the scene.
        /// </summary>
        /// <param name="obj"></param>
        public void Remove(Object obj)
        {
            Objects.Remove(obj);
        }

        /// <summary>
        /// Removes a plane from the scene.
        /// </summary>
        /// <param name="obj"></param>
        public void Remove(PlaneObject obj)
        {
            Planes.Remove(obj);
        }

        /// <summary>
        /// Removes a terrain object from the scene.
        /// </summary>
        /// <param name="obj"></param>
        public void Remove(TerrainObject obj)
        {
            Terrain.Remove(obj);
        }

        /// <summary>
        /// Removes a text object from the scene.
        /// </summary>
        /// <param name="obj"></param>
        public void Remove(TextObject obj)
        {
            Text.Remove(obj);
        }

        /// <summary>
        /// Removes a quad object from the scene.
        /// </summary>
        /// <param name="obj"></param>
        public void Remove(QuadObject obj)
        {
            Quads.Remove(obj);
        }

        /// <summary>
        /// Removes an instance object from the scene.
        /// </summary>
        /// <param name="obj"></param>
        public void Remove(ObjectInstance obj)
        {
            Instances.Remove(obj);
        }

    }
}
