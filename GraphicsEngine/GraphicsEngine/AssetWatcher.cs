﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GraphicsEngineBase
{
    public delegate IWatchable Reload(String filename);

    /// <summary>
    /// Interface for classes which represent assets which can be reloaded.
    /// </summary>
    public interface IWatchable
    {

        void SetItem(object obj);

    }

    /// <summary>
    /// Wrapper for a watchable object.
    /// </summary>
    public sealed class Watch
    {

        internal IWatchable Obj { get; set; }
        
        /// <summary>
        /// Reloads a resource
        /// </summary>
        internal Reload ReloadEvent { get; private set; }

        /// <summary>
        /// The file the asset was loaded from.
        /// </summary>
        public String File { get; private set; }

        internal String hashvalue;

        public Watch(IWatchable obj, String filename, Reload reload)
        {
            this.ReloadEvent = reload;
            this.File = filename;
            this.Obj = obj;
            this.hashvalue = ComputeHash();
        }

        public string ComputeHash()
        {

            try
            {

                System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create();

                byte[] hashed = md5.ComputeHash(System.IO.File.ReadAllBytes(File));

                return System.Text.Encoding.UTF7.GetString(hashed);

            }
            catch (System.Exception ex)
            {
                if (hashvalue != null)
                    return hashvalue;
            }

            throw new System.IO.FileNotFoundException();
        }

    }

    /// <summary>
    /// Represents a subsystem, which scans assets and reloads them if they change.
    /// This allows assets to be changed, and the effect can seen without restarting the program.
    /// </summary>
    internal class AssetWatcher
    {

        private Device device;
        private List<Watch> Watched { get; set; }
        public bool Running { get; private set; }
        private bool Paused { get; set; }

        public AssetWatcher(Device device)
        {
            Watched = new List<Watch>();
            this.device = device;
            Paused = false;
        }

        /// <summary>
        /// Checks to see an item has been modified.
        /// </summary>
        internal void Update()
        {
            Running = true;
            while (Running)
            {

                System.Threading.Thread.Sleep(1000);
                //Don't call every frame since disk operations are slow.

                if (!Paused)
                {

                    foreach (var item in new List<Watch>(Watched))
                    {
                        try
                        {
                            string hash = item.ComputeHash();

                            if (hash != item.hashvalue)
                            {
                                //reload - should be careful in case the item is already in use
                                IWatchable updated = item.ReloadEvent(item.File);
                                lock (item.Obj)
                                {
                                    ((IDisposable)item.Obj).Dispose();
                                    item.Obj.SetItem(updated);
                                }
                                item.hashvalue = hash;
                            }
                        }
                        catch (System.IO.FileNotFoundException ex)
                        {

                        }
                    }

                }

            }
        }

        /// <summary>
        /// Adds an item to be watched.
        /// </summary>
        /// <param name="item"></param>
        public void Add(Watch item)
        {
            Watched.Add(item);
        }

        /// <summary>
        /// Aborts the asset watcher. 
        /// </summary>
        internal void Stop()
        {
            Running = false;
        }

        /// <summary>
        /// Pauses the asset watcher.
        /// </summary>
        internal void Suspend()
        {
            Paused = true;
        }

        /// <summary>
        /// Resumes the asset watcher, from a pause.
        /// </summary>
        internal void Resume()
        {
            Paused = false;
        }
    }
}
