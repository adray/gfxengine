﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Windows.Forms;

namespace GraphicsEngineBase
{
    public abstract class DeviceDescription
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Description">The description of the device.</param>
        public DeviceDescription(String Description)
        {
            this.Description = Description;
        }

        /// <summary>
        /// The description of the device.
        /// </summary>
        public string Description { get; private set; }

        /// <summary>
        /// Creates the device.
        /// </summary>
        /// <param name="handle"></param>
        /// <returns></returns>
        public abstract Device MakeDevice(IntPtr handle);
    }
}
