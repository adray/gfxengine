﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GraphicsEngineBase
{
    /// <summary>
    /// Represents a factory for creating Device objects.
    /// </summary>
    public sealed class RenderFactory
    {

        /// <summary>
        /// Attempts to create a device from the list provided.
        /// If a list is not provided, the running directory will be searched for 
        /// compatible devices.
        /// </summary>
        /// <param name="windowHandle">The handle to render window.</param>
        /// <param name="desc">The dlls to try in order.</param>
        /// <returns>The selected graphics device.</returns>
        /// <exception cref="DeviceCouldNotBeCreatedException"></exception>
        public static Device MakeDevice(IntPtr windowHandle, String[] desc)
        {

            String[] files = new string[desc.Length];
            for (int i = 0; i < desc.Length; i++)
                files[i] = System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), desc[i]);

            List<DeviceDescription> description = GetDeviceDescriptions(files);

            return MakeDevice(windowHandle, description.ToArray());

        }

        /// <summary>
        /// Attempts to create a device from the list provided.
        /// If a list is not provided, the running directory will be searched for 
        /// compatible devices.
        /// </summary>
        /// <param name="windowHandle">The handle to render window.</param>
        /// <param name="desc">The description of the devices to try and create.</param>
        /// <returns>The selected graphics device.</returns>
        /// <exception cref="DeviceCouldNotBeCreatedException"></exception>
        public static Device MakeDevice(IntPtr windowHandle, params DeviceDescription[] desc)
        {
            List<DeviceDescription> description = new List<DeviceDescription>();

            if (desc.Length == 0)
            {

                string[] files = System.IO.Directory.GetFiles(Environment.CurrentDirectory, "*.dll");

                description = GetDeviceDescriptions(files);
            }
            else
            {
                description.AddRange(desc);
            }

            int i = 0;

            while (i < description.Count)
            {

                try
                {
                    Device device = description[i].MakeDevice(windowHandle);

                    if (device == null)
                        throw new DeviceCouldNotBeCreatedException();

                    Console.WriteLine("Using the {0} device.", description[i].Description);

                    return device;

                }
                catch (DeviceCouldNotBeCreatedException ex)
                {
                    Console.WriteLine(String.Format("Could not create {0} device.", description[i].Description));
                }

                i++;
            }

            throw new DeviceCouldNotBeCreatedException();

        }

        /// <summary>
        /// Gets a lists of device descriptions from a list a dynamic link libraries.
        /// </summary>
        /// <param name="files">The list of *.dlls.</param>
        /// <returns>The list of device descriptions</returns>
        private static List<DeviceDescription> GetDeviceDescriptions(string[] files)
        {
            List<DeviceDescription> description = new List<DeviceDescription>();

            foreach (var file in files)
            {
                try
                {
                    System.Reflection.Assembly assembly = System.Reflection.Assembly.LoadFile(file);

                    Type type = assembly.GetType(System.IO.Path.GetFileNameWithoutExtension(file) + ".DeviceDescription");

                    if (type != null)
                    {

                        Type[] types = assembly.GetTypes();

                        System.Reflection.MethodInfo info = type.GetMethod("GetDeviceDescription");

                        if (info != null)
                        {

                            DeviceDescription des = (DeviceDescription)info.Invoke(null, new object[0]);
                            description.Add(des);

                        }

                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            return description;
        }

    }
}
