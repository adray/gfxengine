﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GraphicsEngineBase
{
    /// <summary>
    /// Represents the glue for communicating with the GPU.
    /// </summary>
    public abstract class Device : IDisposable
    {

        private AssetWatcher AssetWatcher { get; set; }
        private System.Threading.Thread WatchThread { get; set; }

        public Device()
        {
            AssetWatcher = new AssetWatcher(this);
            WatchThread = new System.Threading.Thread(new System.Threading.ThreadStart(AssetWatcher.Update));
            WatchThread.Name = "WatchThread";
            WatchThread.Start();
        }

        /// <summary>
        /// Adds an item to be watched.
        /// </summary>
        /// <param name="item"></param>
        protected void AddWatchedItem(Watch item)
        {
            AssetWatcher.Add(item);
        }

        /// <summary>
        /// Suspends the watcher.
        /// </summary>
        protected void SuspendWatcher()
        {
            AssetWatcher.Suspend();
        }

        /// <summary>
        /// Resumes the item watcher.
        /// </summary>
        protected void ResumeWatcher()
        {
            AssetWatcher.Resume();
        }

        /// <summary>
        /// Gets a renderer.
        /// </summary>
        /// <returns></returns>
        public abstract Renderer Renderer();

        /// <summary>
        /// Loads a texture from a bitmap. 
        /// </summary>
        /// <param name="pointer"></param>
        /// <returns></returns>
        public abstract Texture LoadTexture(System.Drawing.Bitmap bitmap);

        /// <summary>
        /// Loads a texture from a file.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public abstract Texture LoadTexture(string filename);

        /// <summary>
        /// Loads a texture from a stream.
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        public abstract Texture LoadTexture(System.IO.Stream stream);

        /// <summary>
        /// Loads an object from a file.
        /// </summary>
        /// <param name="vertextype"></param>
        /// <param name="filename"></param>
        /// <returns></returns>
        public abstract ObjectFactory LoadObject(string filename);

        /// <summary>
        /// Loads an object from a stream.
        /// </summary>
        /// <param name="vertextype"></param>
        /// <param name="filename"></param>
        /// <returns></returns>
        public abstract ObjectFactory LoadObject(System.IO.Stream stream);

        /// <summary>
        /// Creates an empty mesh with the appropriate data storage.
        /// </summary>
        /// <param name="vertextype"></param>
        /// <returns></returns>
        public abstract Mesh CreateMesh(VertexType vertextype);

        /// <summary>
        /// Creates an empty instance mesh.
        /// </summary>
        /// <returns></returns>
        public abstract MeshInstance CreateInstance();

        /// <summary>
        /// Loads terrain from a file.
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public abstract TerrainObject LoadTerrain(string image);

        /// <summary>
        /// The width of the window.
        /// </summary>
        public abstract int Width { get; }

        /// <summary>
        /// The height of the window.
        /// </summary>
        public abstract int Height { get; }

        /// <summary>
        /// The description of the hardware.
        /// </summary>
        public abstract string HardwareDescription { get; }

        /// <summary>
        /// Disposes of unmanaged resources.
        /// </summary>
        public virtual void Dispose()
        {
            this.AssetWatcher.Stop();
        }

        /// <summary>
        /// Loads a scene from a file.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public abstract Scene LoadScene(string filename);
    }
}
