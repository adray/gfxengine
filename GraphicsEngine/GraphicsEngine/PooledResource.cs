﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace GraphicsEngineBase
{
    /// <summary>
    /// Represents a container for a object, where the access of the object can be locked.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public sealed class PooledResource<T> where T : IDisposable
    {

        public T Value { get; private set; }
        public bool IsUsed { get; private set; }

        public PooledResource(T value)
        {
            this.Value = value;
            IsUsed = false;
        }

        public void Lock()
        {
            Debug.Assert(!IsUsed, "Already locked, probably something is incorrect.");

            IsUsed = true;
        }

        public void Unlock()
        {
            Debug.Assert(IsUsed, "Already unlocked, probably something is incorrect.");

            IsUsed = false;
        }

    }
}
