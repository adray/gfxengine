﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using GraphicsEngineMath;

namespace GraphicsEngineBase.Loader
{
    /// <summary>
    /// Loader for WaveFront (.obj) files.
    /// </summary>
    public static class ObjLoader
    {

        public static ObjectFactory Load(Device device, VertexType vertexType, string filename)
        {

            String temp = System.IO.Directory.GetCurrentDirectory();
            System.IO.Directory.SetCurrentDirectory(System.IO.Path.GetDirectoryName(filename));

            StreamReader sr = new StreamReader(System.IO.Path.GetFileName(filename));

            ObjectFactory m = Load(device, vertexType, sr);

            sr.Close();
            System.IO.Directory.SetCurrentDirectory(temp);

            return m;

        }

        public static ObjectFactory Load(Device device, VertexType vertexType, StreamReader sr)
        {
#if DEBUG
            System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();
#endif

            SortedDictionary<String, ObjectSubset> m = new SortedDictionary<string, ObjectSubset>();

            ObjectSubset current = new ObjectSubset()
            {
                ID="Root",
                Material=new Material(),
                Mesh=device.CreateMesh(vertexType)
            };


            List<Vector3> pos = new List<Vector3>();
            List<Vector3> normals = new List<Vector3>();
            List<Vector2> texcoord = new List<Vector2>();

            SortedDictionary<String, Material> material = new SortedDictionary<String, Material>();

            string groupname = "Root";
            Material current_material = new Material();

            try
            {

                while (!sr.EndOfStream)
                {

                    string[] line = sr.ReadLine().Split(new char[]{' '}, StringSplitOptions.RemoveEmptyEntries);

                    if (line.Length == 0)
                        continue;

                    switch (line[0])
                    {
                        case "#":   //comment
                            break;
                        case "mtllib":  //material lib

                            SortedDictionary<String, Material> mat = MaterialLoader.Load(line[1], device);

                            foreach (var n in mat)
                                material.Add(n.Key, n.Value);

#if DEBUG
                            watch.Start();
#endif

                            break;
                        case "usemtl":  //use material

                            string id = groupname;
                            if (line.Length > 1)
                                id += line[1];

                            current = new ObjectSubset()
                                {
                                    Mesh = device.CreateMesh(vertexType),
                                    Material = new Material(),
                                    ID=id
                                };
                            m.Add(current.ID, current);

                            current.Material.DiffuseTexture = current_material.DiffuseTexture;
                            current.Material.BumpTexture = current_material.BumpTexture;

                            if (line.Length > 1 && material.Keys.Contains(line[1]))
                            {
                                Material temp = material[line[1]];
                                if (temp.DiffuseTexture != null)
                                    current.Material.DiffuseTexture = temp.DiffuseTexture;
                                if (temp.BumpTexture != null)
                                    current.Material.BumpTexture = temp.BumpTexture;
                            }

                            if (current.Material.DiffuseTexture != null)
                                current.RenderFlags |= RenderFlags.DiffuseMap;
                            if (current.Material.BumpTexture != null)
                                current.RenderFlags |= RenderFlags.BumpMap;

                            current_material = current.Material;

                            break;
                        case "g":   //new subset/mesh

                            groupname = line[1];
                            //current_material = new Material();

                            break;
                        case "v":   //vertex

                            if (line.Length > 3)
                            {

                                float x, y, z;

                                 if(!float.TryParse(line[1], out x))
                                     throw new Exception();

                                 if (!float.TryParse(line[2], out y))
                                     throw new Exception();

                                 if (!float.TryParse(line[3], out z))
                                     throw new Exception();

                                 pos.Add(new Vector3(x, y, z));
                            }

                            break;
                        case "vn":  //normal

                            if (line.Length > 3)
                            {

                                float x, y, z;

                                if (!float.TryParse(line[1], out x))
                                    throw new Exception();

                                if (!float.TryParse(line[2], out y))
                                    throw new Exception();

                                if (!float.TryParse(line[3], out z))
                                    throw new Exception();

                                normals.Add(new Vector3(x, y, z));
                            }

                            break;
                        case "vt":  //texture

                            if (line.Length > 2)
                            {

                                float x, y;

                                if (!float.TryParse(line[1], out x))
                                    throw new Exception();

                                if (!float.TryParse(line[2], out y))
                                    throw new Exception();

                                texcoord.Add(new Vector2(x, y));
                            }

                            break;
                        case "f":   //face

                            if (line.Length == 4)
                            {

                                Vertex[] v = new Vertex[3];

                                for (int j = 1; j < 4; j++)
                                {

                                    v[j - 1] = new Vertex(current.Mesh.DataCollection.VertexSize / sizeof(float));

                                    //pos/texcoord?/normal?

                                    string[] split = line[j].Split('/');


                                    if (System.Text.RegularExpressions.Regex.IsMatch(line[j], "[0-9]+.*"))
                                    {
                                        int i = 0;

                                        if (!int.TryParse(split[0], out i))
                                            throw new Exception();

                                        int pq = current.Mesh.DataCollection.First(DataType.Position) / sizeof(float);

                                        if (pq >= 0)
                                        {
                                            v[j - 1].SetData(pq, pos[i - 1]);
                                            v[j - 1].SetData(pq + 3, 1);
                                        }
                                    }

                                    if (System.Text.RegularExpressions.Regex.IsMatch(line[j], ".*/[0-9]+/.*"))
                                    {
                                        int i = 0;

                                        if (!int.TryParse(split[1], out i))
                                            throw new Exception();

                                        int pq = current.Mesh.DataCollection.First(DataType.Texcoord) / sizeof(float);

                                        if (pq >= 0)
                                        {
                                            v[j - 1].SetData(pq, texcoord[i - 1].X, texcoord[i - 1].Y);
                                        }
                                    }

                                    if (System.Text.RegularExpressions.Regex.IsMatch(line[j], ".*/.*/[0-9]+"))
                                    {
                                        int i = 0;

                                        if (!int.TryParse(split[split.Length - 1], out i))
                                            throw new Exception();

                                        int pq = current.Mesh.DataCollection.First(DataType.Normal) / sizeof(float);

                                        if (pq >= 0)
                                        {

                                            v[j - 1].SetData(pq, normals[i - 1]);
                                            v[j - 1].SetData(pq + 3, 1);

                                        }
                                    }

                                }

                                current.Mesh.AddTriangle(new Triangle(v[0], v[1], v[2]));

                            }
                            else if (line.Length == 5)
                            {

                                Vertex[] v = new Vertex[4];

                                for (int j = 1; j < 5; j++)
                                {

                                    v[j - 1] = new Vertex(current.Mesh.DataCollection.VertexSize / sizeof(float));

                                    //pos/texcoord?/normal?

                                    string[] split = line[j].Split('/');


                                    if (System.Text.RegularExpressions.Regex.IsMatch(line[j], "[0-9]+.*"))
                                    {
                                        int i = 0;

                                        if (!int.TryParse(split[0], out i))
                                            throw new Exception();

                                        int pq = current.Mesh.DataCollection.First(DataType.Position) / sizeof(float);

                                        if (pq >= 0)
                                        {
                                            v[j - 1].SetData(pq, pos[i - 1]);
                                            v[j - 1].SetData(pq + 3, 1);
                                        }
                                    }

                                    if (System.Text.RegularExpressions.Regex.IsMatch(line[j], ".*/[0-9]+/.*"))
                                    {
                                        int i = 0;

                                        if (!int.TryParse(split[1], out i))
                                            throw new Exception();

                                        int pq = current.Mesh.DataCollection.First(DataType.Texcoord) / sizeof(float);

                                        if (pq >= 0)
                                        {
                                            v[j - 1].SetData(pq, texcoord[i - 1].X, texcoord[i - 1].Y);
                                        }
                                    }

                                    if (System.Text.RegularExpressions.Regex.IsMatch(line[j], ".*/.*/[0-9]+"))
                                    {
                                        int i = 0;

                                        if (!int.TryParse(split[split.Length - 1], out i))
                                            throw new Exception();

                                        int pq = current.Mesh.DataCollection.First(DataType.Normal) / sizeof(float);

                                        if (pq >= 0)
                                        {

                                            v[j - 1].SetData(pq, normals[i - 1]);
                                            v[j - 1].SetData(pq + 3, 1);

                                        }
                                    }

                                }

                                current.Mesh.AddQuad(v[0], v[1], v[2], v[3]);

                            }

                            break;

                    }


                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(String.Format("Obj mesh loader failed to load {0}.", "input file"));
            }



            
            var subsets = new List<ObjectSubset>();
            var instance = new List<ObjectInstanceSubset>();
            if (m.Count > 0)
                foreach (var o in m)
                {
                    subsets.Add(o.Value);
                    instance.Add(new ObjectInstanceSubset() { Mesh = o.Value.Mesh });
                }
            else
            {
                subsets.Add(current);
                instance.Add(new ObjectInstanceSubset() { Mesh = current.Mesh });
            }
            
            ObjectFactory obj = new ObjectFactory(device, subsets, instance);

#if DEBUG
            Console.WriteLine("file loaded: {0}ms", watch.ElapsedMilliseconds);
#endif
            
            return obj;
        }

    }
}
