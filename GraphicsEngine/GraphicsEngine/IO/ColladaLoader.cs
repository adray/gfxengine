﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace GraphicsEngineBase.Loader
{
    /// <summary>
    /// Loads a scene from a Collada file.
    /// </summary>
    public class ColladaLoader
    {

        private class ColladaNode<T>
        {
            public T value;
            public string id;
        }

        private class DataNode
        {
            public string id;
            public Vertex[] data;
        }

        Device device;
        String filename;
        List<ColladaNode<Mesh>> m;
        List<ColladaNode<LightType>> lightlib;

        private ColladaLoader(Device d, string filename)
        {
            device = d;
            this.filename = filename;
        }

        private Scene Parse()
        {
            System.Xml.XmlDocument document = new System.Xml.XmlDocument();
            document.Load(filename);

            for (int i = 0; i < document.ChildNodes.Count; i++)
            {

                XmlNode node = document.ChildNodes.Item(i);


                if (node.Name == "COLLADA")
                {
                    return ParseCollada(node);
                }

                

            }

            return null;
        }

        public static Scene Load(Device d, string filename)
        {

            return new ColladaLoader(d, filename).Parse();

        }

        private Scene ParseCollada(XmlNode node)
        {

            Scene scene = new Scene();

            m=new List<ColladaNode<Mesh>>();

            for (int i = 0; i < node.ChildNodes.Count; i++)
            {

                var child = node.ChildNodes[i];

                switch (child.Name)
                {
                    case "asset":   //settings
                        ParseAsset(child);
                        break;
                    case "library_cameras": //cameras
                        break;
                    case "library_lights":  //lights
                        lightlib = ParseLights(child);
                        break;
                    case "library_geometries":  //vertices/faces
                        m = ParseGeometries(child);
                        break;
                    case "library_visual_scenes":   //scene positions
                        scene = ParseObjects(child);
                        break;
                    case "scene":
                        break;
                }

            }

            return scene;

        }

        private List<ColladaNode<LightType>> ParseLights(XmlNode node)
        {

            List<ColladaNode<LightType>> lights = new List<ColladaNode<LightType>>();

            for (int i = 0; i < node.ChildNodes.Count; i++)
            {

                var child = node.ChildNodes[i];

                switch (child.Name)
                {
                    case "light":
                        lights.Add(ParseLight(child));
                        break;
                }

            }

            return lights;

        }

        private ColladaNode<LightType> ParseLight(XmlNode node)
        {

            ColladaNode<LightType> light = new ColladaNode<LightType>();

            light.id = node.Attributes["id"].Value;

            for (int i = 0; i < node.ChildNodes.Count; i++)
            {

                var child = node.ChildNodes[i];

                switch (child.Name)
                {
                    case "technique_common":
                        light.value = (LightType) Enum.Parse(typeof(LightType), child.ChildNodes[0].Name, true);
                        break;
                }

            }

            return light;

        }

        private Scene ParseObjects(XmlNode node)
        {

            for (int i = 0; i < node.ChildNodes.Count; i++)
            {

                var child = node.ChildNodes[i];

                switch (child.Name)
                {
                    case "visual_scene":
                        return ParseScene(child);
                }

            }

            return new Scene();


        }

        private Scene ParseScene(XmlNode node)
        {
            Scene scene = new Scene();

            for (int i = 0; i < node.ChildNodes.Count; i++)
            {

                var child = node.ChildNodes[i];

                switch (child.Name)
                {
                    case "node":
                        ParseNode(child, scene);
                        break;
                }

            }

            return scene;
        }

        enum SceneType
        {
            None,
            Object,
            Light,
            Camera
        }

        private void ParseNode(XmlNode node, Scene scene)
        {

            GraphicsEngineMath.Vector3 pos = new GraphicsEngineMath.Vector3();
            string reference="";
            SceneType type=SceneType.None;

            for (int i = 0; i < node.ChildNodes.Count; i++)
            {

                var child = node.ChildNodes[i];

                switch (child.Name)
                {
                    case "translate":

                        String[] data = child.InnerText.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                        if (data.Length >= 3)
                        {

                            pos.X = float.Parse(data[0]);
                            pos.Y = float.Parse(data[2]);
                            pos.Z = float.Parse(data[1]);

                        }

                        break;
                    case "instance_geometry":
                        reference = child.Attributes["url"].Value.Remove(0,1);
                        type = SceneType.Object;
                        break;
                    case "instance_light":
                        reference = child.Attributes["url"].Value.Remove(0, 1);
                        type = SceneType.Light;
                        break;
                }

            }

            switch (type)
            {
                case SceneType.Object:

                    foreach (var m in this.m)
                        if (m.id == reference)
                        {
                            Object obj = new Object(m.value, new Material(), RenderFlags.None)
                                {
                                    RootTransform = GraphicsEngineMath.Matrix4x4.Translate(pos.X, pos.Y, pos.Z)
                                };
                            scene.Add(obj);
                            return;
                        }
                    break;
                case SceneType.Light:

                    foreach (var m in this.lightlib)
                        if (m.id == reference)
                        {
                            Light light = new Light()
                            {
                                Position = pos,
                                Radius = new GraphicsEngineMath.Vector3(5,5,5),
                                LightType=m.value,
                                Color=new GraphicsEngineMath.Vector4(1,1,1,1)
                            };
                            scene.Add(light);
                            return;
                        }
                    break;
            }
        }

        private void ParseAsset(XmlNode node)
        {
            //author, time etc..
        }

        private List<ColladaNode<Mesh>> ParseGeometries(XmlNode node)
        {

            List<ColladaNode<Mesh>> meshes = new List<ColladaNode<Mesh>>();


            for (int i = 0; i < node.ChildNodes.Count; i++)
            {

                var child = node.ChildNodes[i];

                switch (child.Name)
                {
                    case "geometry":
                        meshes.Add(ParseGeometry(child));
                        break;
                }

            }


            return meshes;

        }

        private ColladaNode<Mesh> ParseGeometry(XmlNode node)
        {

            for (int i = 0; i < node.ChildNodes.Count; i++)
            {

                var child = node.ChildNodes[i];

                switch (child.Name)
                {
                    case "mesh":
                        ColladaNode<Mesh> mesh = new ColladaNode<Mesh>();
                        mesh.value = ParseMesh(child);
                        mesh.id = node.Attributes["id"].Value;
                        return mesh;
                }

            }

            return null;

        }

        private Mesh ParseMesh(XmlNode node)
        {

            List<DataNode> nodes = new List<DataNode>();

            for (int i = 0; i < node.ChildNodes.Count; i++)
            {

                var child = node.ChildNodes[i];

                switch (child.Name)
                {
                    case "source":
                        nodes.Add(ParseSource(child));
                        break;
                    case "vertices":
                        ParseVertices(child);
                        break;
                    case "polylist":
                        return ParsePolyList(child, nodes);
                }

            }

            return null;
        }

        private void ParseVertices(XmlNode node)
        {

            for (int i = 0; i < node.ChildNodes.Count; i++)
            {

                var child = node.ChildNodes[i];

                switch (child.Name)
                {
                    case "input":
                        
                        break;
                }

            }

            
        }

        private Mesh ParsePolyList(XmlNode node, List<DataNode> nodes)
        {

            int polycount = Int32.Parse(node.Attributes["count"].Value);
            VertexType v=VertexType.PositionNormalTextured;

            Vertex[] vertices = new Vertex[polycount*4];

            int[] vcount=null;

            int inputcount = 0;

            for (int i = 0; i < node.ChildNodes.Count; i++)
            {

                var child = node.ChildNodes[i];

                switch (child.Name)
                {
                    case "vcount":
                        
                        String[] s = child.InnerText.Split(new char[] {' '}, StringSplitOptions.RemoveEmptyEntries);
                        vcount = new int[s.Length];

                        for (int j = 0; j < s.Length; j++)
                        {
                            vcount[j] = Int32.Parse(s[j]);
                        }

                        break;
                    case "input":

                        inputcount++;

                        break;
                    case "p":

                        String[] s2 = child.InnerText.Split(' ');

                        for (int j = 0; j < s2.Length; j += inputcount)
                        {
                            Vertex vert = new Vertex(10);
                            //swap z and y axis
                            vert.SetData(0, nodes[0].data[Int32.Parse(s2[j])].data[0]);
                            vert.SetData(1, nodes[0].data[Int32.Parse(s2[j])].data[2]);
                            vert.SetData(2, nodes[0].data[Int32.Parse(s2[j])].data[1]);
                            vert.SetData(3, 1);
                            vert.SetData(4, nodes[1].data[Int32.Parse(s2[j+1])].data[0]);
                            vert.SetData(5, nodes[1].data[Int32.Parse(s2[j + 1])].data[2]);
                            vert.SetData(6, nodes[1].data[Int32.Parse(s2[j + 1])].data[1]);
                            vert.SetData(7, 1);
                            vert.SetData(8, nodes[2].data[Int32.Parse(s2[j+2])].data);

                            vertices[j / inputcount] = vert;
                        }

                        break;
                }
            }

            Mesh m = device.CreateMesh(v);

            int k = 0;

            for (int i = 0; i < vcount.Length; i++)
            {
                
                if (vcount[i] == 4)
                    m.AddQuad(vertices[k], vertices[k+1], vertices[k+2], vertices[k+3]);
                else if (vcount[i] == 3)
                    m.AddTriangle(new Triangle(vertices[k], vertices[k + 1], vertices[k + 2]));

                k += vcount[i];
            }

            return m;

        }

        private DataNode ParseSource(XmlNode node)
        {

            float[] data=null;
            int stride=1;

            for (int i = 0; i < node.ChildNodes.Count; i++)
            {

                var child = node.ChildNodes[i];

                switch (child.Name)
                {
                    case "float_array":   //float array
                        data = ParseFloatArray(child);
                        break;
                    case "technique_common": //technique
                        stride = ParseDescriptor(child);
                        break;
                }

            }

            DataNode n = new DataNode();

            n.data = new Vertex[data.Length / stride];
            for (int i = 0; i < n.data.Length; i++)
                n.data[i] = new Vertex(stride);

            n.id = node.Attributes["id"].Value;

            for (int i = 0; i < data.Length; i++)
                n.data[i / stride].SetData(i % stride, data[i]);

            return n;

        }

        private int ParseDescriptor(XmlNode node)
        {

            for (int i = 0; i < node.ChildNodes.Count; i++)
            {

                var child = node.ChildNodes[i];

                switch (child.Name)
                {
                    case "accessor": 
                        return ParseDescriptorAccessor(child);
                }

            }

            return 1;
        }

        private int ParseDescriptorAccessor(XmlNode node)
        {

            int count = Int32.Parse(node.Attributes["count"].Value);
            int stride = Int32.Parse(node.Attributes["stride"].Value);
            String[] description = new String[stride];
            String[] type = new String[stride];

            for (int i = 0; i < node.ChildNodes.Count; i++)
            {

                var child = node.ChildNodes[i];

                switch (child.Name)
                {
                    case "param":
                        description[i] = child.Attributes["name"].Value;
                        type[i] = child.Attributes["type"].Value;
                        break;
                }

            }

            return stride;

        }

        private float[] ParseFloatArray(XmlNode node)
        {
            float[] parsed = new float[Int32.Parse(node.Attributes["count"].Value)];


            string[] data = node.InnerText.Split(' ');

            for (int i = 0; i < data.Length; i++)
            {
                parsed[i] = float.Parse(data[i]);
            }


            return parsed;
        }

    }
}
