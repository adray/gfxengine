﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using GraphicsEngineMath;
using System.Drawing;

namespace GraphicsEngineBase.Loader
{
    /// <summary>
    /// Loader for terrains from a heightmap.
    /// </summary>
    public static class TerrainLoader
    {

        public static TerrainObject Load(Device device, string map)
        {

            Mesh mesh = device.CreateMesh(VertexType.Terrain);
            
            Bitmap img = new Bitmap(map);

            Texture heightmap = device.LoadTexture(map);

            Bitmap weightmap = new Bitmap(img.Width, img.Height);

            Vector3[] v = new Vector3[img.Width * img.Height];
            
            System.Drawing.Imaging.BitmapData bitdata = img.LockBits(new System.Drawing.Rectangle(0, 0, img.Width, img.Height),
                 System.Drawing.Imaging.ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            System.Drawing.Imaging.BitmapData bitdata2 = weightmap.LockBits(new System.Drawing.Rectangle(0, 0, img.Width, img.Height),
                 System.Drawing.Imaging.ImageLockMode.WriteOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);


            unsafe
            {

                byte* pointer = (byte*)bitdata.Scan0;
                byte* pointer2 = (byte*)bitdata2.Scan0;

                for (int i = 0; i < img.Width; i++)
                {
                    for (int j = 0; j < img.Height; j++)
                    {

                        Vector3 vertex = new Vector3((float)i / img.Width,
                           // 0,
                            (*pointer)/255.0f,
                            (float)j / img.Height);

                        *pointer2 = *pointer; pointer2++;
                        *pointer2 = (byte)(255-*pointer); pointer2++;

                        *pointer2 += 2;

                        v[i + j * img.Width] = vertex;
                        pointer += 4;

                    }
                }

            }

            img.UnlockBits(bitdata);
            weightmap.UnlockBits(bitdata2);

            int vertexsize = mesh.DataCollection.VertexSize / 4;

            Vector3[] normals = CalculateNormals(v, img.Width, img.Height);

            for (int i =0 ; i < img.Width-1; i++)
                for (int j = 0; j < img.Height-1; j++)
                {

                    int[] index = new int[]{i + img.Width * j, (i + 1) + img.Width * (j + 1),
                        i + 1 + img.Width * j, i + img.Width * (j + 1) };

                    Vertex v1 = new Vertex(vertexsize).SetData(0, v[index[0]].X, v[index[0]].Z).SetData(2,
                        normals[index[0]]);
                    Vertex v2 = new Vertex(vertexsize).SetData(0, v[index[1]].X, v[index[1]].Z).SetData(2,
                        normals[index[1]]);
                    Vertex v3 = new Vertex(vertexsize).SetData(0, v[index[2]].X, v[index[2]].Z).SetData(2,
                        normals[index[2]]);
                    Vertex v4 = new Vertex(vertexsize).SetData(0, v[index[3]].X, v[index[3]].Z).SetData(2,
                        normals[index[3]]);

                    mesh.AddTriangle(new Triangle(v1, v2, v3));

                    mesh.AddTriangle(new Triangle(v2, v1, v4));

                }

            return new TerrainObject(mesh, heightmap, device.LoadTexture(weightmap), img, v);

        }

        private static Vector3[] CalculateNormals( Vector3[] vertices, int width, int height)
        {
            Vector3[] normals=new Vector3[width*height];
            Vector3 scale = //new Vector3(64, 4, 64);
                new Vector3(1, 1, 1);

            for (int i = 0; i < width * height; i++)
                normals[i] = new Vector3(0,1,0);

            for (int i = 1; i < width-1; i++)
                for (int j = 1; j < height-1; j++)
                {
                    Vector3 vert = vertices[i + j * width]*scale;

                    Vector3 a = vertices[(i+1) + j*width]*scale - vert;
                    Vector3 b = vertices[i + (j - 1) * width]*scale - vert;
                    Vector3 c = vertices[(i - 1) + j * width]*scale - vert;
                    Vector3 d = vertices[i + (j + 1) * width]*scale - vert;
                    a.Normalize(); b.Normalize(); c.Normalize(); d.Normalize();

                    Vector3 v1 = Vector3.Cross(d, a);
                    Vector3 v2 = Vector3.Cross(c, d);
                    Vector3 v3 = Vector3.Cross(a, b);
                    Vector3 v4 = Vector3.Cross(b, c);
                    v1.Normalize(); v2.Normalize(); v3.Normalize(); v4.Normalize();

                    Vector3 normal = v1 + v2 + v3 + v4;
                    normal.Normalize();
                    normals[i + j * width] = normal;

                    System.Diagnostics.Debug.Assert(Vector3.Dot(normals[i+j*width], new Vector3(0, 1, 0)) > 0, normals[i+j*width].ToString());

                }

            //for (int i = 0; i < width * height; i++)
            //    System.Diagnostics.Debug.Assert(normals[i].Equals(new Vector3(0, 1, 0)), normals[i].ToString());

            return normals;
        }

    }
}
