﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using GraphicsEngineMath;

namespace GraphicsEngineBase.Loader
{
    /// <summary>
    /// Loader for Stanford Polygon Files.
    /// </summary>
    public static class PlyLoader
    {

        private enum PlyPropertyType
        {
            x, y, z, vertexIndices
        }

        private class PlyProperty
        {

            public PlyPropertyType type;
            public bool list = false;

            public PlyProperty(PlyPropertyType type)
            {
                this.type = type;
            }

        }

        private enum PlyElementType
        {
            Face,
            Vertex
        }

        private class PlyElement
        {

            public PlyElementType type;
            public List<PlyProperty> properties;
            public int count;

            public PlyElement(PlyElementType type, int count)
            {
                this.type = type;
                properties = new List<PlyProperty>();
                this.count = count;
            }

        }

        public static Mesh Load(Device device, VertexType vertexType, string filename)
        {

            StreamReader sr = new StreamReader(filename);

            Mesh m = Load(device, vertexType, sr);

            sr.Close();

            return m;

        }

        public static Mesh Load(Device device, VertexType vertexType, StreamReader sr)
        {

            Mesh m = device.CreateMesh(vertexType);

            List<Vector3> pos = new List<Vector3>();
            List<Vector3> normals = new List<Vector3>();
            List<Vector2> texcoord = new List<Vector2>();

            List<PlyElement> elements = new List<PlyElement>();

            bool isHeader = true;

            //header
            try
            {

                while (!sr.EndOfStream && isHeader)
                {

                    string[] line = sr.ReadLine().Split(' ');

                    if (line.Length == 0)
                        continue;

                    switch (line[0])
                    {
                        case "comment":
                            break;
                        case "element":

                            if (line.Length < 3)
                                continue;

                            switch (line[1])
                            {
                                case "face":
                                    int faceCount = Int32.Parse(line[2]);
                                    elements.Add(new PlyElement(PlyElementType.Face, faceCount));
                                    break;
                                case "vertex":
                                    int vertexCount = Int32.Parse(line[2]);
                                    elements.Add(new PlyElement(PlyElementType.Vertex, vertexCount));
                                    break;
                            }

                            break;
                        case "end_header":
                            isHeader = false;
                            break;
                        case "property":

                            if (line.Length < 3)
                                continue;

                            bool list=false;
                            PlyPropertyType type=0;

                            switch (line[1])
                            {
                                case "list":
                                    list = true;
                                    type = PlyPropertyType.vertexIndices;
                                    break;
                            }

                            if (!list)
                            {
                                switch (line[2])
                                {
                                    case "x":
                                        type = PlyPropertyType.x;
                                        break;
                                    case "y":
                                        type = PlyPropertyType.y;
                                        break;
                                    case "z":
                                        type = PlyPropertyType.z;
                                        break;
                                }
                            }

                            PlyProperty prop = new PlyProperty(type);
                            prop.list = list;

                            elements.Last().properties.Add(prop);
                            break;
                    }

                }
            }
            catch (Exception ex)
            {

            }

            //body
            try
            {

                int elementcount = 0;
                int count = 0;

                while (!sr.EndOfStream && elementcount < elements.Count)
                {

                    string[] line = sr.ReadLine().Split(' ');

                    if (line.Length == 0)
                        continue;

                    switch (elements[elementcount].type)
                    {
                        case PlyElementType.Vertex:
                            pos.Add(new Vector3(float.Parse(line[0]), float.Parse(line[1]), float.Parse(line[2])));
                            break;
                        case PlyElementType.Face:
                            m.AddTriangle(new Triangle(new Vertex(10).SetData(0, pos[int.Parse(line[1])]).SetData(3,1),
                                new Vertex(10).SetData(0, pos[int.Parse(line[2])]).SetData(3,1),
                                new Vertex(10).SetData(0, pos[int.Parse(line[3])]).SetData(3,1)));
                            break;
                    }

                    count++;

                    if (count > elements[elementcount].count)
                    {
                        count = 0;
                        elementcount++;
                    }
                

                }
            }
            catch (Exception ex)
            {

            }

            return m;
        }
    }
}
