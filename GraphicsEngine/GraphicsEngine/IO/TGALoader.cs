﻿/* 
 * The TGA loader is based on the TGA specification provided here:
 * http://www.gamers.org/dEngine/quake3/TGA.txt
 * 
 * 
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace GraphicsEngineBase.Loader
{
    public class TGAData
    {

        public byte[] Data { get; internal set; }
        public int Width { get; internal set; }
        public int Height { get; internal set; }
        public int PixelDepth { get; internal set; }

        internal TGAData()
        {

        }

    }

    /// <summary>
    /// Loader for Tga texture files.
    /// </summary>
    public static class TGALoader
    {

        public static TGAData Load(string filename)
        {
            
            byte[] inital = System.IO.File.ReadAllBytes(filename);

            BinaryReader stream = new BinaryReader(new MemoryStream(inital));
            int id_length = stream.ReadByte();

            for (int i = 0; i < id_length; i++)
                stream.ReadByte();

            int colour_map_type = stream.ReadByte();
            int image_type = stream.ReadByte();

            if (image_type != 2)
            {
                stream.Close();
                throw new Exception("TGA format not recognized.");
            }

            int[] colourmap=new int[5];
            //colour map
            for (int i = 0; i<5; i++)
                colourmap[i]=stream.ReadByte();

            int x_origin = stream.ReadInt16();
            int y_origin = stream.ReadInt16();
            int width = stream.ReadInt16();
            int height = stream.ReadInt16();
            int pixeldepth = stream.ReadByte();
            int img_descriptor = stream.ReadByte();
            
            byte[] bytes=new byte[width*height*4];

            if (width * height * 4 <= stream.BaseStream.Length - stream.BaseStream.Position)
            {
                for (int i = 0; i < width * height; i++)
                {
                    bytes[i * 4+2] = stream.ReadByte();
                    bytes[i * 4 + 1] = stream.ReadByte();
                    bytes[i * 4 + 0] = stream.ReadByte();
                    bytes[i * 4 + 3] = 255; stream.ReadByte();    //empty
                }
            }
            else if (width * height * 3 <= stream.BaseStream.Length - stream.BaseStream.Position)
            {
                for (int i = 0; i < width * height; i++)
                {
                    bytes[i * 4+2] = stream.ReadByte();
                    bytes[i * 4 + 1] = stream.ReadByte();
                    bytes[i * 4 + 0] = stream.ReadByte();
                    bytes[i * 4 + 3] = 255;
                }
            }
            else if (width * height * 2 <= stream.BaseStream.Length - stream.BaseStream.Position)
            {
                for (int i = 0; i < width * height; i++)
                {
                    int second = stream.ReadByte();
                    int first = stream.ReadByte();

                    bytes[i * 4+2] = (byte) ((first >> 2) & 32);
                    bytes[i * 4 + 1] = (byte) (first & 2);
                    bytes[i * 4 + 0] = (byte) (second & 32);
                    bytes[i * 4 + 3] = 255;
                }
            }
            else
                throw new FileNotFoundException(String.Format("Filename {0}. Format not recognized. {1} bytes per pixel?", filename,
                    (stream.BaseStream.Length-stream.BaseStream.Position)/(width*height)));

            if (stream.BaseStream.Length - stream.BaseStream.Position == 16)
            {
                //footer
                for (int i = 0; i < 16; i++)
                    stream.ReadByte();
            }

            //System.Diagnostics.Debug.Assert(stream.BaseStream.Position >= stream.BaseStream.Length);

            TGAData data = new TGAData();
            data.Data = bytes;
            data.Width = width;
            data.Height = height;
            data.PixelDepth = 32;

            stream.Close();

            return data;

        }

    }
}
