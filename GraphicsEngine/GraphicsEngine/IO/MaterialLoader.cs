﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace GraphicsEngineBase.Loader
{
    /// <summary>
    /// Loader for WaveFront material files.
    /// </summary>
    public sealed class MaterialLoader
    {
        private class MaterialJob
        {

            public Material material;
            public string bumpmap;
            public string diffusemap;

        }


        public static SortedDictionary<String, Material> Load(string filename, Device device)
        {

#if DEBUG
            System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();
            watch.Start();
#endif

            SortedDictionary<String, MaterialJob> jobs = new SortedDictionary<string, MaterialJob>();
            SortedDictionary<String, Material> material = new SortedDictionary<String, Material>();

            MaterialJob current=null;

            try
            {

                StreamReader sr = new StreamReader(filename);

                while (!sr.EndOfStream)
                {

                    String[] line = sr.ReadLine().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                    if (line.Length == 0)
                        continue;

                    for (int i = 0; i < line.Length; i++)
                        line[i] = line[i].Replace("\t", "");

                    switch (line[0])
                    {
                        case "#":   //comment
                            break;
                        case "newmtl":
                            current = new MaterialJob();
                            current.material = new Material()
                                {
                                    DiffuseConstant=new GraphicsEngineMath.Vector3(0,0,0)
                                };
                            jobs.Add(line[1], current);
                            break;
                        case "map_Kd":

                            current.diffusemap = line[1];

                            break;
                        case "Kd":
                            current.material.DiffuseConstant = new GraphicsEngineMath.Vector3(
                                float.Parse(line[1]), float.Parse(line[2]), float.Parse(line[3]));
                            break;
                        case "map_bump":
                        case "bump":
                            current.bumpmap = line[1];
                            break;

                    }



                }

                sr.Close();

            }
            catch (Exception ex)
            {
                Console.WriteLine("Material file failed to load: " + ex.Message);
            }

            List<System.Threading.Thread> threads = new List<System.Threading.Thread>();

            //execute jobs
            foreach (var job in jobs)
            {
                if (job.Value.material == null)
                    continue;

                var thread =  new System.Threading.Thread(new System.Threading.ParameterizedThreadStart((o)  =>
                    {

                        var j = (MaterialJob)o;
                
                        if (j.bumpmap != null)
                        {
                            try
                            {
                                j.material.BumpTexture = device.LoadTexture(j.bumpmap);
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                        }
                        if (j.diffusemap != null)
                        {
                            try
                            {
                                j.material.DiffuseTexture = device.LoadTexture(j.diffusemap);
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                        }
                }));
                thread.Start(job.Value);
                threads.Add(thread);
            }
            
            //wait on threads

            foreach (System.Threading.Thread thread in threads)
            {
                try
                {
                    thread.Join();
                }
                catch (System.Threading.ThreadStateException ex)
                {
                    Console.WriteLine("report thread failed.");
                    Console.WriteLine(ex.Message);
                }
            }

            foreach (var job in jobs)
                material.Add(job.Key, job.Value.material);

#if DEBUG
            Console.WriteLine("Report: Material : {0} loaded in {1}ms", filename, watch.ElapsedMilliseconds);
#endif

            return material;
        }

        
    }
}
