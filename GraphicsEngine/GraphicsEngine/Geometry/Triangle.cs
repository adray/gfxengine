﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GraphicsEngineBase
{
    /// <summary>
    /// Represent a virtual triangle.
    /// </summary>
    public class Triangle
    {

        public Vertex[] vertices { get; private set; }


        public Triangle(Vertex v1, Vertex v2, Vertex v3)
        {

            vertices = new Vertex[] { v1, v2, v3 };

        }

    }
}
