﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GraphicsEngineBase
{
    public abstract class LodContainer<T> where T : Mesh
    {

        private T[] Levels { get; set; }

        public LodContainer(params T[] levels)
        {
            Levels = levels;
        }



    }
}
