﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GraphicsEngineMath;

namespace GraphicsEngineBase
{
    /// <summary>
    /// Represents a virtual vertex.
    /// </summary>
    public class Vertex
    {

        public float[] data { get; private set;}

        public Vertex(int size)
        {
            data = new float[size];
        }

        public Vertex SetData(int offset, params float[] data)
        {
            
            Array.Copy(data, 0, this.data, offset, data.Length);

            return this;
        }

        public Vertex SetData(int offset, Vector3 data)
        {

            this.data[offset] = data.X;
            this.data[offset + 1] = data.Y;
            this.data[offset + 2] = data.Z;

            return this;
        }

    }
}
