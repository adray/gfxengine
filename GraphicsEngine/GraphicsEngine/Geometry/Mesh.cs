﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GraphicsEngineMath;

namespace GraphicsEngineBase
{

    /// <summary>
    /// Vertex shader semantic types.
    /// </summary>
    public enum DataType
    {
        Normal,
        Position,
        Colour,
        Texcoord
    }

    /// <summary>
    /// Data format layouts.
    /// </summary>
    public enum DataFormat
    {
        Float1,
        Float2,
        Float3,
        Float4
    }

    /// <summary>
    /// Pre-defined vertex types.
    /// </summary>
    public enum VertexType
    {
        /// <summary>
        /// Position - 4 floats
        /// </summary>
        Position,
        /// <summary>
        /// Position - 4 floats, Texture - 2 floats
        /// </summary>
        PositionTextured,
        PositionNormal,
        PositionNormalTextured,
        QuadPosition,
        SimpleParticle,
        Matrix,
        QuadFrustum,
        Terrain,
        InstanceParticle
    }

    /// <summary>
    /// Holds information about the vertex format.
    /// </summary>
    public sealed class DataCollection
    {

        public List<DataType> Type { get; private set; }
        public List<DataFormat> Format { get; private set;}
        /// <summary>
        /// Size of vertex in bytes.
        /// </summary>
        public int VertexSize
        {
            get;
            private set;
        }

        public DataCollection(VertexType vtype)
        {
            Type = new List<DataType>();
            Format = new List<DataFormat>();

            switch (vtype)
            {
                case VertexType.Position:
                    Type.Add(DataType.Position);
                    Format.Add(DataFormat.Float4);
                    break;
                case VertexType.QuadPosition:
                    Type.Add(DataType.Position);
                    Format.Add(DataFormat.Float2);
                    break;
                case VertexType.PositionNormal:
                    Type.Add(DataType.Position);
                    Format.Add(DataFormat.Float4);
                    Type.Add(DataType.Normal);
                    Format.Add(DataFormat.Float4);
                    break;
                case VertexType.PositionNormalTextured:
                    Type.Add(DataType.Position);
                    Format.Add(DataFormat.Float4);
                    Type.Add(DataType.Normal);
                    Format.Add(DataFormat.Float4);
                    Type.Add(DataType.Texcoord);
                    Format.Add(DataFormat.Float2);
                    break;
                case VertexType.PositionTextured:
                    Type.Add(DataType.Position);
                    Format.Add(DataFormat.Float4);
                    Type.Add(DataType.Texcoord);
                    Format.Add(DataFormat.Float2);
                    break;
                case VertexType.SimpleParticle:
                    Type.Add(DataType.Position);
                    Format.Add(DataFormat.Float1);  //should be an int
                    break;
                case VertexType.Matrix:
                    Type.Add(DataType.Texcoord);
                    Type.Add(DataType.Texcoord);
                    Type.Add(DataType.Texcoord);
                    Type.Add(DataType.Texcoord);
                    Format.Add(DataFormat.Float4);
                    Format.Add(DataFormat.Float4);
                    Format.Add(DataFormat.Float4);
                    Format.Add(DataFormat.Float4);
                    break;
                case VertexType.QuadFrustum:
                    Type.Add(DataType.Position);
                    Format.Add(DataFormat.Float3);
                    break;
                case VertexType.Terrain:
                    Type.Add(DataType.Position);
                    Format.Add(DataFormat.Float2);
                    Type.Add(DataType.Normal);
                    Format.Add(DataFormat.Float3);
                    //Type.Add(DataType.Texcoord);    //blend factor1
                    //Format.Add(DataFormat.Float1);
                    //Type.Add(DataType.Texcoord);    //blend factor2
                    //Format.Add(DataFormat.Float1);
                    break;
                case VertexType.InstanceParticle:
                    Type.Add(DataType.Texcoord);
                    Format.Add(DataFormat.Float3);
                    break;
            }

            foreach (DataFormat fmt in Format)
                VertexSize += GetSize(fmt);
        }

        /// <summary>
        /// Gets the size of the data format.
        /// </summary>
        /// <param name="fmt"></param>
        /// <returns></returns>
        private int GetSize(DataFormat fmt)
        {
            switch (fmt)
            {
                case DataFormat.Float4:
                    return 4 * sizeof(float);
                case DataFormat.Float3:
                    return 3 * sizeof(float);
                case DataFormat.Float2:
                    return 2 * sizeof(float);
                case DataFormat.Float1:
                    return 1 * sizeof(float);
                default:
                    return 0;
            }
        }

        /// <summary>
        /// Gets the index of the first occurence of data type.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public int First(DataType type)
        {

            int pos = 0;

            for (int i = 0; i < Type.Count; i++)
            {
                if (Type[i] == type)
                    return pos;
                pos += GetSize(Format[i]);
            }


            return -1;
        }

    }

    /// <summary>
    /// Is a subset of a mesh.
    /// Contains a list of triangles which are part of a mesh.
    /// </summary>
    public sealed class MeshSubset
    {

        public List<Triangle> Triangles { get; private set; }

        internal MeshSubset()
        {
            Triangles = new List<Triangle>();
        }

        /// <summary>
        /// Gets the number of triangles.
        /// </summary>
        public int PrimitiveCount
        {
            get
            {
                return Triangles.Count;
            }
        }

        /// <summary>
        /// Adds a triangle to the subset.
        /// </summary>
        /// <param name="triangle"></param>
        internal void AddTriangle(Triangle triangle)
        {
            Triangles.Add(triangle);
        }
    }

    /// <summary>
    /// Contains a collection of mesh subsets.
    /// </summary>
    public abstract class Mesh : IDisposable, IWatchable
    {

        protected List<Triangle> Triangles { get; set; }

        public List<MeshSubset> subsets { get; private set; }

        public DataCollection DataCollection { get; private set; }

        protected bool dirty = true;

        protected bool bounding_sphere_dirty = true;

        protected bool bounding_box_dirty = true;

        protected Sphere BoundingSphere { get; private set; }

        protected Box BoundingBox { get; private set; }

        public Mesh(VertexType vertexType)
        {

            Triangles = new List<Triangle>();
            subsets = new List<MeshSubset>();

            DataCollection = new DataCollection(vertexType);

        }

        public List<Triangle> GetTriangles()
        {
            return new List<Triangle>(Triangles);
        }

        public abstract int MaxPrimitivesPerSubset { get; }

        public void AddTriangle(Triangle triangle)
        {
            Triangles.Add(triangle);
            dirty = true;
            bounding_box_dirty = true;
            bounding_sphere_dirty = true;

            if (subsets.Count == 0 || subsets.Last().PrimitiveCount + 3 > MaxPrimitivesPerSubset)
            {
                MeshSubset sub = new MeshSubset();
                sub.AddTriangle(triangle);
                subsets.Add(sub);
            }
            else
            {
                MeshSubset last = subsets.Last();
                last.AddTriangle(triangle);
            }
        }
        
        public abstract void UpdateBuffer(Device device);

        public void CalculateNormals()
        {

            if (!(DataCollection.First(DataType.Position) >= 0 && DataCollection.First(DataType.Normal) >= 0))
                throw new Exception("Vertex not in correct format."); 


            foreach (Triangle triangle in Triangles)
            {

                int index = DataCollection.First(DataType.Position)/sizeof(float);

                Vector3 v1 =
                    new GraphicsEngineMath.Vector3(triangle.vertices[0].data[index], triangle.vertices[0].data[index+1],
                        triangle.vertices[0].data[index+2]);

                Vector3 v2 =
                    new GraphicsEngineMath.Vector3(triangle.vertices[1].data[index], triangle.vertices[1].data[index+1],
                        triangle.vertices[1].data[index+2]);

                Vector3 v3 =
                    new GraphicsEngineMath.Vector3(triangle.vertices[2].data[index], triangle.vertices[2].data[index+1],
                        triangle.vertices[2].data[index+2]);

                Vector3 n1 = Vector3.Cross(v3 - v1, v2 - v1); n1.Normalize();
                Vector3 n2 = Vector3.Cross(v1 - v2, v3 - v2); n2.Normalize();
                Vector3 n3 = Vector3.Cross(v1 - v3, v2 - v3); n3.Normalize();

                index = DataCollection.First(DataType.Normal)/sizeof(float);

                triangle.vertices[0].SetData(index, n1.X, n1.Y, n1.Z, 1);
                triangle.vertices[1].SetData(index, n2.X, n2.Y, n2.Z, 1);
                triangle.vertices[2].SetData(index, n3.X, n3.Y, n3.Z, 1);

            }

        }

        public Sphere CalculateBoundingSphere()
        {

            if (!bounding_sphere_dirty && BoundingSphere != null)
                return BoundingSphere;

            Vector3 centre=new Vector3();
            int count = 0;
            float dist=float.NegativeInfinity;

            foreach (var triangle in Triangles)
                foreach (var vertex in triangle.vertices)
                {
                    centre += new Vector3(vertex.data[0], vertex.data[1], vertex.data[2]);
                    count++;
                }

            centre /= count;

            foreach (var triangle in Triangles)
                foreach (var vertex in triangle.vertices)
                {
                    Vector3 length = new Vector3(vertex.data[0], vertex.data[1], vertex.data[2]) - centre; 
                    float ndist=Vector3.Dot(length, length);
                    if (ndist > dist)
                        dist = ndist;
                }


            BoundingSphere=new Sphere(centre, (float)Math.Sqrt(dist));
            bounding_sphere_dirty = false;

            return BoundingSphere;
        }

        public Box CalculateBoundingBox()
        {


            if (!bounding_box_dirty && BoundingBox != null)
                return BoundingBox;

            Box box = new Box();

            foreach (Triangle tri in Triangles)
            {
                foreach (Vertex v in tri.vertices)
                    box.Union(new Vector3(v.data[0], v.data[1], v.data[2]));

            }

            BoundingBox = box;
            bounding_box_dirty = false;

            return box;


        }

        public int PrimitiveCount
        {
            get
            {
                return Triangles.Count;
            }
        }


        public void Clear()
        {

            Triangles.Clear();
            subsets.Clear();
            dirty = true;

        }

        public abstract void Dispose();

        public void AddQuad(Vertex v1, Vertex v2, Vertex v3, Vertex v4)
        {

            //if (!ContainsPosition())
            //    throw new Exception("Vertex not in correct format.");

            Triangle t1 = new Triangle(v1, v2, v3);  //first triangle is easy

            Triangle t2 = new Triangle(v3, v4, v1); ;


            AddTriangle(t1);
            AddTriangle(t2);

        }

        public abstract void SetItem(object obj);

    }

    /// <summary>
    /// Represents a collection of objects that are the mesh type, but have a different world transform.
    /// </summary>
    public abstract class MeshInstance : IDisposable
    {

        public abstract void Add(Matrix4x4 instance);

        public abstract void Remove(int index);

        public abstract int InstanceCount { get; }

        public abstract void Dispose();

        public abstract Matrix4x4 At(int index);

        public abstract void Set(int index, Matrix4x4 instance);
    }
}
