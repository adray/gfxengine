﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GraphicsEngineBase
{
    /// <summary>
    /// Represents the strategy used to the render the scene.
    /// </summary>
    public abstract class Renderer : IDisposable
    {

        /// <summary>
        /// Renders the scene.
        /// </summary>
        /// <param name="scene"></param>
        public abstract void RenderScene(Scene scene);

        /// <summary>
        /// Disposes of the renderer.
        /// </summary>
        public abstract void Dispose();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="chain"></param>
        public abstract void SetPostProcessChain(Chain chain);
    }
}
